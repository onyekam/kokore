module.exports = {
    'Demo test Google': function (browser) {
        browser
            .url('http://www.google.com')
            .expect.element('body').to.be.present;
            browser.setValue('input[type=text]', 'nightwatch')
            .expect.element('input[name=btnK]').to.be.present;
            browser
            .click('input[name=btnK]')
            .pause(1000)
            .assert.containsText('#main', 'Night Watch')
            .end();
    }
};