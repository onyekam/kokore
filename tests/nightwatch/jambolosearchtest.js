module.exports = {
    'Test search': function (browser) {
        browser
            .url('https://jambolo.com')
            .expect.element('body').to.be.present;
        browser
            .setValue('select[name=city]', 'Lagos')
            .pause(5000)
            .expect.element('#areaDropdown2').to.have.value.equals('- Choose Your Area -');
        browser
            .setValue('select[name=area]', 'Apapa')
            .setValue('select[name=category]', 'Carpenter')
            .click('button.btn.btn-primary.btn-block')
            .end();
    }
}