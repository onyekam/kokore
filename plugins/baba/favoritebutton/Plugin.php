<?php namespace BABA\Favoritebutton;

use RainLab\User\Models\User;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
   /**
     * @var array Plugin dependencies
     */
    public $require = ['RainLab.User'];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Favorite Button',
            'description' => 'Adds a favorite feature to your October models.',
            'author'      => 'BABA',
            'icon'        => 'icon-star'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        User::extend(function($model) {
            $model->implement[] = 'BABA.Favoritebutton.Behaviors.Favoriteability';
        });
    }
    
    public function registerComponents()
    {
        return [
            '\BABA\Favoritebutton\Components\FavoriteButton' => 'favoriteButton',
            '\BABA\Favoritebutton\Components\MyFavoritePosts' => 'myFavoritePosts'
        ];
    }
}
