<?php namespace Ffande\Competition\Models;

use Model;

/**
 * Model
 */
class Category extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ffande_competition_categories';

     public $attachOne = [
        'image' => 'System\Models\File'
    ];
}