<?php namespace Ffande\Competition\Components;

use Cms\Classes\ComponentBase;
use Ffande\Competition\Models\Category;

class CompetitionCategories extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'CompetitionCategories Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }


    public function onRun(){
        $this->competitionCategories = $this->getCategories();
        //return $this->competitionCategories;
    }

    public function getCategories(){
        $categories = Category::all();
        return $categories;
    }

    public $competitionCategories;
}
