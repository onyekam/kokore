<?php namespace Ffande\Competition\Components;

use Cms\Classes\ComponentBase;
use Ffande\Competition\Models\Category;

class CompetitionCategoryPage extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'CompetitionCategoryPage Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun(){
        $this->competitionCategoryPage = $this->getCompetitionCategoryPage();
        
    }

    public function getCompetitionCategoryPage(){
        $categoryPage = Category::where('slug', $this->param('slug'))->first();
        return $categoryPage;
    }

    public $competitionCategoryPage;
}
