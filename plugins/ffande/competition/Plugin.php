<?php namespace Ffande\Competition;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
        'Ffande\Competition\Components\CompetitionCategories' => 'CompetitionCategories',
        'Ffande\Competition\Components\CompetitionCategoryPage' => 'CompetitionCategoryPage'
        ];
    }

    public function registerSettings()
    {
    }
}
