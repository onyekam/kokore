<?php namespace Ffande\Competition\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateFfandeCompetitionCategories extends Migration
{
    public function up()
    {
        Schema::create('ffande_competition_categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->text('content')->nullable();
            $table->string('description')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ffande_competition_categories');
    }
}
