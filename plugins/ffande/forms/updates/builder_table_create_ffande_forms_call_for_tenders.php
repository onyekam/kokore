<?php namespace Ffande\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateFfandeFormsCallForTenders extends Migration
{
    public function up()
    {
        Schema::create('ffande_forms_call_for_tenders', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('f_name')->nullable();
            $table->string('l_name')->nullable();
            $table->string('email')->nullable();
            $table->string('number')->nullable();
            $table->string('company_name')->nullable();
            $table->string('title')->nullable();
            $table->integer('project_type_id')->nullable();
            $table->string('project_location')->nullable();
            $table->string('preferred_manufacturer')->nullable();
            $table->string('tender_period_deadline')->nullable();
            $table->string('preferred_delivery_terms')->nullable();
            $table->text('comments_questions')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ffande_forms_call_for_tenders');
    }
}
