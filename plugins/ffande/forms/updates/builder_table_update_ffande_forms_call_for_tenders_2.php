<?php namespace Ffande\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeFormsCallForTenders2 extends Migration
{
    public function up()
    {
        Schema::table('ffande_forms_call_for_tenders', function($table)
        {
            $table->text('slug')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('ffande_forms_call_for_tenders', function($table)
        {
            $table->dropColumn('slug');
        });
    }
}
