<?php namespace Ffande\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class JobOrTenderForTenderstable extends Migration
{
    public function up()
    {
        Schema::table('ffande_forms_call_for_tenders', function($table)
        {
            $table->enum('job_or_tender',['tender','job']);
        });
    }
    
    public function down()
    {
        Schema::table('ffande_forms_call_for_tenders', function($table)
        {
            $table->dropColumn('job_or_tender');
        });
    }
}
