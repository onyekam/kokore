<?php namespace Ffande\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeFormsCallForTenders3 extends Migration
{
    public function up()
    {
        Schema::table('ffande_forms_call_for_tenders', function($table)
        {
            $table->renameColumn('tender_period_deadline', 'deadline');
            $table->renameColumn('comments_questions', 'description');
        });
    }
    
    public function down()
    {
        Schema::table('ffande_forms_call_for_tenders', function($table)
        {
            $table->renameColumn('deadline', 'tender_period_deadline');
            $table->renameColumn('description', 'comments_questions');
        });
    }
}
