<?php namespace Ffande\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateFfandeFormsRequestForQuotes extends Migration
{
    public function up()
    {
        Schema::create('ffande_forms_request_for_quotes', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('f_name')->nullable();
            $table->string('l_name')->nullable();
            $table->string('email')->nullable();
            $table->string('number')->nullable();
            $table->string('project_name')->nullable();
            $table->string('project_location')->nullable();
            $table->text('comments_questions')->nullable();
            $table->integer('user_id')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ffande_forms_request_for_quotes');
    }
}
