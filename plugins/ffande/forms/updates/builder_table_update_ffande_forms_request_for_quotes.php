<?php namespace Ffande\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeFormsRequestForQuotes extends Migration
{
    public function up()
    {
        Schema::table('ffande_forms_request_for_quotes', function($table)
        {
            $table->text('slug')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('ffande_forms_request_for_quotes', function($table)
        {
            $table->dropColumn('slug');
        });
    }
}
