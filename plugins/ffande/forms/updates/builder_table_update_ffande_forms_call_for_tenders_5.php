<?php namespace Ffande\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeFormsCallForTenders5 extends Migration
{
    public function up()
    {
        Schema::table('ffande_forms_call_for_tenders', function($table)
        {
            $table->string('slug', 255)->nullable()->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('ffande_forms_call_for_tenders', function($table)
        {
            $table->text('slug')->nullable()->unsigned(false)->default(null)->change();
        });
    }
}
