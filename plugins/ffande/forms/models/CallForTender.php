<?php namespace Ffande\Forms\Models;

use Model;

/**
 * Model
 */
class CallForTender extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ffande_forms_call_for_tenders';

    protected $fillable = ['f_name', 'l_name', 'number', 'email', 'project_location', 'title','preferred_delivery_terms', 'preferred_manufacturer','company_name', 'tender_period_deadline', 'comments_questions', 'user_id'];
    
        public $attachMany = [
            'documents' => 'System\Models\File'
        ];
    
        public $belongsTo = [
            'user' => 'Rainlab\User\Models\User'
        ];
}