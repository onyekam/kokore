<?php namespace Ffande\Forms\Models;

use Model;

/**
 * Model
 */
class RequestForQuote extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ffande_forms_request_for_quotes';

    protected $fillable = ['f_name', 'l_name', 'number', 'email', 'project_location', 'project_name', 'comments_questions', 'user_id'];

    public $attachMany = [
        'documents' => 'System\Models\File'
    ];

    public $belongsTo = [
        'user' => 'Rainlab\User\Models\User'
    ];
}