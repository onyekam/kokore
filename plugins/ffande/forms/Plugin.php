<?php namespace Ffande\Forms;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Ffande\Forms\Components\RequestForQuotes' => 'RequestForQuotes',
            'Ffande\Forms\Components\SubmitRequestForQuotes' => 'SubmitRequestForQuotes',
            'Ffande\Forms\Components\CallForTenders' => 'CallForTenders',
            'Ffande\Forms\Components\SubmitCallForTenders' => 'SubmitCallForTenders'
            ];
    }

    public function registerSettings()
    {
    }
}
