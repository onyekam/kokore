<?php namespace Ffande\Forms\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Ffande\Forms\Models\RequestForQuote;
use Ffande\Forms\Models\Product;
use Redirect; 
use File;
use Auth;
use Lang;
use Mail;
use Event;
use Flash;
use Request;
use Validator;
use ValidationException;
use ApplicationException;
use Cms\Classes\Page;
use RainLab\User\Models\Settings as UserSettings;
use Exception;



class SubmitRequestForQuotes extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Submit Request for quotes',
			'description' => 'Submit request for quotes'
		];
	}



	public function onRun(){

    }
	
	public function submitRequestForQuotes(){
    	$this->requestForQuotes = RequestForQuote::all();
	}
	
	public function onRFQSubmit(){
		$user = Auth::getUser();
		$rfq = new RequestForQuote();
		$rfq->f_name = $user->name;
		$rfq->l_name = $user->surname;
		$rfq->project_name = Input::get('project_name');
		$rfq->project_location = Input::get('project_location');
		$rfq->email = $user->email;
		$rfq->slug = preg_replace('~[^\pL\d]+~u', '-', Input::get('project_name'));
		$rfq->number = $user->phone_number;
		$rfq->comments_questions = Input::get('comments_questions');
		$rfq->documents = Input::file('documents');
		$rfq->user_id = $user->id;
		$rfq->save();	
		Flash::success('Request for quote submitted successfully!');
        return Redirect::refresh();
	}

	public function onDocumentUpload(){
		$document = Input::all();
		$file = (new File())->fromPost($document['documents']);
		return [
            '#document' => '<img src="'. $file->getThumb(200, 200, ['mode' => 'crop']).'">'

        ];

	}

}