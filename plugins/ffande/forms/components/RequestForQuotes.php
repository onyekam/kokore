<?php namespace Ffande\Forms\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Ffande\Forms\Models\RequestForQuote;
use Ffande\Forms\Models\Product;
use Redirect; 
use Auth;


class RequestForQuotes extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Display Request for quotes',
			'description' => 'List all request for quotes'
		];
	}



	public function onRun(){
        $this->requestForQuotes = $this->displayRequestForQuotes();
        // if ($this->param('slug')) {
		// 	$this->displayManufacturer();
		// 	$this->displayManufacturerProducts();	
        // } else {
        // 	$this->manufacturers = Manufacturer::all();
        // }
        

    }

    // public function displayManufacturer(){
    // 	$this->manufacturer = Manufacturer::where('slug', $this->param('slug'))->first();
	// }
	
	public function displayRequestForQuotes(){
		$user = Auth::getUser();
		$requestForQuotes = RequestForQuote::where('user_id', $user['id'])->get();
		return $requestForQuotes;
    }
    public $requestForQuotes;
	public $requestForQuote;
	//public $products;

}