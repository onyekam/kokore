<?php namespace Ffande\Forms\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Ffande\Forms\Models\CallForTender;
use Ffande\Forms\Models\Product;
use File;
use Auth;
use Lang;
use Mail;
use Event;
use Flash;
use Request;
use Redirect;
use Validator;
use ValidationException;
use ApplicationException;
use Cms\Classes\Page;
use RainLab\User\Models\Settings as UserSettings;
use Exception;



class SubmitCallForTenders extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Submit Call for tenders',
			'description' => 'Submit Call for tenders'
		];
	}



	public function onRun(){
       
    }
    
	public function onCFTSubmit(){
		$cft = new CallForTender();
		$cft->f_name = Input::get('f_name');
		$cft->l_name = Input::get('l_name');
		$cft->title = Input::get('title');
		$cft->slug = $this->to_permalink(Input::get('title'));
		$cft->project_location = Input::get('project_location');
		$cft->email = Input::get('email');
		$cft->number = Input::get('number');
		$cft->description = Input::get('description');
		$cft->documents = Input::file('documents');
		// $cft->project_type_id = Input::get('project_type_id');
		$cft->deadline = Input::get('deadline');
		$cft->approved = false;
		$cft->company_name = Input::get('company_name');
		// $cft->preferred_delivery_terms = Input::get('preferred_delivery_terms');
		// $cft->preferred_manufacturer = Input::get('preferred_manufacturer');
		$cft->user_id = Auth::getUser()->id;
		$cft->save();	
	}

	public function onDocumentUpload(){
		$document = Input::all();
		$file = (new File())->fromPost($document['documents']);
		return [
            '#document' => '<img src="'. $file->getThumb(200, 200, ['mode' => 'crop']).'">'

        ];

	}

	public function to_permalink($str){
		if($str !== mb_convert_encoding( mb_convert_encoding($str, 'UTF-32', 'UTF-8'), 'UTF-8', 'UTF-32') )
			$str = mb_convert_encoding($str, 'UTF-8', mb_detect_encoding($str));
		$str = htmlentities($str, ENT_NOQUOTES, 'UTF-8');
		$str = preg_replace('`&([a-z]{1,2})(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig);`i', '\\1', $str);
		$str = html_entity_decode($str, ENT_NOQUOTES, 'UTF-8');
		$str = preg_replace(array('`[^a-z0-9]`i','`[-]+`'), '-', $str);
		$str = strtolower( trim($str, '-') );
		return $str;
	}

}