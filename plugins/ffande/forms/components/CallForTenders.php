<?php namespace Ffande\Forms\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Ffande\Forms\Models\CallForTender;
use Ffande\Forms\Models\Product;
use Redirect; 
use Auth;


class CallForTenders extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Display Call for tenders',
			'description' => 'List all Call for tenders'
		];
	}



	public function onRun(){

		if ($this->page->title == 'Jobs') {
			$this->displayJobs();
		}
		if ($this->page->title == 'Job') {
			$this->job = $this->displayJob();
		}
		if ($this->page->title == 'Call for Tenders') {
			$this->displayCallForTenders();
		}
		if ($this->page->title == 'Call for Tender') {
			$this->tender = $this->displayTender();
		}
		//$this->displayCallForTenders();
		//return $this->callForTenders;
        // if ($this->param('slug')) {
		// 	$this->displayManufacturer();
		// 	$this->displayManufacturerProducts();	
        // } else {
        // 	$this->manufacturers = Manufacturer::all();
        // }
        

    }

    // public function displayManufacturer(){
    // 	$this->manufacturer = Manufacturer::where('slug', $this->param('slug'))->first();
	// }
	
	public function displayCallForTenders(){
		$this->callForTenders = CallForTender::where('job_or_tender', 'tender')->get();
	}

	public function displayJobs(){
		$this->jobs = CallForTender::where('job_or_tender','job')->get();
	}
	
	public function displayUserCallForTender(){
		$user = Auth::getUser();
		$this->callForTenders = CallForTender::where('user_id', $user['id'])->get();
	}

	public function displayTender(){
		$tender = CallForTender::where('slug', $this->param('slug'))->where('job_or_tender','tender')->first();
		return $tender;
	}

	public function displayJob(){
		$job = CallForTender::where('slug', $this->param('slug'))->where('job_or_tender','job')->first();
		return $job;
	}

    public $callForTenders;
	public $tender;
	public $jobs;
	public $job;
	//public $products;

}