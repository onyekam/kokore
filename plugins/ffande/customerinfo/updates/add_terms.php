<?php namespace Ffande\Customerinfo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddTerms extends Migration
{
    public function up()
    {
        Schema::table('users', function($table)
        {
            $table->boolean('terms');
        });
    }
    
    public function down()
    {
        Schema::table('users', function ($table) {
            $table->dropColumn('terms');
        });
    }
}
