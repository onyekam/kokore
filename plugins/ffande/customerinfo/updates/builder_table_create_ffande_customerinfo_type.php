<?php namespace Ffande\Customerinfo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateFfandeCustomerinfoType extends Migration
{
    public function up()
    {
        Schema::create('ffande_customerinfo_type', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('type_name')->nullable();
            $table->string('slug')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ffande_customerinfo_type');
    }
}
