<?php namespace Ffande\Customerinfo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddAdditionalFieldsNew extends Migration
{

    public function up()
    {
        Schema::table('users', function($table)
        {
            $table->integer('exclusives')->nullable();
            $table->integer('newsletter')->nullable();
        });
    }

    public function down()
    {
       Schema::table('users', function ($table) {
    $table->dropColumn([
                'exclusives',
                'newsletter'
            ]);
    });
            
       
    }

}
