<?php namespace Ffande\Customerinfo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeCustomerinfoType extends Migration
{
    public function up()
    {
        Schema::table('ffande_customerinfo_type', function($table)
        {
            $table->string('type_image')->nullable();
            $table->increments('id')->unsigned(false)->change();
        });
    }
    
    public function down()
    {
        Schema::table('ffande_customerinfo_type', function($table)
        {
            $table->dropColumn('type_image');
            $table->increments('id')->unsigned()->change();
        });
    }
}
