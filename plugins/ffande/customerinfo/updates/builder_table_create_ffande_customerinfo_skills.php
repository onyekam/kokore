<?php namespace Ffande\Customerinfo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateFfandeCustomerinfoSkills extends Migration
{
    public function up()
    {
        Schema::create('ffande_customerinfo_skills', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('modern');
            $table->integer('contemporary');
            $table->integer('minimalistic');
            $table->integer('mid_century_modern');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ffande_customerinfo_skills');
    }
}
