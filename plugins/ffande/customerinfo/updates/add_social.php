<?php namespace Ffande\Customerinfo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddSocial extends Migration
{
    public function up()
    {
        Schema::table('users', function($table)
        {
            $table->string('instagram')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('users', function ($table) {
            $table->dropColumn('instagram');
        });
    }
}
