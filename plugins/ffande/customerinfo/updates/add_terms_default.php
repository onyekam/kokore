<?php namespace Ffande\Customerinfo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddTermsDefault extends Migration
{
    public function up()
    {
        Schema::table('users', function($table)
        {
            $table->boolean('terms')->default(0)->change();
        });
    }
    
    public function down()
    {
        Schema::table('users', function ($table) {
            $table->dropColumn('terms');
        });
    }
}
