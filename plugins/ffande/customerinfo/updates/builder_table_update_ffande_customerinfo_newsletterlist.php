<?php namespace Ffande\Customerinfo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeCustomerinfoNewsletterlist extends Migration
{
    public function up()
    {
        Schema::table('ffande_customerinfo_newsletterlist', function($table)
        {
            $table->increments('id')->unsigned(false)->change();
        });
    }
    
    public function down()
    {
        Schema::table('ffande_customerinfo_newsletterlist', function($table)
        {
            $table->increments('id')->unsigned()->change();
        });
    }
}
