<?php namespace Ffande\Customerinfo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeCustomerinfoSkills extends Migration
{
    public function up()
    {
        Schema::table('ffande_customerinfo_skills', function($table)
        {
            $table->integer('user_id')->nullable();
            $table->increments('id')->unsigned(false)->change();
            $table->integer('modern')->nullable()->change();
            $table->integer('contemporary')->nullable()->change();
            $table->integer('minimalistic')->nullable()->change();
            $table->integer('mid_century_modern')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('ffande_customerinfo_skills', function($table)
        {
            $table->dropColumn('user_id');
            $table->increments('id')->unsigned()->change();
            $table->integer('modern')->nullable(false)->change();
            $table->integer('contemporary')->nullable(false)->change();
            $table->integer('minimalistic')->nullable(false)->change();
            $table->integer('mid_century_modern')->nullable(false)->change();
        });
    }
}
