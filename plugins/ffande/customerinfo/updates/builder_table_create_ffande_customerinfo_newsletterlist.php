<?php namespace Ffande\Customerinfo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateFfandeCustomerinfoNewsletterlist extends Migration
{
    public function up()
    {
        Schema::create('ffande_customerinfo_newsletterlist', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('email')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ffande_customerinfo_newsletterlist');
    }
}
