<?php namespace Ffande\Customerinfo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddAdditionalFields extends Migration
{

    public function up()
    {
        Schema::table('users', function($table)
        {
            $table->string('display_name')->nullable();
            $table->string('address1')->nullable();
            $table->string('address2')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('country')->nullable();
            $table->string('type_id')->nullable();
            $table->string('bio')->nullable();
        });
    }

    public function down()
    {
       Schema::table('users', function ($table) {
    $table->dropColumn([
                'address1',
                'address2',
                'city',
                'state',
                'country',
                'type_id',
                'bio'
            ]);
});
            
       
    }

}
