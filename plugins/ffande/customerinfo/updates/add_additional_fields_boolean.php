<?php namespace Ffande\Customerinfo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddAdditionalFieldsBoolean extends Migration
{

    public function up()
    {
        Schema::table('users', function($table)
        {
            $table->boolean('exclusives')->change();
            $table->boolean('newsletter')->change();
        });
    }

    public function down()
    {
       Schema::table('users', function ($table) {
    $table->dropColumn([
                'exclusives',
                'newsletter'
            ]);
    });
            
       
    }

}
