<?php namespace Ffande\Customerinfo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeCustomerinfoNewsletterlists extends Migration
{
    public function up()
    {
        Schema::rename('ffande_customerinfo_newsletterlist', 'ffande_customerinfo_newsletterlists');
    }
    
    public function down()
    {
        Schema::rename('ffande_customerinfo_newsletterlists', 'ffande_customerinfo_newsletterlist');
    }
}
