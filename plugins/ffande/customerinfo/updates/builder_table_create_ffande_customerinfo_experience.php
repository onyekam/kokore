<?php namespace Ffande\Customerinfo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateFfandeCustomerinfoExperience extends Migration
{
    public function up()
    {
        Schema::create('ffande_customerinfo_experience', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('year')->nullable();
            $table->string('degree_type')->nullable();
            $table->string('description', 123)->nullable();
            $table->integer('user_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ffande_customerinfo_experience');
    }
}
