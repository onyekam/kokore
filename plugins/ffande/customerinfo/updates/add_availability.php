<?php namespace Ffande\Customerinfo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddAvailability extends Migration
{

    public function up()
    {
        Schema::table('users', function($table)
        {
            $table->boolean('available')->nullable();
        });
    }

    public function down()
    {
        
       Schema::table('users', function ($table) {
    $table->dropColumn('available');
});
        
    }

}