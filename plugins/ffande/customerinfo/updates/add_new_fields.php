<?php namespace Ffande\Customerinfo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddNewFields extends Migration
{

    public function up()
    {
        Schema::table('users', function($table)
        {
            $table->string('phone_number')->nullable();
            

        });
    }

    public function down()
    {
        
       Schema::table('users', function ($table) {
    $table->dropColumn('phone_number');
});
        
    }

}