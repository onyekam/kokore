<?php namespace Ffande\Customerinfo\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Ffande\Customerinfo\Models\Profession;
use Ffande\Customerinfo\Models\Experience;
use Ffande\Customerinfo\Models\NewsletterList;
use Redirect; 
use Illuminate\Support\Collection;
use Auth;
use Flash;
use System\Models\File as File;
use Cookie;


class Newsletterusers extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Registration Widget',
			'description' => 'Includes processsing for some registration fields'
		];
	}



	public function onRun(){
		$this->newsletterCookie = Cookie::get('newsletterCookie');
    }

	public function onNewsletter(){
		
		//dd(Input::all());
		if (Input::get('email')) {
			$newsletterCheck = NewsletterList::where('email', Input::get('email'))->first();
			if($newsletterCheck){
				return [
					'#newslettervalidation' => $this->renderPartial('Newsletterusers::newsletterModal')
				];
			} else {
				$subscribe = new NewsletterList;
				$subscribe->email = Input::get('email');
				$subscribe->save();
				$uniqueVal = uniqid();
				Cookie::queue('newsletterCookie', $uniqueVal, 60 * 24 * 365 * 5 );
				\Flash::success('You have successfully subscribed to the newsletter');
				return Redirect::refresh();
			}
		} else {
			return [
				'#newslettervalidation' => $this->renderPartial('Newsletterusers::validemail')
			];
		}
		

		
		//return $professions;
	}


	
	public $professions;
	public $newsletterCookie;
	//public $products;

}