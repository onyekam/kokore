<?php namespace Ffande\CustomerInfo\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Ffande\Customerinfo\Models\Profession;
use Ffande\Procurement\Models\Product;
use Ffande\Procurement\Models\ArtistPortfolio;
use Redirect; 
use Auth;
use Mail;
use Rainlab\User\Models\User;


class Community extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Community component',
			'description' => 'Community component'
		];
	}



	public function onRun(){
		// $this->professions = $this->displayProfessions();
		if (Auth::getUser()) {
			$user = Auth::getUser();
			// if ($user->type_id !== 2 || $user->type_id !== 3){
			// 	return Redirect::to('/');
			// }
			$this->urlUser = $this->getUrlUser();
			//dd($this->urlUser->type_id);
			if ( $this->urlUser->type_id < 2){
				return Redirect::to('/');
			}
			// if($this->param('display_name')){
				$this->prints = $this->getPrints();
				$this->works = $this->getWorks();
			// } elseif($this->param('slug')) {
				$this->allPrints = $this->getAllPrints();
			// }
			if($this->urlUser->id == $user->id ){
				$this->canEdit = true;
			} else {
				$this->canEdit = false;
			}
		} else {
			$this->urlUser = $this->getUrlUser();
			$this->works = $this->getWorks();
			$this->prints = $this->getPrints();
		}
		
        
	}
	
	public function getUrlUser(){
		if ($this->param('display_name')) {
			$urlUserParam = $this->param('display_name');
			$urlUser = User::where('display_name',$urlUserParam)->first();
			return $urlUser;
		} else {
			return  Auth::getUser();
		}
	}

	public function getPrints(){
		$userParam = User::where('display_name', $this->param('display_name'))->first();
		$prints = Product::where('user_id', $userParam->id)->paginate(8);
		return $prints;
	}

	public function getWorks(){
		$userParam = User::where('display_name', $this->param('display_name'))->first();
		$prints = ArtistPortfolio::where('user_id', $userParam->id)->paginate(8);
		return $prints;
	}

	public function getAllPrints(){
		$userParam = User::where('display_name', $this->param('display_name'))->first();
		$prints = Product::where('user_id', $userParam->id)->get();
		return $prints;
	}
	
	public function onGetMorePrints(){
		$offset = intval(post('offset'));
		$limit = intval(post('limit'));
		$userID = post('userID');

		$userPrints = Product::where('user_id', $userID)->get();
		$count = $userPrints->count();
		$this->more = $count > $offset + $limit;
	}

    public function displayManufacturer(){
    	$this->manufacturer = Manufacturer::where('slug', $this->param('slug'))->first();
	}
	
	public function displayProfessions(){
		$professions = Profession::all();
		return $professions;
	}

	public function onChangeHeaderImage(){
		$user = Auth::getUser();
		$user->header_image = Input::file('header_image');
		$user->save();
		return Redirect::refresh();
	}

	public function onChangeProfileImage(){
		$user = Auth::getUser();
		$user->avatar = Input::file('avatar');
		$user->save();
		return Redirect::refresh();
	}

	public function onInvite() {
        $this->user = Auth::getUser();
        $data2 = array_combine(Input::get('email'), Input::get('name'));
        foreach ($data2 as $this->key => $this->value) {
            $vars = ['firstname' => $this->user->name, 'lastname' => $this->user->surname, 'name' =>$this->value];
            Mail::send('ffande.procurement::mail.invitation', $vars, function($message) {
                $message->to($this->key, $this->value);
                $message->subject('FF&E Invitation by '.$this->user->name.' '.$this->user->surname);
            }); 
		}
		
		\Flash::success('Your invitation(s) have been sent to your friend(s)');
        return Redirect::refresh();
    }

    public $value;
    public $key;
	public $user;
	public $canEdit;
	public $professions;
	public $urlUser;
	public $more = false;
	public $prints;
	public $works;
}