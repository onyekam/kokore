<?php namespace Ffande\Customerinfo\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Ffande\Customerinfo\Models\Profession;
use Ffande\Customerinfo\Models\Experience;
use Redirect; 
use Illuminate\Support\Collection;
use Auth;
use Flash;
use System\Models\File as File;


class Getcustomerinfo extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Registration Widget',
			'description' => 'Includes processsing for some registration fields'
		];
	}



	public function onRun(){
        $this->professions = $this->displayProfessions();

    }

	public function displayProfessions(){
		$professions = Profession::all();
		return $professions;
	}

	public function onAvatarUpload(){
		$image = Input::all();
		$avatar = (new File())->fromPost($image['avatar']);
        $avatar = '<img src="'.$avatar->getThumb(150, ['mode' => 'crop']).'" style="padding:2px;"/>';
        return [
            '#avatar' => $avatar
        ];
	}

	public function onBackImageUpload(){
		$image = Input::all();
		$backImage = (new File())->fromPost($image['backImage']);
		$user = Auth::getUser();
		if (Input::hasFile('backImage')) {
            $user->backImage = Input::file('backImage');
        }

        $backImage = '<img src="'.$backImage->getThumb(500, ['mode' => 'crop']).'" style="padding:2px;"/>';
		$user->save();
		return [
            '#backImage' => $backImage
		];
		
	}

	public function onUpdate(){
		
	}
	

	
    public $professions;
	//public $products;

}