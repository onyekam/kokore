<?php namespace Ffande\Customerinfo\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Ffande\Customerinfo\Models\Profession;
use Redirect; 
use Auth;


class ProfessionalRegistration extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Registration Widget',
			'description' => 'Includes processsing for some registration fields'
		];
	}



	public function onRun(){
        $this->professions = $this->displayProfessions();

    }

	public function onProfessionalRegistration(){
		$this->account->onRegister();
        $user = $this->account->user();
        if($user){

            return Redirect::to('/');
        }
    }
    

    public function sendRegistrationEmail(){
        $data = post();
        $vars = [
             'first_name' => $data['name'], 
             'last_name' => $data['surname'],
             'email'    => $data['email'], 
             'phone_number' => $data['phone_number']
         ];
          Mail::send('ffande.customerinfo::mail.registrationmessage', $vars, function($message) {

             $message->to($data['email'], 'FF&E');
             $message->subject('Welcome to FF&E');

         });


    }
	

	
    public $professions;
	//public $products;

}