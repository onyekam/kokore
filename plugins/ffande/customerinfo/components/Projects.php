<?php namespace Ffande\Customerinfo\Components;
use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Ffande\Customerinfo\Models\Project;
use Ffande\Customerinfo\Models\ProjectItem;
use Redirect; 
use Auth;
use System\Models\File as File;
use Flash;
class Projects extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Display Projects',
			'description' => 'List all projects'
		];
	}
	public function onRun(){
        $this->user = Auth::getUser();
        if($this->param('slug')){
            $this->project = $this->getProject();
            $this->projectItem = $this->getProjectItem();
        }

        if($this->param('project')){
            $this->project = $this->getProject();
        }

        $this->projects = $this->displayProjects();
        $this->projectsProfile = $this->displayProjectsProfile();
        //dd($this->user);
        //dd($this->projects);
    }
    public function onSubmitProject(){
        $user = Auth::getUser();
        $project = new Project();
        $project->project_name = Input::get('project_name');
        $project->slug = preg_replace('~[^\pL\d]+~u', '-', Input::get('project_name'));
        $project->project_description = Input::get('project_description');
        $project->project_image = Input::file('project_image');
        $project->user_id = $user['id'];
        $project->save();
        Flash::success('Project Successfully Created!');
        return Redirect::refresh();
    }
    public function onImageUpload(){
        $image = Input::all();
        //$images = [];
        $project_images = "";
        // foreach ($image['project_images'] as $projectImage) {
		// 	array_push($images, (new File())->fromPost($projectImage));
        // }
        $project_image = (new File())->fromPost($image['project_image']);
        // foreach ($images as $project_image){
        //     $project_images .= '<img src="'. $project_image->getThumb(200, 200, ['mode' => 'crop']).'" style="padding:2px;"/>';
        // }
        $project_featured_image = '<img src="'.$project_image->getThumb(200, 200, ['mode' => 'crop']).'" style="padding:2px;"/>';
  
        return [
            '#project_image' => $project_featured_image
        ];
    }


    public function onSubmitProjectItem(){
        $user = Auth::getUser();
        $projectItem = new ProjectItem();
        $projectItem->title = Input::get('title');
        $projectItem->slug = preg_replace('~[^\pL\d]+~u', '-', Input::get('title'));
        $projectItem->description = Input::get('description');
        $projectItem->project_id = Input::get('project');
        $projectItem->project_item_image = Input::file('project_item_image');
        $projectItem->save();
        Flash::success('Project Item Successfully Created!');
        return Redirect::refresh();
    }
    public function onItemImageUpload(){
        $image = Input::all();
        //$images = [];
        //$project_images = "";
        // foreach ($image['project_images'] as $projectImage) {
		// 	array_push($images, (new File())->fromPost($projectImage));
        // }
        $project_item_image = (new File())->fromPost($image['project_item_image']);
        // foreach ($images as $project_image){
        //     $project_images .= '<img src="'. $project_image->getThumb(200, 200, ['mode' => 'crop']).'" style="padding:2px;"/>';
        // }
        $project_item_image = '<img src="'.$project_item_image->getThumb(200, 200, ['mode' => 'crop']).'" style="padding:2px;"/>';
  
        return [
            '#project_item_image' => $project_item_image
        ];
    }



	public function displayProjects(){
        $user = Auth::getUser();
        $projects = Project::where('user_id', $user['id'])->get();
        //dd($projects);
        return $projects;
    }

    public function displayProjectsProfile(){
        $user = Auth::getUser();
        $projects = Project::where('user_id', $user['id'])->take(4)->get();
        //dd($projects);
        return $projects;
    }

    public function displayProjectItems(){
        $user = Auth::getUser();
        $projects = Project::where('user_id', $user['id'])->where('slug',$this->param('slug'))->get();
        //dd($projects);
        return $projects;
    }

    public function getProject(){
        $project = Project::where('slug', $this->param('slug'))->first();
        //dd($project);
        return $project;
    }

    public function getProjectItem(){
        $projectItem = ProjectItem::where('slug', $this->param('slug'))->first();
        //dd($project);
        return $projectItem;
    }

    public function onProjectEdit(){
        $user = Auth::getUser();
        $project = Project::where('slug',$this->param('slug'))->where('user_id',$user['id'])->first();
        $project->project_name = Input::get('project_name');
        $project->slug = preg_replace('~[^\pL\d]+~u', '-', Input::get('project_name'));
        $project->project_description = Input::get('project_description');
        $project->project_image = Input::file('project_image');
        $project->user_id = $user['id'];
        $project->save();
        Flash::success('Project Successfully Created!');
        return Redirect::refresh();
    }

    public function onProjectItemEdit(){
        $user = Auth::getUser();
        $projectItem = ProjectItem::where('slug',$this->param('slug'))->first();
        $projectItem->title = Input::get('title');
        $projectItem->slug = preg_replace('~[^\pL\d]+~u', '-', Input::get('project_name'));
        $projectItem->description = Input::get('description');
        $projectItem->project_item_image = Input::file('project_item_image');
        $projectItem->save();
        Flash::success('Project Successfully Created!');
        return Redirect::refresh();
    }

    public $project;
    public $user;
    public $projects;
    public $projectItem;
    public $projectsProfile;
}