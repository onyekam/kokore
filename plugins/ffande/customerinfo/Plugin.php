<?php namespace Ffande\Customerinfo;

use System\Classes\PluginBase;
use Rainlab\User\Controllers\Users as UsersController;
use Rainlab\User\Models\User as UserModel;
use Ffande\Procurement\Models\Order;
use Ffande\Procurement\Models\Product;
use Ffande\Procurement\Models\Cart;
use Ffande\Customerinfo\Models\NewsletterList;
use Cookie;
use Auth;
use Log;
use Event;

class Plugin extends PluginBase
{
    public $elevated = true;
    public function registerComponents()
    {
        return [
            'Ffande\Customerinfo\Components\Projects' => 'Projects',
            'Ffande\Customerinfo\Components\Getcustomerinfo' => 'Getcustomerinfo',
            'Ffande\Customerinfo\Components\Community' => 'Community',
            'Ffande\Customerinfo\Components\Newsletterusers' => 'Newsletterusers'
    	];
    }

    public function registerSettings()
    {
    }

    public function boot() {

//         Event::listen('*', function($o1 = null, $o2 = null, $o3 = null ){

//             echo( "<br/>" . Event::firing() );

// });

        UserModel::extend(function($model){
            $model->addFillable([
                'display_name',
                'phone_number',
                'address1',
                'address2',
                'city',
                'state',
                'country',
                'type_id',
                'bio',
                'available',
                'newsletter',
                'exclusives',
                'instagram',
                'terms'
            ]);

            Event::listen('rainlab.user.login', function($user) {
                //dd($user);
                if ($user->newsletter == 1) {
                    $newsletter = NewsletterList::where('email',$user->email)->first();
                    if (!$newsletter) {
                        $newNewsletterUser = New NewsletterList;
                        $newNewsletterUser->email = $user->email;
                        $newNewsletterUser->save();
                    }
                    $userModel = UserModel::where('id', $user->id)->first();
                }
                $currentCookie = Cookie::get('cartCookie');
                $cartItemWithCookie = Cart::where('cookie', $currentCookie)->get();
                foreach ($cartItemWithCookie as $cartItem) {
                    $cartItem->user_id = $user->id;
                    $cartItem->save();
                }
                Cookie::queue(\Cookie::forget('cartCookie'));
                \Log::info('Cart Updated');
            });

            // UserModel::extend(function($model){
            //     $model->bindEvent('auth.login', function() use ($model) {
            //             $user = Auth::getUser();
            //             dd($user);
            //             $currentCookie = Cookie::get('cartCookie');
            //             $cartItemWithCookie = Cart::where('cookie', $currentCookie)->get();
            //             foreach ($cartItemWithCookie as $cartItem) {
            //                 $cartItem->user_id = $user->id;
            //                 $cartItem->save();
            //             }
            //             \Log::info('Cart Updated');
                    
            //     });
            // });

            $model->belongsTo['type'] = ['Ffande\Customerinfo\Models\Profession', 'key' =>'id', 'otherKey' => 'type_id'];
            $model->hasMany['prints'] = ['Ffande\Procurement\Models\Product'];
            // $model->hasMany['portfolio'] = ['Ffande\Procurement\Models\ArtistPortfolio'];
            $model->hasMany['coupons'] = ['Ffande\Procurement\Models\Coupon'];
            $model->hasMany['likes'] = ['BABA\Favoritebutton\Models\Favorite'];
            $model->hasMany['orders'] = ['Ffande\Procurement\Models\Order'];
            $model->hasMany['experiences'] = ['Ffande\Customerinfo\Models\Experience'];
            $model->hasMany['skills'] = ['Ffande\Customerinfo\Models\Skills'];
            $model->attachOne['backImage'] = ['System\Models\File'];
            
            $model->addDynamicMethod('awaitingDelivery', function() use ($model){
                $orders = Order::where('user_id', $model->id)->where('status_id','2')->orWhere('status_id','3')->get();
                $awaitingDelivery = count($orders);
                return $awaitingDelivery;
            });
            $model->addDynamicMethod('processedOrders', function() use ($model){
                $processedOrders = Order::where('user_id', $model->id)->where('status_id','3')->orWhere('status_id','4')->take(4)->get();
                return $processedOrders;
            });
            $model->addDynamicMethod('deliveredOrders', function() use ($model){
                $deliveredOrders = Order::where('user_id', $model->id)->Where('status_id','4')->get();
                return $deliveredOrders;
            });
            $model->addDynamicMethod('toBeProcessed', function() use ($model){
                $toBeProcessed = Order::where('user_id', $model->id)->Where('status_id','1')->take(3)->get();
                return $toBeProcessed;
            });
            $model->addDynamicMethod('notApproved', function() use ($model){
                $notApproved = Product::where('user_id', $model->id)->Where('published', 0)->get();
                return $notApproved;
            });
            $model->addDynamicMethod('notApprovedCount', function() use ($model){
                $notApproved = Product::where('user_id', $model->id)->Where('published', 0)->get();
                return count($notApproved);
            });
            $model->addDynamicMethod('liked', function() use ($model){
                $count = 0;
                $userProducts = Product::where('user_id', $model->id)->get();
                foreach($userProducts as $product){
                    $count += $product->favorites_count;
                }
                return $count;
            });
            $model->addDynamicMethod('printsUserLiked', function() use ($model){
                $userFavorites = $model->favoritesOf(Product::class);
                return $userFavorites;
            });
            

        
        });

    	UsersController::extendFormFields(function($form, $model, $context){
    		$form->addTabFields([
    			'phone_number' => [
    				'label' => 'Phone Number',
    				'type' => 'text',
    				'tab' => 'Profile'
                ],
                'address1' => [
    				'label' => 'Address line 1',
    				'type' => 'text',
    				'tab' => 'Profile'
                ],
                'address2' => [
    				'label' => 'Address line 2',
    				'type' => 'text',
    				'tab' => 'Profile'
                ],
                'city' => [
    				'label' => 'City',
    				'type' => 'text',
    				'tab' => 'Profile'
                ],
                'state' => [
    				'label' => 'State',
    				'type' => 'text',
    				'tab' => 'Profile'
                ],
                'country' => [
    				'label' => 'Country',
    				'type' => 'text',
    				'tab' => 'Profile'
                ],
                'type_id' => [
    				'label' => 'Profession',
    				'type' => 'text',
    				'tab' => 'Profile'
                ],
                'display_name' => [
    				'label' => 'Display Name',
    				'type' => 'text',
    				'tab' => 'Profile'
                ],
                'instagram' => [
    				'label' => 'Instagram',
    				'type' => 'text',
    				'tab' => 'Profile'
                ],
                'bio' => [
    				'label' => 'Bio',
    				'type' => 'text',
    				'tab' => 'Profile'
                ],
                'available' => [
    				'label' => 'Available',
    				'type' => 'checkbox',
    				'tab' => 'Profile'
                ],
                'backImage' => [
    				'label' => 'Header Image',
    				'type' => 'fileupload',
    				'tab' => 'Profile'
                ],
                'newsletter' => [
    				'label' => 'Newsletter',
    				'type' => 'checkbox',
    				'tab' => 'Profile'
                ],
                'exclusives' => [
    				'label' => 'Exclusives',
    				'type' => 'checkbox',
    				'tab' => 'Profile'
                ]

    		]);

    	});

    }
}
