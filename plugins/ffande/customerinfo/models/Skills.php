<?php namespace Ffande\Customerinfo\Models;

use Model;

/**
 * Model
 */
class Skills extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ffande_customerinfo_skills';

    public $belongsTo = [
        'user' => 'Rainlab\User\Models\User'
    ];
}