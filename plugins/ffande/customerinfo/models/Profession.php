<?php namespace Ffande\Customerinfo\Models;

use Model;

/**
 * Model
 */
class Profession extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ffande_customerinfo_profession';

    public $hasMany = [
        'User' => ['Rainlab\User\Models\User', 'key' => 'profession_id', 'otherKey' => 'id']
    ];
}