<?php namespace Ffande\Customerinfo\Models;

use Model;

/**
 * Model
 */
class NewsletterList extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ffande_customerinfo_newsletterlists';
}