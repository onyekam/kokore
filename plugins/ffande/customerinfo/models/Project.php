<?php namespace Ffande\Customerinfo\Models;

use Model;

/**
 * Model
 */
class Project extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ffande_customerinfo_projects';

    public $attachOne = [
        'project_image' => 'System\Models\File'
    ];

    public $belongsTo = [
        'user' => 'Rainlab\User\Models\User'
    ];

    public $hasMany = [
        'projectItem' => 'Ffande\Customerinfo\Models\ProjectItem'
    ];
}