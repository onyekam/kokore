<?php namespace Ffande\Customerinfo\Models;

use Model;

/**
 * Model
 */
class ProjectItem extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ffande_customerinfo_project_item';

    public $attachOne = [
        'project_item_image' => ['System\Models\File']
    ];

    public $belongsTo = [
        'project' => 'Ffande\Customerinfo\Models\Project'
    ];
}