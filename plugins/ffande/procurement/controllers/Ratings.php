<?php namespace Ffande\Procurement\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Ratings extends Controller
{
    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\ReorderController'];
    
    public $listConfig = 'config_list.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Ffande.Procurement', 'main-menu-item2');
    }
}