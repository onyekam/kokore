<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementDpackages extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_dpackages', function($table)
        {
            $table->string('package_name');
            $table->increments('id')->unsigned(false)->change();
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_dpackages', function($table)
        {
            $table->dropColumn('package_name');
            $table->increments('id')->unsigned()->change();
        });
    }
}
