<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementSizes2 extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_sizes', function($table)
        {
            $table->string('sizeinnumbers');
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_sizes', function($table)
        {
            $table->dropColumn('sizeinnumbers');
        });
    }
}
