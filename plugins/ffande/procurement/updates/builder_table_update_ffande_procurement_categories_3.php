<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementCategories3 extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_categories', function($table)
        {
            $table->boolean('home_item_category')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_categories', function($table)
        {
            $table->dropColumn('home_item_category');
        });
    }
}
