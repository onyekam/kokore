<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteFfandeProcurementProducts extends Migration
{
    public function up()
    {
        Schema::dropIfExists('ffande_procurement_products');
    }
    
    public function down()
    {
        Schema::create('ffande_procurement_products', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('sku', 255)->nullable();
            $table->string('product_name', 255)->nullable();
            $table->text('product_description')->nullable();
            $table->integer('category_id')->nullable();
            $table->string('slug', 255)->nullable();
            $table->boolean('published')->nullable()->default(0);
            $table->integer('manufacturer_id')->nullable();
            $table->string('price', 255)->nullable();
        });
    }
}
