<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateFfandeProcurementProjectSolutionsAnnotations extends Migration
{
    public function up()
    {
        Schema::create('ffande_procurement_project_solutions_annotations', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('project_solution_item_id')->nullable();
            $table->string('context')->nullable();
            $table->text('text')->nullable();
            $table->string('width')->nullable();
            $table->string('height')->nullable();
            $table->string('x')->nullable();
            $table->string('y')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ffande_procurement_project_solutions_annotations');
    }
}
