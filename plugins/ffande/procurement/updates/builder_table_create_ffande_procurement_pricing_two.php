<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateFfandeProcurementPricingTwo extends Migration
{
    public function up()
    {
        Schema::create('ffande_procurement_pricing_two', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->decimal('base_price', 10, 2);
            $table->decimal('5x7', 10, 2)->nullable();
            $table->decimal('8x12', 10, 2)->nullable();
            $table->decimal('12x16', 10, 2)->nullable();
            $table->decimal('16x20', 10, 2)->nullable();
            $table->decimal('20x20', 10, 2)->nullable();
            $table->decimal('20x28', 10, 2)->nullable();
            $table->decimal('24x36', 10, 2)->nullable();
            $table->decimal('28x39', 10, 2)->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ffande_procurement_pricing_two');
    }
}
