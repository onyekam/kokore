<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementCoupon extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_coupon', function($table)
        {
            $table->integer('artist_id')->nullable();
            $table->increments('id')->unsigned(false)->change();
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_coupon', function($table)
        {
            $table->dropColumn('artist_id');
            $table->increments('id')->unsigned()->change();
        });
    }
}
