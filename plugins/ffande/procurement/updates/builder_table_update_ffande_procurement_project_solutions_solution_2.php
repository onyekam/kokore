<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementProjectSolutionsSolution2 extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_project_solutions_solution', function($table)
        {
            $table->string('title');
            $table->string('slug');
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_project_solutions_solution', function($table)
        {
            $table->dropColumn('title');
            $table->dropColumn('slug');
        });
    }
}
