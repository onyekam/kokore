<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementPricingTwo extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_pricing_two', function($table)
        {
            $table->decimal('5_7', 10, 2)->nullable();
            $table->decimal('8_12', 10, 2)->nullable();
            $table->decimal('12_16', 10, 2)->nullable();
            $table->decimal('16_20', 10, 2)->nullable();
            $table->decimal('20_20', 10, 2)->nullable();
            $table->decimal('20_28', 10, 2)->nullable();
            $table->decimal('24_36', 10, 2)->nullable();
            $table->decimal('28_39', 10, 2)->nullable();
            $table->dropColumn('5x7');
            $table->dropColumn('8x12');
            $table->dropColumn('12x16');
            $table->dropColumn('16x20');
            $table->dropColumn('20x20');
            $table->dropColumn('20x28');
            $table->dropColumn('24x36');
            $table->dropColumn('28x39');
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_pricing_two', function($table)
        {
            $table->dropColumn('5_7');
            $table->dropColumn('8_12');
            $table->dropColumn('12_16');
            $table->dropColumn('16_20');
            $table->dropColumn('20_20');
            $table->dropColumn('20_28');
            $table->dropColumn('24_36');
            $table->dropColumn('28_39');
            $table->decimal('5x7', 10, 2)->nullable();
            $table->decimal('8x12', 10, 2)->nullable();
            $table->decimal('12x16', 10, 2)->nullable();
            $table->decimal('16x20', 10, 2)->nullable();
            $table->decimal('20x20', 10, 2)->nullable();
            $table->decimal('20x28', 10, 2)->nullable();
            $table->decimal('24x36', 10, 2)->nullable();
            $table->decimal('28x39', 10, 2)->nullable();
        });
    }
}
