<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementPortfolio extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_portfolio', function($table)
        {
            $table->integer('user_id')->nullable();
            $table->increments('id')->unsigned(false)->change();
            $table->string('name')->nullable()->change();
            $table->text('description')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_portfolio', function($table)
        {
            $table->dropColumn('user_id');
            $table->increments('id')->unsigned()->change();
            $table->string('name', 255)->nullable(false)->change();
            $table->text('description')->nullable(false)->change();
        });
    }
}
