<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateFfandeProcurementCouponUser extends Migration
{
    public function up()
    {
        Schema::create('ffande_procurement_coupon_user', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('customer_id');
            $table->integer('coupon_id');
            $table->primary(['customer_id','coupon_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ffande_procurement_coupon_user');
    }
}
