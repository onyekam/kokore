<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementDorders6 extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_dorders', function($table)
        {
            $table->decimal('amount', 10, 2)->nullable()->unsigned(false)->default(0.00)->change();
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_dorders', function($table)
        {
            $table->double('amount', 10, 0)->nullable()->unsigned(false)->default(null)->change();
        });
    }
}
