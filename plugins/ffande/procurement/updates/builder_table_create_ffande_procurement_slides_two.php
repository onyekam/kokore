<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateFfandeProcurementSlidesTwo extends Migration
{
    public function up()
    {
        Schema::create('ffande_procurement_slides_two', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('heading');
            $table->text('subheading');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ffande_procurement_slides_two');
    }
}
