<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateFfandeProcurementCoupon extends Migration
{
    public function up()
    {
        Schema::create('ffande_procurement_coupon', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('code')->nullable();
            $table->string('value')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ffande_procurement_coupon');
    }
}
