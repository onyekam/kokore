<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementPortfolio2 extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_portfolio', function($table)
        {
            $table->string('slug');
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_portfolio', function($table)
        {
            $table->dropColumn('slug');
        });
    }
}
