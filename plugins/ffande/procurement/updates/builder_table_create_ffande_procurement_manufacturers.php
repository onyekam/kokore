<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateFfandeProcurementManufacturers extends Migration
{
    public function up()
    {
        Schema::create('ffande_procurement_manufacturers', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('manufacturer_name')->nullable();
            $table->text('manufacturer_description')->nullable();
            $table->boolean('published')->nullable()->default(0);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ffande_procurement_manufacturers');
    }
}
