<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateFfandeProcurementFormatsProducts extends Migration
{
    public function up()
    {
        Schema::create('ffande_procurement_formats_products', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('product_id');
            $table->integer('format_id');
            $table->primary(['product_id','format_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ffande_procurement_formats_products');
    }
}
