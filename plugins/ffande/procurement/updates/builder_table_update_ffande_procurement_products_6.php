<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementProducts6 extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_products', function($table)
        {
            $table->boolean('published')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_products', function($table)
        {
            $table->dropColumn('published');
        });
    }
}
