<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementProjectSolutionsSolution4 extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_project_solutions_solution', function($table)
        {
            $table->renameColumn('project_solution_id', 'project_solutions_id');
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_project_solutions_solution', function($table)
        {
            $table->renameColumn('project_solutions_id', 'project_solution_id');
        });
    }
}
