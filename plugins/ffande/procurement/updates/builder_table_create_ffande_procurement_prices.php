<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateFfandeProcurementPrices extends Migration
{
    public function up()
    {
        Schema::create('ffande_procurement_prices', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('product_id');
            $table->integer('size_id');
            $table->decimal('price', 10, 0);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ffande_procurement_prices');
    }
}
