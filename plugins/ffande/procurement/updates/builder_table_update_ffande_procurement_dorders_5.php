<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementDorders5 extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_dorders', function($table)
        {
            $table->smallInteger('status_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_dorders', function($table)
        {
            $table->dropColumn('status_id');
        });
    }
}
