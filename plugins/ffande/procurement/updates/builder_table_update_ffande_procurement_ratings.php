<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementRatings extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_ratings', function($table)
        {
            $table->increments('id')->unsigned(false)->change();
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_ratings', function($table)
        {
            $table->increments('id')->unsigned()->change();
        });
    }
}
