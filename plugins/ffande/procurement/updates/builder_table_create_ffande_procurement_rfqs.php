<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateFfandeProcurementRfqs extends Migration
{
    public function up()
    {
        Schema::create('ffande_procurement_rfqs', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('project_name')->nullable();
            $table->string('project_location')->nullable();
            $table->string('additional_comments')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ffande_procurement_rfqs');
    }
}
