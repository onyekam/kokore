<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementProducts18 extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_products', function($table)
        {
            $table->decimal('price', 13, 4)->nullable()->unsigned(false)->default(0.00)->change();
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_products', function($table)
        {
            $table->string('price', 255)->nullable()->unsigned(false)->default(null)->change();
        });
    }
}
