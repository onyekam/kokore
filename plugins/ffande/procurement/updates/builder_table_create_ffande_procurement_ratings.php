<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateFfandeProcurementRatings extends Migration
{
    public function up()
    {
        Schema::create('ffande_procurement_ratings', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('product_id')->nullable();
            $table->integer('rating')->nullable();
            $table->text('comment')->nullable();
            $table->integer('user_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ffande_procurement_ratings');
    }
}
