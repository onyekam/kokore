<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementSlidesTwo extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_slides_two', function($table)
        {
            $table->string('button_label')->nullable();
            $table->string('button_link')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_slides_two', function($table)
        {
            $table->dropColumn('button_label');
            $table->dropColumn('button_link');
        });
    }
}
