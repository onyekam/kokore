<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateFfandeProcurementOffers extends Migration
{
    public function up()
    {
        Schema::create('ffande_procurement_offers', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('offer_name');
            $table->string('slug');
            $table->text('offer_description');
            $table->smallInteger('offer_discount')->nullable();
            $table->string('color_theme')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ffande_procurement_offers');
    }
}
