<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementCart2 extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_cart', function($table)
        {
            $table->integer('price_id')->nullable();
            $table->integer('frame_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_cart', function($table)
        {
            $table->dropColumn('price_id');
            $table->dropColumn('frame_id');
        });
    }
}
