<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateFfandeProcurementProjectSolutionsSolution extends Migration
{
    public function up()
    {
        Schema::create('ffande_procurement_project_solutions_solution', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('project_solution_id')->nullable();
            $table->text('description')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ffande_procurement_project_solutions_solution');
    }
}
