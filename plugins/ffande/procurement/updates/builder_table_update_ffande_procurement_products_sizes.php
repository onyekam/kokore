<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementProductsSizes extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_products_sizes', function($table)
        {
            $table->decimal('percentage_value', 10, 2)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_products_sizes', function($table)
        {
            $table->dropColumn('percentage_value');
        });
    }
}
