<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementRfqs2 extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_rfqs', function($table)
        {
            $table->string('slug');
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_rfqs', function($table)
        {
            $table->dropColumn('slug');
        });
    }
}
