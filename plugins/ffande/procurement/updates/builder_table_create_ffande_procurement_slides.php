<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateFfandeProcurementSlides extends Migration
{
    public function up()
    {
        Schema::create('ffande_procurement_slides', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('header')->nullable();
            $table->string('subheader')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ffande_procurement_slides');
    }
}
