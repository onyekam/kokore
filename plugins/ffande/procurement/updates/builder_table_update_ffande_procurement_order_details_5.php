<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementOrderDetails5 extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_order_details', function($table)
        {
            $table->string('border', 12)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_order_details', function($table)
        {
            $table->dropColumn('border');
        });
    }
}
