<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementProducts4 extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_products', function($table)
        {
            $table->string('product_image_link')->nullable();
            $table->string('product_reference')->nullable();
            $table->string('product_excerpt')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_products', function($table)
        {
            $table->dropColumn('product_image_link');
            $table->dropColumn('product_reference');
            $table->dropColumn('product_excerpt');
        });
    }
}
