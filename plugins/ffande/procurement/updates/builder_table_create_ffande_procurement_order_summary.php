<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateFfandeProcurementOrderSummary extends Migration
{
    public function up()
    {
        Schema::create('ffande_procurement_order_summary', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->nullable()->unsigned();
            $table->double('total', 10, 0)->nullable();
            $table->integer('payment_method_id')->nullable()->unsigned();
            $table->integer('delivery_terms_id')->nullable()->unsigned();
            $table->integer('status_id')->nullable()->unsigned();
            $table->string('address')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('city')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ffande_procurement_order_summary');
    }
}
