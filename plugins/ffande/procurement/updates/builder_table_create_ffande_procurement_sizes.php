<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateFfandeProcurementSizes extends Migration
{
    public function up()
    {
        Schema::create('ffande_procurement_sizes', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('size');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ffande_procurement_sizes');
    }
}
