<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementProjectSolutionsSolution3 extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_project_solutions_solution', function($table)
        {
            $table->string('title', 255)->nullable()->change();
            $table->string('slug', 255)->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_project_solutions_solution', function($table)
        {
            $table->string('title', 255)->nullable(false)->change();
            $table->string('slug', 255)->nullable(false)->change();
        });
    }
}
