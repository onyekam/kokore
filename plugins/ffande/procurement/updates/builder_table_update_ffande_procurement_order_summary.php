<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementOrderSummary extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_order_summary', function($table)
        {
            $table->integer('coupon_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_order_summary', function($table)
        {
            $table->dropColumn('coupon_id');
        });
    }
}
