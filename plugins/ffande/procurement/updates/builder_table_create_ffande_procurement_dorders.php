<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateFfandeProcurementDorders extends Migration
{
    public function up()
    {
        Schema::create('ffande_procurement_dorders', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('package_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ffande_procurement_dorders');
    }
}
