<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateFfandeProcurementProducts extends Migration
{
    public function up()
    {
        Schema::create('ffande_procurement_products', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('sku')->nullable();
            $table->string('product_name')->nullable();
            $table->text('product_description')->nullable();
            $table->integer('category_id')->nullable();
            $table->string('slug')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ffande_procurement_products');
    }
}
