<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateFfandeProcurementCart extends Migration
{
    public function up()
    {
        Schema::create('ffande_procurement_cart', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('user_id')->nullable()->unsigned();
            $table->integer('product_id')->nullable()->unsigned();
            $table->integer('quantity')->nullable()->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ffande_procurement_cart');
    }
}
