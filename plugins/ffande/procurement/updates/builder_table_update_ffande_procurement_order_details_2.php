<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementOrderDetails2 extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_order_details', function($table)
        {
            $table->integer('size_id');
            $table->integer('frame_id');
            $table->integer('color_id');
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_order_details', function($table)
        {
            $table->dropColumn('size_id');
            $table->dropColumn('frame_id');
            $table->dropColumn('color_id');
        });
    }
}
