<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateFfandeProcurementTenders extends Migration
{
    public function up()
    {
        Schema::create('ffande_procurement_tenders', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title_of_project')->nullable();
            $table->string('project_location')->nullable();
            $table->integer('manufacturer_id')->nullable();
            $table->string('tender_period')->nullable();
            $table->string('preferred_delivery_terms')->nullable();
            $table->text('comments_questions')->nullable();
            $table->string('project_type')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ffande_procurement_tenders');
    }
}
