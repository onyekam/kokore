<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateFfandeProcurementFrames extends Migration
{
    public function up()
    {
        Schema::create('ffande_procurement_frames', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('frame');
            $table->string('slug');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ffande_procurement_frames');
    }
}
