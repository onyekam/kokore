<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementOrderSummary4 extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_order_summary', function($table)
        {
            $table->string('transref')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_order_summary', function($table)
        {
            $table->dropColumn('transref');
        });
    }
}
