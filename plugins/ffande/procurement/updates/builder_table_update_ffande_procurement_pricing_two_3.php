<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementPricingTwo3 extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_pricing_two', function($table)
        {
            $table->decimal('five_by_seven', 10, 2)->nullable();
            $table->decimal('eight_by_twelve', 10, 2)->nullable();
            $table->decimal('twelve_by_sixteen', 10, 2)->nullable();
            $table->decimal('sixteen_by_twenty', 10, 2)->nullable();
            $table->decimal('twenty_by_twenty', 10, 2)->nullable();
            $table->decimal('twenty_by_twenty_eight', 10, 2)->nullable();
            $table->decimal('twenty_four_by_thirty_six', 10, 2)->nullable();
            $table->decimal('twenty_eight_by_thirty_nine', 10, 2)->nullable();
            $table->dropColumn('57');
            $table->dropColumn('812');
            $table->dropColumn('1216');
            $table->dropColumn('1620');
            $table->dropColumn('2020');
            $table->dropColumn('2028');
            $table->dropColumn('2436');
            $table->dropColumn('2839');
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_pricing_two', function($table)
        {
            $table->dropColumn('five_by_seven');
            $table->dropColumn('eight_by_twelve');
            $table->dropColumn('twelve_by_sixteen');
            $table->dropColumn('sixteen_by_twenty');
            $table->dropColumn('twenty_by_twenty');
            $table->dropColumn('twenty_by_twenty_eight');
            $table->dropColumn('twenty_four_by_thirty_six');
            $table->dropColumn('twenty_eight_by_thirty_nine');
            $table->decimal('57', 10, 2)->nullable();
            $table->decimal('812', 10, 2)->nullable();
            $table->decimal('1216', 10, 2)->nullable();
            $table->decimal('1620', 10, 2)->nullable();
            $table->decimal('2020', 10, 2)->nullable();
            $table->decimal('2028', 10, 2)->nullable();
            $table->decimal('2436', 10, 2)->nullable();
            $table->decimal('2839', 10, 2)->nullable();
        });
    }
}
