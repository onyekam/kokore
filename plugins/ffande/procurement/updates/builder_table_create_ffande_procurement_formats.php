<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateFfandeProcurementFormats extends Migration
{
    public function up()
    {
        Schema::create('ffande_procurement_formats', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('format');
            $table->string('slug');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ffande_procurement_formats');
    }
}
