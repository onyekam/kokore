<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateFfandeProcurementOrderShipping extends Migration
{
    public function up()
    {
        Schema::create('ffande_procurement_order_shipping', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('order_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->string('address');
            $table->string('city');
            $table->string('phone_number');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ffande_procurement_order_shipping');
    }
}
