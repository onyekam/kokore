<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementManufacturers2 extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_manufacturers', function($table)
        {
            $table->string('manufacturer_image_url')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_manufacturers', function($table)
        {
            $table->dropColumn('manufacturer_image_url');
        });
    }
}
