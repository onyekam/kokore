<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementCategories extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_categories', function($table)
        {
            $table->renameColumn('parent_category', 'parent_category_id');
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_categories', function($table)
        {
            $table->renameColumn('parent_category_id', 'parent_category');
        });
    }
}
