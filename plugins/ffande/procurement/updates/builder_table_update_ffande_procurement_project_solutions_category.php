<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementProjectSolutionsCategory extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_project_solutions_category', function($table)
        {
            $table->string('description')->nullable();
            $table->string('icon')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_project_solutions_category', function($table)
        {
            $table->dropColumn('description');
            $table->dropColumn('icon');
        });
    }
}
