<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateFfandeProcurementCategories extends Migration
{
    public function up()
    {
        Schema::create('ffande_procurement_categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('category_name')->nullable();
            $table->string('category_description')->nullable();
            $table->string('slug')->nullable();
            $table->integer('parent_category')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ffande_procurement_categories');
    }
}
