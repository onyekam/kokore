<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementOrderSummary2 extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_order_summary', function($table)
        {
            $table->double('discounted_total', 10, 0)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_order_summary', function($table)
        {
            $table->dropColumn('discounted_total');
        });
    }
}
