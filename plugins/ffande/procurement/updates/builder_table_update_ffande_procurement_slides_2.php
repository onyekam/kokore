<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementSlides2 extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_slides', function($table)
        {
            $table->boolean('decor_services')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_slides', function($table)
        {
            $table->dropColumn('decor_services');
        });
    }
}
