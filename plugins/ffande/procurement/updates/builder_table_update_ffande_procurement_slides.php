<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementSlides extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_slides', function($table)
        {
            $table->string('button_label')->nullable();
            $table->string('button_link')->nullable();
            $table->increments('id')->unsigned(false)->change();
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_slides', function($table)
        {
            $table->dropColumn('button_label');
            $table->dropColumn('button_link');
            $table->increments('id')->unsigned()->change();
        });
    }
}
