<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementCart4 extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_cart', function($table)
        {
            $table->string('cookie')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_cart', function($table)
        {
            $table->dropColumn('cookie');
        });
    }
}
