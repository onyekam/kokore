<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateFfandeProcurementProjectSolutions extends Migration
{
    public function up()
    {
        Schema::create('ffande_procurement_project_solutions', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->string('description');
            $table->integer('category_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ffande_procurement_project_solutions');
    }
}
