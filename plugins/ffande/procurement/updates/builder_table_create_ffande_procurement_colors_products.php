<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateFfandeProcurementColorsProducts extends Migration
{
    public function up()
    {
        Schema::create('ffande_procurement_colors_products', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('product_id');
            $table->integer('color_id');
            $table->primary(['product_id','color_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ffande_procurement_colors_products');
    }
}
