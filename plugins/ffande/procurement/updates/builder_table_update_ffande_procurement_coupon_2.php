<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementCoupon2 extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_coupon', function($table)
        {
            $table->renameColumn('artist_id', 'user_id');
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_coupon', function($table)
        {
            $table->renameColumn('user_id', 'artist_id');
        });
    }
}
