<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementDorders4 extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_dorders', function($table)
        {
            $table->string('transref')->nullable();
            $table->double('amount', 10, 0)->nullable();
            $table->integer('user_id')->nullable()->change();
            $table->integer('package_id')->nullable()->change();
            $table->string('l_name', 255)->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_dorders', function($table)
        {
            $table->dropColumn('transref');
            $table->dropColumn('amount');
            $table->integer('user_id')->nullable(false)->change();
            $table->integer('package_id')->nullable(false)->change();
            $table->string('l_name', 255)->nullable(false)->change();
        });
    }
}
