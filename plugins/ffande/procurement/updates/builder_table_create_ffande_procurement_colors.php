<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateFfandeProcurementColors extends Migration
{
    public function up()
    {
        Schema::create('ffande_procurement_colors', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('color');
            $table->string('slug');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ffande_procurement_colors');
    }
}
