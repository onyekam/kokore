<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateFfandeProcurementHomelinks extends Migration
{
    public function up()
    {
        Schema::create('ffande_procurement_homelinks', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('link')->nullable();
            $table->string('label')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ffande_procurement_homelinks');
    }
}
