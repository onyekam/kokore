<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementCategories2 extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_categories', function($table)
        {
            $table->integer('id')->change();
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_categories', function($table)
        {
            $table->increments('id')->change();
        });
    }
}
