<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementProjectSolutionsAnnotations2 extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_project_solutions_annotations', function($table)
        {
            $table->string('project_solution_url')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_project_solutions_annotations', function($table)
        {
            $table->dropColumn('project_solution_url');
        });
    }
}
