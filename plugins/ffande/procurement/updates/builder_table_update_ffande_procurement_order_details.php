<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementOrderDetails extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_order_details', function($table)
        {
            $table->double('price', 10, 0)->nullable();
            $table->double('total', 10, 0)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_order_details', function($table)
        {
            $table->dropColumn('price');
            $table->dropColumn('total');
        });
    }
}
