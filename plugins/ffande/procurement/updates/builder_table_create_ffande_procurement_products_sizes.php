<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateFfandeProcurementProductsSizes extends Migration
{
    public function up()
    {
        Schema::create('ffande_procurement_products_sizes', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('product_id');
            $table->integer('size_id');
            $table->primary(['product_id','size_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ffande_procurement_products_sizes');
    }
}
