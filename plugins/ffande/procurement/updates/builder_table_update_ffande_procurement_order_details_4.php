<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementOrderDetails4 extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_order_details', function($table)
        {
            $table->integer('frame_id')->nullable()->change();
            $table->integer('size_id')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_order_details', function($table)
        {
            $table->integer('frame_id')->nullable(false)->change();
            $table->integer('size_id')->nullable(false)->change();
        });
    }
}
