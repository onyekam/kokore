<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementDorders extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_dorders', function($table)
        {
            $table->string('f_name')->nullable();
            $table->string('l_name');
            $table->text('message');
            $table->string('email');
            $table->increments('id')->unsigned(false)->change();
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_dorders', function($table)
        {
            $table->dropColumn('f_name');
            $table->dropColumn('l_name');
            $table->dropColumn('message');
            $table->dropColumn('email');
            $table->increments('id')->unsigned()->change();
        });
    }
}
