<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateFfandeProcurementProjectSolutionsCategory extends Migration
{
    public function up()
    {
        Schema::create('ffande_procurement_project_solutions_category', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('category');
            $table->string('slug');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ffande_procurement_project_solutions_category');
    }
}
