<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementProjectSolutions2 extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_project_solutions', function($table)
        {
            $table->text('description')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_project_solutions', function($table)
        {
            $table->string('description', 255)->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
}
