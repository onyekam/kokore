<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementPricingTwo2 extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_pricing_two', function($table)
        {
            $table->decimal('57', 10, 2)->nullable();
            $table->decimal('812', 10, 2)->nullable();
            $table->decimal('1216', 10, 2)->nullable();
            $table->decimal('1620', 10, 2)->nullable();
            $table->decimal('2020', 10, 2)->nullable();
            $table->decimal('2028', 10, 2)->nullable();
            $table->decimal('2436', 10, 2)->nullable();
            $table->decimal('2839', 10, 2)->nullable();
            $table->dropColumn('5_7');
            $table->dropColumn('8_12');
            $table->dropColumn('12_16');
            $table->dropColumn('16_20');
            $table->dropColumn('20_20');
            $table->dropColumn('20_28');
            $table->dropColumn('24_36');
            $table->dropColumn('28_39');
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_pricing_two', function($table)
        {
            $table->dropColumn('57');
            $table->dropColumn('812');
            $table->dropColumn('1216');
            $table->dropColumn('1620');
            $table->dropColumn('2020');
            $table->dropColumn('2028');
            $table->dropColumn('2436');
            $table->dropColumn('2839');
            $table->decimal('5_7', 10, 2)->nullable();
            $table->decimal('8_12', 10, 2)->nullable();
            $table->decimal('12_16', 10, 2)->nullable();
            $table->decimal('16_20', 10, 2)->nullable();
            $table->decimal('20_20', 10, 2)->nullable();
            $table->decimal('20_28', 10, 2)->nullable();
            $table->decimal('24_36', 10, 2)->nullable();
            $table->decimal('28_39', 10, 2)->nullable();
        });
    }
}
