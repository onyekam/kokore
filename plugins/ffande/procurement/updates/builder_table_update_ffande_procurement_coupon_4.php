<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementCoupon4 extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_coupon', function($table)
        {
            $table->dropColumn('customer_id');
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_coupon', function($table)
        {
            $table->integer('customer_id')->nullable();
        });
    }
}
