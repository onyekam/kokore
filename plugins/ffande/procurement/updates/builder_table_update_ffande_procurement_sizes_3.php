<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementSizes3 extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_sizes', function($table)
        {
            $table->string('sizeintext');
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_sizes', function($table)
        {
            $table->dropColumn('sizeintext');
        });
    }
}
