<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementCart7 extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_cart', function($table)
        {
            $table->double('price', 10, 0)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_cart', function($table)
        {
            $table->dropColumn('price');
        });
    }
}
