<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementProjectSolutions3 extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_project_solutions', function($table)
        {
            $table->dropColumn('description');
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_project_solutions', function($table)
        {
            $table->text('description');
        });
    }
}
