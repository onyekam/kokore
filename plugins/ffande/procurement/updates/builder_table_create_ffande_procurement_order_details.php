<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateFfandeProcurementOrderDetails extends Migration
{
    public function up()
    {
        Schema::create('ffande_procurement_order_details', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('order_id')->nullable()->unsigned();
            $table->integer('product_id')->nullable()->unsigned();
            $table->integer('quantity')->nullable()->unsigned();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ffande_procurement_order_details');
    }
}
