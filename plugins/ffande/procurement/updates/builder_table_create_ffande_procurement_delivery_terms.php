<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateFfandeProcurementDeliveryTerms extends Migration
{
    public function up()
    {
        Schema::create('ffande_procurement_delivery_terms', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ffande_procurement_delivery_terms');
    }
}
