<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementProducts10 extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_products', function($table)
        {
            $table->renameColumn('sale', 'sale_amount');
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_products', function($table)
        {
            $table->renameColumn('sale_amount', 'sale');
        });
    }
}
