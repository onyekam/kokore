<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementOrderDetails3 extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_order_details', function($table)
        {
            $table->integer('color_id')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_order_details', function($table)
        {
            $table->integer('color_id')->nullable(false)->change();
        });
    }
}
