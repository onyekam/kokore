<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateFfandeProcurementDpackages extends Migration
{
    public function up()
    {
        Schema::create('ffande_procurement_dpackages', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->double('cost', 10, 0);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ffande_procurement_dpackages');
    }
}
