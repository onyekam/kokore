<?php namespace Ffande\Procurement\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFfandeProcurementDorders3 extends Migration
{
    public function up()
    {
        Schema::table('ffande_procurement_dorders', function($table)
        {
            $table->string('phone');
        });
    }
    
    public function down()
    {
        Schema::table('ffande_procurement_dorders', function($table)
        {
            $table->dropColumn('phone');
        });
    }
}
