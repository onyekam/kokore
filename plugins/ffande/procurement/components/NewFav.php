<?php namespace Ffande\Procurement\Components;

use Cms\Classes\ComponentBase;
use ApplicationException;
use Auth;
use Lang;

class Newfav extends ComponentBase {

    public function componentDetails() {
        return [
            'name' => 'New Favorite Button',
            'description' => 'render a favorite button'
        ];
    }

    public function defineProperties() {
        return [
            'print_id' => [
                'description' => 'print id',
                'title' => 'print_id',
                'default' => '',
                'type' => 'string',
            ],
            'model_class' => [
                'title' => 'Model Class Fullname',
                'description' => 'Model class fullname, ex "\Ffande\Procurement\Models\Product" ',
                'type' => 'dropdown',
                'default' => '\Ffande\Procurement\Models\Product',
                'required' => true,
            ],
        ];
    }

    public function getModel_ClassOptions() {
        return ['\Ffande\Procurement\Models\Product' => 'Print'];
    }
   public function init() {
        $this->addCss('assets/favoritebutton.css');
    }
    public function onRender() {
        $this->page['isfavorited'] = 0;
        $this->page['haspost'] = 1;
        $print_id = $this->property('print_id');
        if (!empty($print_id) && is_numeric($print_id)) {
            $this->page['print_id'] = $print_id;
            $model = $this->property('model_class');
            $print = $model::find($print_id);
            if (!$print) {
                throw new ApplicationException('print id not found in ' . $model);
            }
            $user = Auth::getUser();
            if ($user && $print->isFavorited($user)) {
                $this->page['isfavorited'] = 1;
            }
        }else{
            $this->page['haspost'] = 0;
        }
    }

    public function onFavoritePost() {
        if (!$user = Auth::getUser()) {
            throw new ApplicationException(Lang::get(/* You must be logged in first! */'rainlab.user::lang.account.login_first'));
        }
        $this->page['haspost'] = 1;
        $print_id = post('print_id');
        $this->page['isfavorited'] = 0;
        if (!empty($print_id) && is_numeric($print_id)) {
            $model = $this->property('model_class');
            $print = $model::find($print_id);
            if ($print) {
                $res = $print->toggleFavorite($user); // auth user added to favorites this post
                if ($print->isFavorited($user)) {
                    $this->page['isfavorited'] = 1;
                }
            }else{
                $this->page['haspost'] = 0;
            }
        }
    }

}
