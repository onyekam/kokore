<?php namespace Ffande\Procurement\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Ffande\Procurement\Models\Manufacturer;
use Ffande\Procurement\Models\Product;
use Ffande\Procurement\Models\Price;
use Ffande\Procurement\Models\Frame;
use Ffande\Procurement\Models\Color;
use Redirect; 
use DB;
use Session;
use Flash;
use Ffande\Procurement\Models\OrderDetails;


class DisplaySingleProduct extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Display Manufacturers',
			'description' => 'List all manufacturers on manufacturers page'
		];
	}



	public function onRun(){
		$this->peopleAlsoBought = $this->displayOtherBoughtItems();
		if (Session::has('cart')) {
			
		}
        
        if ($this->param('slug')) {
			
				$this->displayProduct();
				// $this->relatedProducts();
				$this->frames = $this->frames();
				$this->colors = $this->getColors();

			
			//$this->displayManufacturerProducts();	
        } else {
        	$this->manufacturers = Manufacturer::all();
        }
        

	}
	
	public function displayOtherBoughtItems(){
		$boughtItems = OrderDetails::select('product_id')->orderByRaw("RAND()")->distinct()->take(4)->get();
		return $boughtItems;
	}

    public function displayProduct(){
		$this->product = Product::where('slug', $this->param('slug'))->first();
		$this->prices = Price::where('product_id', $this->product->id)->get();
		$productsizes = $this->product->sizes;
		$productsizes = $productsizes->toArray();
		if(!$this->product->homeitem){
			$this->productSizes = [
				'5 x 7' => $this->product->base_price->five_by_seven,
				'8 x 12' => $this->product->base_price->eight_by_twelve,
				'12 x 16' => $this->product->base_price->twelve_by_sixteen,
				'16 x 20' => $this->product->base_price->sixteen_by_twenty,
				'20 x 20' => $this->product->base_price->twenty_by_twenty,
				'20 x 38' => $this->product->base_price->twenty_by_twenty_eight,
				'24 x 36' => $this->product->base_price->twenty_four_by_thirty_six,
				'28 x 39' => $this->product->base_price->twenty_eight_by_thirty_nine
			];
			$temp = [];
			foreach ($productsizes as $size) {
				foreach ($this->productSizes as $key => $value) {
					if ($size['sizeinnumbers'] == $key) {
						$temp[$key] = $value;
					}
				}
			}
			$this->productSizes = $temp;
		}
		
		//dd($this->product->ratings);
		$this->productRatings = $this->product->ratings;
		$ratingSum = 0;
		foreach($this->productRatings as $rating){
			$ratingSum += $rating->rating; 
		}
		if(count($this->productRatings) == 0 ){
			$this->averageRating = $ratingSum / 1;
		} else {
			$this->averageRating = $ratingSum / count($this->productRatings);
		}
		
	}

	public function relatedProducts() {
		$product = Product::where('slug', $this->param('slug'))->first();
		if ($product) {
			$this->categoryId = $product->category->id;
			if ($product->homeitem) {
				$this->relatedProducts = Product::where('category_id', $this->categoryId)->where('homeitem', 1)->take(4)->orderBy(Db::raw('RAND()'))->get();
			} else {
				$this->relatedProducts = Product::where('category_id', $this->categoryId)->take(4)->orderBy(Db::raw('RAND()'))->get();
			}
		}
		//$this->relatedArtisans = Artisan::where('category_id',$this->categoryId)->take(4)->orderBy(Db::raw('RAND()'))->get();
	}	

	public function getColors() {
		$colors = Color::all();
		return $colors;
	}

	public function frames(){
		$frames = Frame::all();
		return $frames;
	}

	public function onSizeSelectModal(){
		$product = Product::where('id', post('product'))->first();
		$sizePrice = post('sizeprice');
		if ($product->sale_off) {
			$oldPrice = $sizePrice;
			$sizePrice = $sizePrice - ($sizePrice * $product->sale_amount / 100);	
			return [
				'#productbaseprice' => $this->renderPartial('@sizeprice', ['sizePrice' => $sizePrice, 'oldPrice' => $oldPrice]),
				'#priceupdate' => $this->renderPartial('@hiddeninputprice', ['sizePrice' => $sizePrice])
			   ];
		} else {
			return [
				'#productbaseprice' => $this->renderPartial('@sizeprice', ['sizePrice' => $sizePrice]),
				'#priceupdate' => $this->renderPartial('@hiddeninputprice', ['sizePrice' => $sizePrice])
			];
		}
	}

	public function onSizeSelect(){
		$product = Product::where('id', post('product'))->first();
		$sizePrice = post('sizeprice');
		if ($product->sale_off) {
			$oldPrice = $sizePrice;
			$sizePrice = $sizePrice - ($sizePrice * $product->sale_amount / 100);	
			return [
				'#productbaseprice' => $this->renderPartial('@sizeprice', ['sizePrice' => $sizePrice, 'oldPrice' => $oldPrice]),
				'#priceupdate' => $this->renderPartial('@hiddeninputprice', ['sizePrice' => $sizePrice])
			];
		} else {
			return [
				'#productbaseprice' => $this->renderPartial('@sizeprice', ['sizePrice' => $sizePrice]),
				'#priceupdate' => $this->renderPartial('@hiddeninputprice', ['sizePrice' => $sizePrice])
			];
		}
	}

	// public function formats(){
	// 	$formats = Format::all();
	// 	return $formats;
	// }
	// public function displayManufacturerProducts(){

    // 	$this->products = Product::where('manufacturer_id', $this->manufacturer->id)->get();
    // }
		//public $manufacturers;
	public $product;
	public $productSizes; 
	public $categoryId;
	public $prices;
	// public $relatedProducts;
	public $relatedHomeItemProducts;
	public $frames;
	public $colors;
	public $productRatings;
	public $averageRating;
	public $peopleAlsoBought;
	// public $frames;
	//public $products;

}