<?php namespace Ffande\Procurement\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Ffande\Procurement\Models\Manufacturer;
use Ffande\Procurement\Models\Product;
use Ffande\Procurement\Models\Annotations;
use Redirect; 
use Session;
use Response;


class Annotation extends ComponentBase {
    public function componentDetails(){
		return [
			'name' => 'Annotations',
			'description' => 'Display & Create Annotations'
		];
    }
    
    public function onRun(){

    }

    public function onSaveAnnotation(){
     // dd(post());
      //$data = post();
    
      $annotation = new Annotations;
      $annotation->text = post('text');
      $annotation->context = post('context');
      $annotation->width = post('width');
      $annotation->height = post('height');
      $annotation->x = post('x');
      $annotation->y = post('y');
      $annotation->project_solution_url = post('project_solution_url');
      $annotation->save();
      return Response::json(['response' => 'Annotation saved successfully']);

    
    
    }

    public function onGetAnnotations(){
      $annotations = Annotations::where('project_solution_url',post('url'))->get();
      return Response::json($annotations);
    }
}