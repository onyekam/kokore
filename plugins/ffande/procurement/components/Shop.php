<?php namespace Ffande\Procurement\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Ffande\Procurement\Models\Manufacturer;
use Ffande\Procurement\Models\Product;
use Ffande\Procurement\Models\Category;
use Ffande\Procurement\Models\Frame;
use Ffande\Procurement\Models\Size;
use Ffande\Procurement\Models\Format;
use Ffande\Procurement\Models\Color;
use Ffande\Procurement\Models\Price;
use Ffande\Procurement\Models\Pricingtwo;
use Redirect; 
use Url;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;


class Shop extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Display Shop',
			'description' => 'List all products'
		];
	}

	public function onRun(){
		$this->getColorFilters();
		$this->colors = $this->getColors();
        if ($this->page->id == 'home' ) {
			$this->prints = $this->homePrints();
			$this->printsSales = $this->homeSales();
			$this->frames = $this->frames();
			$this->formats = $this->formats();
		}
		else if ($this->page->id == 'home-items' ) {
			$this->homeItems = $this->homeItems();
			$this->colors = $this->getColors();
			$this->homeItemCategories = $this->homeItemCategories();
		}
		else if ($this->page->id == 'perfect-pair' ) {
			$this->perfectPair = $this->perfectPair();
		}
		else if ($this->page->id == 'perfect-pair-confirm') {
			$this->prints = $this->displaySelectedPerfectPair();
			//$this->productCategories = $this->loadCategories();
		}
		else if ($this->page->id == 'register') {
			$this->prints = $this->randomPrints();
			//$this->productCategories = $this->loadCategories();
        }
        else if ($this->param('slug')) {
			$this->productCategories = $this->loadCategories();
			$this->frames = $this->frames();
			$this->formats = $this->formats();
			$this->colors = $this->getColors();

		} 
		else if($this->param('category-slug')){
			$this->prints = $this->displayCategoryPrints();
		}
		else {
			$this->prints = Product::where('homeitem',0)->where('perfect_pair',0)->paginate(9);
			$this->productCategories = $this->loadCategories();
			$this->frames = $this->frames();
			$this->formats = $this->formats();
			$this->colors = $this->getColors();
			$this->sizes = $this->sizes();
        }
	}
	
	public function displaySelectedPerfectPair(){
		$perfectPair = Product::where('slug', $this->param('slug') )->where('perfect_pair',1)->first();
		return $perfectPair;
	}

	public function homePrints(){
		$prints = Product::where('sale_off',0)->where('perfect_pair', 0)->orderByRaw("RAND()")->where('published',1)->where('trending',1)->take(8)->get();
		return $prints;
	}

	public function homeItems(){
		$prints = Product::where('homeitem', 1)->paginate(8);
		return $prints;
	}

	public function homeSales(){
		$prints = Product::where('sale_off',1)->orderBy('created_at','desc')->where('published',1)->take(5)->get();
		return $prints;
	}

	public function perfectPair(){
		$prints = Product::where('perfect_pair',1)->where('published',1)->paginate(6);
		return $prints;
	}

	public function randomPrints(){
		$prints = Product::where('perfect_pair',0)->where('published',1)->where('homeitem',0)->orderByRaw("RAND()")->take(5)->get();
		return $prints;
	}

	public function onView(){
		$this->printModal = Product::where('slug', post('slug') )->first();
		$this->prices = Price::where('product_id', $this->printModal->id)->get();
		$this->colors = $this->getColors();
		$this->frames = $this->frames();
		$this->formats = $this->formats();
		$this->sizes = $this->sizes();
		$productsizes = $this->printModal->sizes;
		$productsizes = $productsizes->toArray();
		$this->productSizes = [
			'5 x 7' => $this->printModal->base_price->five_by_seven,
			'8 x 12' => $this->printModal->base_price->eight_by_twelve,
			'12 x 16' => $this->printModal->base_price->twelve_by_sixteen,
			'16 x 20' => $this->printModal->base_price->sixteen_by_twenty,
			'20 x 20' => $this->printModal->base_price->twenty_by_twenty,
			'20 x 38' => $this->printModal->base_price->twenty_by_twenty_eight,
			'24 x 36' => $this->printModal->base_price->twenty_four_by_thirty_six,
			'28 x 39' => $this->printModal->base_price->twenty_eight_by_thirty_nine
		];
		$temp = [];
		foreach ($productsizes as $size) {
			foreach ($this->productSizes as $key => $value) {
				if ($size['sizeinnumbers'] == $key) {
					$temp[$key] = $value;
				}
			}
		}
		$this->productSizes = $temp;
		return [
			'#quickModal' => $this->renderPartial('Shop::modalPartial')
		];
	}

	public function onViewGrids(){
		$homeItem = $this->printModal = Product::where('slug', post('slug') )->first();
		//$this->prices = Price::where('product_id', $this->printModal->id)->get();
		$this->colors = $homeItem->colors;
		$this->frames = $this->frames();
		$this->formats = $this->formats();
		$this->sizes = $this->sizes();
		return [
			'#homeitemsModal' => $this->renderPartial('Shop::homeItemsPartial')
		];
	}

	public function displayCategoryPrints(){
		$productCategory = Category::where('slug', $this->param('category-slug'))->first();
		$print = Product::where('category_id', $productCategory->id)->paginate(9);
		$prints = new Collection;
		$prints->push($print);
		if (!$productCategory->parent_category_id) {
			$categories = $productCategory->categories;
			//print_r($categories);
			//dd($categories);
			//$productCollection = new Collection();
			foreach ($categories as $category) {
				//return var_dump($category);
				$print = Product::where('category_id', $category->id)->get();
				$prints->push($print);
				//dd($product;
				//var_dump($product);
				//array_push($product,$product);
			}
			$this->category = $productCategory;
			$prints = $prints->flatten();
			$paginatedProducts = new LengthAwarePaginator($this->getPaginatorSlice($prints), count($prints), 9);
			return $paginatedProducts->setPath(Url::to($this->currentPageUrl()));
		}
		$this->category = $productCategory;
		$prints = Product::where('category_id', $productCategory->id)->paginate(9);
		return $prints;
	}

	public function onPrints(){
		$prints = Product::where('homeitem','0')->where('perfect_pair',0)->where('published',1)->get();
		return [
        	'#printsPartial' => $this->renderPartial('@printsPartial',[ 'prints' => $prints])
    	];
	}

	public function onCategoryPrints(){
		$productCategory = Category::where('slug', Input::post('slug'))->first();
		$print = Product::where('category_id', $productCategory->id)->paginate(9);
		$prints = new Collection;
		$prints->push($print);
		if (!$productCategory->parent_category_id) {
			$categories = $productCategory->categories;
			//print_r($categories);
			//dd($categories);
			//$productCollection = new Collection();
			foreach ($categories as $category) {
				//return var_dump($category);
				$print = Product::where('category_id', $category->id)->get();
				$prints->push($print);
				//dd($product;
				//var_dump($product);
				//array_push($product,$product);
			}
			$this->category = $productCategory;
			$prints = $prints->flatten();
			$paginatedProducts = new LengthAwarePaginator($this->getPaginatorSlice($prints), count($prints), 9);
			return $paginatedProducts->setPath(Url::to($this->currentPageUrl()));
		}
		$this->category = $productCategory;
		$prints = Product::where('category_id', $productCategory->id)->paginate(9);
		return [
        	'#printsPartial' => $this->renderPartial('@printsPartial',[ 'prints' => $prints])
    	];
		//$this->prints;
	}

	protected function getPaginatorSlice($results) {
        return $results->slice((\Request::get('page', 1) - 1) * 9, 9);
	}
	
	public function getPageUrl()
    {
        // Component sits in cms page
        if (isset($this->page->settings['url'])) {
            return $this->page->settings['url'];
        }
        // Component sits in static page via snippet
        if (isset($this->page->apiBag['staticPage'])) {
            return $this->page->apiBag['staticPage']->viewBag['url'];
        }
        return '';
    }

	public function onCategorySelect(){
		$category = Input::get('category');
		return Redirect::to('shop/'.$category);
	}

    public function displayProduct(){
		$this->product = Product::where('slug', $this->param('slug'))->first();
		
	}

	public function loadCategories() {
		$categories = Category::where('home_item_category','0')->get();
		return $categories;
	}

	public function homeItemCategories() {
		$categories = Category::where('home_item_category','1')->get();
		return $categories;
	}

	public function loadChildCategories(){}

	public function flattenArray($array) {
		$arrayValues = array();
		foreach (new RecursiveIteratorIterator( new RecursiveArrayIterator($array)) as $val) {
			$arrayValues[] = $val;
		}
 		return $arrayValues;
	} 

	public function getColorFilters() {
		$dir = themes_path().'/kokore/assets/images/color-filter';
		$array_files = scandir($dir);
		unset($array_files[0]);
		unset($array_files[1]);
		$this->colorFiles = [];
		$this->colorNames = [];
		foreach ($array_files as $file) {
			array_push($this->colorFiles, $file);
		}
		foreach ($this->colorFiles as $name) {
			array_push($this->colorNames, substr($name, 0, -4));
		}
	}

	public function getColors() {
		$colors = Color::all();
		return $colors;
	}

	public function frames(){
		$frames = Frame::all();
		return $frames;
	}

	public function formats(){
		$formats = Format::all();
		return $formats;
	}

	public function sizes(){
		$sizes = Size::all();
		return $sizes;
	}

// function flattenArrayIndexed()
	
	// public function displayManufacturerProducts(){

    // 	$this->products = Product::where('manufacturer_id', $this->manufacturer->id)->get();
    // }
	//public $manufacturers;
	public $colorFiles = [];
	public $colorNames = [];
	public $product;
	public $products;
	public $products2 = [];
	public $category;
	public $productCategories;
	public $prints;
	public $printsSales;
	public $perfectPair;
	public $printModal;
	public $homeItems;
	public $homeItemCategories;
	public $frames;
	public $colors;
	public $formats;
	public $sizes;
	public $prices;
	public $productSizes;

}