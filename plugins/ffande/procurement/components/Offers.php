<?php namespace Ffande\Procurement\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Ffande\Procurement\Models\Manufacturer;
use Ffande\Procurement\Models\Product;
use Ffande\Procurement\Models\Annotations;
use Ffande\Procurement\Models\SlideTwo as Slide;
use Redirect; 
use Session;
use Response;
use Network\Http;
use Ffande\Procurement\Models\Offer;


class Offers extends ComponentBase {
    public function componentDetails(){
		return [
			'name' => 'List of Offers',
			'description' => 'Display list of Custom Collections and offers'
		];
    }
    
    public function onRun(){
        
       
            $this->offers = $this->getOffers();
    
        //dd($code);

    }

    public function getOffers(){
        $offers = Offer::all();
        return $offers;
    }

    public $offers;

}