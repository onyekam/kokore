<?php namespace Ffande\Procurement\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Ffande\Procurement\Models\Manufacturer;
use Ffande\Procurement\Models\Product;
use Ffande\Procurement\Models\Category;
use Ffande\Procurement\Models\Frame;
use Ffande\Procurement\Models\Size;
use Ffande\Procurement\Models\Coupon;
use Redirect; 
use Session;
use ValidationException;
use ApplicationException;
use Validator;
use Ffande\Procurement\Models\Order;
use Ffande\Procurement\Models\OrderDetails;
use Ffande\Procurement\Models\OrderShipping;
use Onyekam\Payment\Components\PaystackPayment;
use Auth;
use Renatio\DynamicPDF\Classes\PDF;
use Mail;
use Request;
use Ffande\Procurement\Models\Cart;
use Cookie;

class Checkout extends ComponentBase {
	public function componentDetails(){
		return [
			'name' => 'Checkout Process',
			'description' => 'Finish placing order for product'
		];
	}
	public function onRun() {
        $cart = $this->dbCart();
        //return $this->onPDF();
        if(Session::has('pdf')){
            $pdf = Session::get('pdf');
            Session::forget('pdf');
            return $pdf;
        }  
    }
    /**
     * Returns a cart, if available, 
     * 
     */
    public function cart() {
        if (!$cart = Session::has('cart') ){
            return null;
        }
        $cart = Session::get('cart');
        return $cart;
    }

    public function dbCart()
    {	
		$user = Auth::getUser();
		if($user){
			$cart = Cart::where('user_id', $user->id)->get();
		} else {
			$cart = Cart::where('cookie', Cookie::get('cartCookie'))->get();
		}
		$this->dbCartTotal = $this->getDbCartTotal();
      return $cart;
   }
   
   public function getDbCartTotal() {
		$user = Auth::getUser();
		$cartTotal = 0;
		if($user){
			$cart = Cart::where('user_id', $user->id)->get();
		} else {
			$cart = Cart::where('cookie', Cookie::get('cartCookie'))->get();
		}
		foreach($cart as $cartItem){
			$cartTotal += $cartItem->total;
		}
		return $cartTotal;
	}

    public function onApplyCoupon() {
      $cart = $this->dbCart();
      if (post('coupon')) {
         $coupon = Coupon::where('code', post('coupon'))->first();
         if ($coupon) {
            $user = Auth::getUser();
            $usedCoupon = Order::where('coupon_id',$coupon->id)->where('user_id', $user->id)->first();
            if ($usedCoupon) {
               return [
                  "#coupondiscount" => "You can only use a coupon once and you have already used this coupon"
               ];
            } else {
               $couponDiscount = $this->dbCartTotal - ( $this->dbCartTotal * $coupon->value / 100);
               return [
                  "#coupondiscount" => $this->renderPartial('@coupondiscount', ['coupondiscount' => $couponDiscount, 'couponcode' => $coupon->code])
               ];
            }
         } else {
            return [
               "#coupondiscount" => $this->renderPartial('@nocouponfound')
            ];
         }
      } else {
         return [
            "#coupondiscount" => "You did not enter a coupon code"
         ];
      }
    }

    public function onSubmitCheckout() {
      $cart = $this->dbCart();  
      $checkoutData = Input::all();
        //return $checkoutData;
        //$cart = $this->cart();
        //dd($checkoutData);
        if ($checkoutData) {
            $rules = [
                'first_name_shipping' => 'required',
                'last_name_shipping' => 'required',
                'address_shipping1' => 'required',
                'email_shipping' => 'required',
                'phone_number_shipping' => 'required',
                'city_shipping' => 'required'
            ];
        }
        $validator = Validator::make($checkoutData, $rules);
        if ($validator->fails()) {
            \Flash::error('Something went wrong, please fill all the required fields');
            return Redirect::refresh()->withErrors($validator);
        }
        $order = new Order;
        $orderDetails = new OrderDetails;
        $user = Auth::getUser(); 
        $order->user_id = $user->id;
      //   if (Input::get('cod')) {
      //    $order->payment_method_id = 1;
      //   } else {
      //    $order->payment_method_id = 2;
      //   }
        $order->status_id = 1;
        if ($this->dbCartTotal >= 35000) {
           $shippingCost = 0;
        } else {
           $shippingCost = 1000;
        }
        $order->total = $this->dbCartTotal + $shippingCost;
        $order->address = $checkoutData["address_shipping1"]." ".$checkoutData["address_shipping2"];
        $order->phone_number = $checkoutData["phone_number_shipping"]; 
        $order->city = $checkoutData["city_shipping"];

        if (Input::get('coupon_code')) {
            $couponCode = Input::get('coupon_code');
            $coupon = Coupon::where('code', $couponCode)->first();
            if ($coupon) {
                $discount_amount = ($coupon->value / 100) * $this->dbCartTotal;
                $order->discount_amount = $discount_amount;
                $order->coupon_id = $coupon->id;
                $order->discounted_total = $this->dbCartTotal - $discount_amount + $shippingCost;
            }
        } else {
            $order->total = $this->dbCartTotal + $shippingCost;
        }
         $order->save();
         
         $shippingInfo = new OrderShipping;
         $shippingInfo->order_id = $order->id;
         $shippingInfo->first_name = $checkoutData["first_name_shipping"];
         $shippingInfo->last_name = $checkoutData["last_name_shipping"];
         $shippingInfo->email = $checkoutData["email_shipping"];
         $shippingInfo->address = $checkoutData["address_shipping1"]." ".$checkoutData["address_shipping2"];
         $shippingInfo->phone_number = $checkoutData["phone_number_shipping"]; 
         $shippingInfo->city = $checkoutData["city_shipping"] ;
         $shippingInfo->save();
        $this->newOrder = $order;
   
        foreach($cart as $cartKeyItem) {
            $orderDetails = new OrderDetails;
            $orderDetails->order_id = $order->id;
            $orderDetails->product_id = $cartKeyItem->product->id;
            $orderDetails->quantity = $cartKeyItem["quantity"];
            $orderDetails->border = $cartKeyItem["border"];
            if (!$cartKeyItem->product->homeitem) {
               $size = Size::where('sizeinnumbers',$cartKeyItem['size'])->first();
               $orderDetails->frame_id = $cartKeyItem["frame_id"];
               $orderDetails->size_id = $size->id;
            } else {
               $orderDetails->size_id = 1;
               $orderDetails->frame_id = 1;
            }
            $orderDetails->price = $cartKeyItem["price"];
               $orderDetails->total = $cartKeyItem["total"];
               $orderDetails->save();
            
        }
      //   foreach($order->orderDetails as $orderDetail){
      //       $this->sendArtistNotification($orderDetail->product_id->)
      //   }
        $this->emptyCart($user->id);
        \Flash::success('You have successfully ordered for your items');
        $this->sendOrderConfirmation();
        return Redirect::to('/confirm-pay'.'/'.$order->id);
    }

    public function emptyCart($userid) {
		if($userid){
			$cart = Cart::where('user_id', $userid)->get();
		} else {
			$cart = Cart::where('cookie', Cookie::get('cartCookie'))->get();
		}
		foreach ($cart as $cartItem) {
			$cartItem->delete();
		}
    }

   

    public function onPDF(){
        $cart = $this->cart();
        $cartTotal = Session::get('cartTotal');
        $tableRow = "";
        $tableOpen = "<table ><thead><tr><th >Product Image</th><th >Product</th><th >Total</th></tr></thead><tbody>";
        foreach($cart as $cartItem){
            $tableRow .= "<tr><td ><img src='$cartItem[product_image]' width='50' /></td><td >$cartItem[product] &nbsp;<strong >× $cartItem[quantity]</strong></td><td ><span ><span>NGN </span>$cartItem[cost]</span></td></tr>";
        }
        $tableFoot = "</tbody><tfoot><tr><th>Subtotal</th><td></td><td><span><span>NGN </span>$cartTotal</span></td></tr><tr><th>Total</th><td></td><td><strong><span><span>NGN </span>$cartTotal</span><input type='hidden' value='$cartTotal' name='total' /></strong></td></tr></tfoot>";
        $tableClose = "</table>";
        $cartStyled = $tableOpen.$tableRow.$tableFoot.$tableClose;
        $checkoutData = ['cart' => $cartStyled];
        $templateCode = 'checkout'; // unique code of the template
        $data = ['name' => 'John Doe']; // optional data used in template
        $pdf = PDF::loadTemplate($templateCode, $checkoutData)->stream('download.pdf');
        Session::put('pdf', $pdf);
        return Redirect::refresh();
        //return PDF::loadTemplate($templateCode, $checkoutData)->stream('download.pdf');
    }

    public function preparePDF(){
        $cart = $this->cart();
        $cartTotal = Session::get('cartTotal');
        $tableRow = "";
        $tableOpen = "<table ><thead><tr><th >Product Image</th><th >Product</th><th >Total</th></tr></thead><tbody>";
        foreach($cart as $cartItem){
            $tableRow .= "<tr><td ><img src='$cartItem[product_image]' width='50' /></td><td >$cartItem[product] &nbsp;<strong >× $cartItem[quantity]</strong></td><td ><span ><span>NGN </span>$cartItem[cost]</span></td></tr>";
        }
        $tableFoot = "</tbody><tfoot><tr><th>Subtotal</th><td></td><td><span><span>NGN </span>$cartTotal</span></td></tr><tr><th>Total</th><td></td><td><strong><span><span>NGN </span>$cartTotal</span><input type='hidden' value='$cartTotal' name='total' /></strong></td></tr></tfoot>";
        $tableClose = "</table>";
        $cartStyled = $tableOpen.$tableRow.$tableFoot.$tableClose;
        return $cartStyled;
    }

    public function prepareOrder() {
        $newOrder = $this->newOrder;
        $total = 0;
        $accountUrl = $_SERVER['HTTP_HOST'].'my-account/orders/';
        if ($this->newOrder->discounted_total) {
         $total = $this->newOrder->discounted_total.'(You used a Coupon Code)';
         } else {
            $total = $this->newOrder->total;
         };
         // $paymentMethod = $this->newOrder->payment_method->name;
        $order = "<table cellpadding='0' cellspacing='0' border='0' width='88%' style='width: 88% !important; min-width: 88%; max-width: 88%;'>
        <tr>
           <td align='left' valign='top'>
              <div style='height: 37px; line-height: 37px; font-size: 35px;'>&nbsp;</div>
              <font face='Source Sans Pro, sans-serif' color='#000000' style='font-size: 20px; line-height: 28px;'>
                 <span style='font-family: Source Sans Pro, Arial, Tahoma, Geneva, sans-serif; color: #000000; font-size: 20px; line-height: 28px;'>Hey ".$this->user->name.' '.$this->user->surname.",</span>
              </font>
              <div style='height: 22px; line-height: 22px; font-size: 20px;'>&nbsp;</div>
              <font face='Source Sans Pro, sans-serif' color='#000000' style='font-size: 20px; line-height: 28px;'>
                 <span style='font-family: Source Sans Pro, Arial, Tahoma, Geneva, sans-serif; color: #000000; font-size: 20px; line-height: 28px;'>Thank you for your order.<br/></span>
              </font>
              <div style='height: 22px; line-height: 22px; font-size: 20px;'>&nbsp;</div>
              <font face='Source Sans Pro, sans-serif' color='#000000' style='font-size: 20px; line-height: 28px;'>
                 <span style='font-family: Source Sans Pro, Arial, Tahoma, Geneva, sans-serif; color: #000000; font-size: 20px; line-height: 28px;'>Your order ID is <b>KKR ".$newOrder->id."</b> and the total amount is NGN ".$total.".<br/>You will find all the details about your order below or <a target='_blank' href='/".$accountUrl.$newOrder->id."'>here</a></span>
              </font>
              <div style='height: 50px; line-height: 50px; font-size: 48px;'>&nbsp;</div>
           </td>
        </tr>
     </table>";
     $order .= "<table cellpadding='0' cellspacing='0' border='0' width='88%' style='width: 88% !important; min-width: 88%; max-width: 88%;'>
        <tr>
           <td align='left' valign='top'>
              <!--[if (gte mso 9)|(IE)]>
              <table border='0' cellspacing='0' cellpadding='0'>
              <tr><td align='center' valign='top' width='309'><![endif]-->
              <div style='display: inline-block; vertical-align: top; width: 50%; min-width: 296px;'>
                 <table cellpadding='0' cellspacing='0' border='0' width='100%' style='width: 100% !important; min-width: 100%; max-width: 100%;'>
                    <tr>
                       <td align='left' valign='top' style='border-width: 1px; border-style: solid; border-color: #e8e8e8; border-top: none; border-left: none; border-right: none;'>
                          <font face='Source Sans Pro, sans-serif' color='#000000' style='font-size: 20px; line-height: 28px; font-weight: 600;'>
                             <span style='font-family: Source Sans Pro, Arial, Tahoma, Geneva, sans-serif; color: #000000; font-size: 20px; line-height: 28px; font-weight: 600;'>Order Number</span>
                          </font>
                          <div style='height: 10px; line-height: 10px; font-size: 8px;'>&nbsp;</div>
                       </td>
                    </tr>
                    <tr>
                       <td align='left' valign='top'>
                          <div style='height: 12px; line-height: 12px; font-size: 10px;'>&nbsp;</div>
                          <font face='Source Sans Pro, sans-serif' color='#000000' style='font-size: 20px; line-height: 28px;'>
                             <span style='font-family: Source Sans Pro, Arial, Tahoma, Geneva, sans-serif; color: #000000; font-size: 20px; line-height: 28px;'>Your Order id: ".$newOrder->id."</span>
                          </font>
                          <div style='height: 40px; line-height: 40px; font-size: 38px;'>&nbsp;</div>
                       </td>
                    </tr>
                 </table>
              </div><!--[if (gte mso 9)|(IE)]></td><td align='center' valign='top' width='309'><![endif]--><div style='display: inline-block; vertical-align: top; width: 50%; min-width: 296px;'>
                 <table cellpadding='0' cellspacing='0' border='0' width='100%' style='width: 100% !important; min-width: 100%; max-width: 100%;'>
                    <tr>
                       <td align='left' valign='top' style='border-width: 1px; border-style: solid; border-color: #e8e8e8; border-top: none; border-left: none; border-right: none;'>
                          <font face='Source Sans Pro, sans-serif' color='#000000' style='font-size: 20px; line-height: 28px; font-weight: 600;'>
                             <span style='font-family: Source Sans Pro, Arial, Tahoma, Geneva, sans-serif; color: #000000; font-size: 20px; line-height: 28px; font-weight: 600;'>Order Date</span>
                          </font>
                          <div style='height: 10px; line-height: 10px; font-size: 8px;'>&nbsp;</div>
                       </td>
                    </tr>
                    <tr>
                       <td align='left' valign='top'>
                          <div style='height: 12px; line-height: 12px; font-size: 10px;'>&nbsp;</div>
                          <font face='Source Sans Pro, sans-serif' color='#000000' style='font-size: 20px; line-height: 28px;'>
                             <span style='font-family: Source Sans Pro, Arial, Tahoma, Geneva, sans-serif; color: #000000; font-size: 20px; line-height: 28px;'> 17&nbsp;".$newOrder->created_at."</span>
                          </font>
                          <div style='height: 40px; line-height: 40px; font-size: 38px;'>&nbsp;</div>
                       </td>
                    </tr>
                 </table>
              </div>
              <!--[if (gte mso 9)|(IE)]>
              </td></tr>
              </table><![endif]-->
           </td>
        </tr>
     </table>";
    $order .= "<table cellpadding='0' cellspacing='0' border='0' width='88%' style='width: 88% !important; min-width: 88%; max-width: 88%;'>
        <tr>
           <td align='left' valign='top'>
              <!--[if (gte mso 9)|(IE)]>
              <table border='0' cellspacing='0' cellpadding='0'>
              <tr><td align='center' valign='top' width='309'><![endif]-->
              <div style='display: inline-block; vertical-align: top; width: 50%; min-width: 296px;'>
                 <table cellpadding='0' cellspacing='0' border='0' width='100%' style='width: 100% !important; min-width: 100%; max-width: 100%;'>
                    <tr>
                       <td align='left' valign='top' style='border-width: 1px; border-style: solid; border-color: #e8e8e8; border-top: none; border-left: none; border-right: none;'>
                          <font face='Source Sans Pro, sans-serif' color='#000000' style='font-size: 20px; line-height: 28px; font-weight: 600;'>
                             <span style='font-family: Source Sans Pro, Arial, Tahoma, Geneva, sans-serif; color: #000000; font-size: 20px; line-height: 28px; font-weight: 600;'>Shipping Address</span>
                          </font>
                          <div style='height: 10px; line-height: 10px; font-size: 8px;'>&nbsp;</div>
                       </td>
                    </tr>
                    <tr>
                       <td align='left' valign='top'>
                          <div style='height: 12px; line-height: 12px; font-size: 10px;'>&nbsp;</div>
                          <font face='Source Sans Pro, sans-serif' color='#000000' style='font-size: 20px; line-height: 28px;'>
                             <span style='font-family: Source Sans Pro, Arial, Tahoma, Geneva, sans-serif; color: #000000; font-size: 20px; line-height: 28px;'>" .$newOrder->address. "</span>
                          </font>
                          <div style='height: 40px; line-height: 40px; font-size: 38px;'>&nbsp;</div>
                       </td>
                    </tr>
                 </table>
              </div>
              <!--[if (gte mso 9)|(IE)]></td><td align='center' valign='top' width='309'><![endif]-->
              <!--[if (gte mso 9)|(IE)]>
              </td></tr>
              </table><![endif]-->
           </td>
        </tr>
     </table>
    <table cellpadding='0' cellspacing='0' border='0' width='88%' style='width: 88% !important; min-width: 88%; max-width: 88%;'>
        <tr>
           <td align='left' valign='top'>
              <font face='Source Sans Pro, sans-serif' color='#000000' style='font-size: 20px; line-height: 26px; font-weight: 600;'>
                 <span style='font-family: Source Sans Pro, Arial, Tahoma, Geneva, sans-serif; color: #000000; font-size: 20px; line-height: 26px; font-weight: 600;'>Here’s what you ordered:</span>
              </font>
              <div style='height: 12px; line-height: 12px; font-size: 10px;'>&nbsp;</div>
           </td>
        </tr>
     </table>";
     $order .= "<table cellpadding='0' cellspacing='0' border='0' width='88%' style='width: 88% !important; min-width: 88%; max-width: 88%; border-width: 1px; border-style: solid; border-color: #e8e8e8; border-top: none; border-left: none; border-right: none;'>
     <tr>
        <td align='left' valign='top' width='17%' style='width: 17%; max-width: 17%; min-width: 20px'>
           <div style='height: 10px; line-height: 10px; font-size: 10px;'>&nbsp;</div>
           <font face='Source Sans Pro, sans-serif' color='#000000' style='font-size: 20px; line-height: 28px; font-weight: 600;'>
              <span style='font-family: Source Sans Pro, Arial, Tahoma, Geneva, sans-serif; color: #000000; font-size: 20px; line-height: 28px; font-weight: 600;'>Item</span>
           </font>
           <div style='height: 10px; line-height: 10px; font-size: 10px;'>&nbsp;</div>
        </td>
        <td width='7' style='width: 7px; max-width: 7px; min-width: 7px;'>&nbsp;</td>
        <td align='left' valign='top' width='57%' style='width: 57%; max-width: 57%; min-width: 90px'>&nbsp;</td>
        <td width='7' style='width: 7px; max-width: 7px; min-width: 7px;'>&nbsp;</td>
        <td align='left' valign='top' width='10%' style='width: 10%; max-width: 10%; min-width: 40px'>
           <div style='height: 10px; line-height: 10px; font-size: 10px;'>&nbsp;</div>
           <font face='Source Sans Pro, sans-serif' color='#000000' style='font-size: 20px; line-height: 28px; font-weight: bold;'>
              <span style='font-family: Source Sans Pro, Arial, Tahoma, Geneva, sans-serif; color: #000000; font-size: 20px; line-height: 28px; font-weight: bold;'>Qty</span>
           </font>
           <div style='height: 10px; line-height: 10px; font-size: 10px;'>&nbsp;</div>
        </td>
        <td width='7' style='width: 7px; max-width: 7px; min-width: 7px;'>&nbsp;</td>
        <td align='right' valign='top' width='12%' style='width: 12%; max-width: 12%; min-width: 70px'>
           <div style='height: 10px; line-height: 10px; font-size: 10px;'>&nbsp;</div>
           <font face='Source Sans Pro, sans-serif' color='#000000' style='font-size: 20px; line-height: 28px; font-weight: bold;'>
              <span style='font-family: Source Sans Pro, Arial, Tahoma, Geneva, sans-serif; color: #000000; font-size: 20px; line-height: 28px; font-weight: bold;'>Price</span>
           </font>
           <div style='height: 10px; line-height: 10px; font-size: 10px;'>&nbsp;</div>
        </td>
     </tr>
  </table>";
    //$orderDetails = OrderDetails::where('order_id', $newOrder->id)->get();
  
    foreach($newOrder->order_details as $orderDetail){
       if(!$orderDetail->product->homeitem){
         $order .= "<table cellpadding='0' cellspacing='0' border='0' width='88%' style='width: 88% !important; min-width: 88%; max-width: 88%; border-width: 1px; border-style: solid; border-color: #e8e8e8; border-top: none; border-left: none; border-right: none;'>
        <tr>
           <td align='left' valign='middle' width='17%' style='width: 17%; max-width: 17%; min-width: 20px'>
              <div style='height: 10px; line-height: 10px; font-size: 10px;'>&nbsp;</div>
              <a href='#' target='_blank' style='display: block; max-width: 99px;'>
                 <img src='".$orderDetail->product->product_image->path."' alt='img' width='99' border='0' style='display: block; width: 99px; max-width: 100%;' />
              </a>
              <div style='height: 10px; line-height: 10px; font-size: 10px;'>&nbsp;</div>
           </td>
           <td width='7' style='width: 7px; max-width: 7px; min-width: 7px;'>&nbsp;</td>
           <td align='left' valign='top' width='57%' style='width: 57%; max-width: 57%; min-width: 90px'>
              <div style='height: 22px; line-height: 22px; font-size: 20px;'>&nbsp;</div>
              <font face='Source Sans Pro, sans-serif' color='#000000' style='font-size: 17px; line-height: 21px; font-weight: 600;'>
                 <span style='font-family: Source Sans Pro, Arial, Tahoma, Geneva, sans-serif; color: #000000; font-size: 17px; line-height: 21px; font-weight: 600;'>".$orderDetail->product->product_name." </span>
                 
                 <span style='font-family: Source Sans Pro, Arial, Tahoma, Geneva, sans-serif; color: #000000; font-size: 12px; line-height: 21px; font-weight: 600;'> ".$orderDetail->size->sizeinnumbers." ".$orderDetail->frame->frame." ".$orderDetail->border."</span>
              </font>
              <div style='height: 10px; line-height: 10px; font-size: 10px;'>&nbsp;</div>
           </td>
           <td width='7' style='width: 7px; max-width: 7px; min-width: 7px;'>&nbsp;</td>
           <td align='left' valign='top' width='10%' style='width: 10%; max-width: 10%; min-width: 40px'>
              <div style='height: 22px; line-height: 22px; font-size: 20px;'>&nbsp;</div>
              <font face='Source Sans Pro, sans-serif' color='#000000' style='font-size: 17px; line-height: 21px;'>
                 <span style='font-family: Source Sans Pro, Arial, Tahoma, Geneva, sans-serif; color: #000000; font-size: 17px; line-height: 21px;'>".$orderDetail->quantity."</span>
              </font>
              <div style='height: 10px; line-height: 10px; font-size: 10px;'>&nbsp;</div>
           </td>
           <td width='7' style='width: 7px; max-width: 7px; min-width: 7px;'>&nbsp;</td>
           <td align='right' valign='top' width='12%' style='width: 12%; max-width: 12%; min-width: 70px'>
              <div style='height: 22px; line-height: 22px; font-size: 20px;'>&nbsp;</div>
              <font face='Source Sans Pro, sans-serif' color='#000000' style='font-size: 17px; line-height: 21px;'>
                 <span style='font-family: Source Sans Pro, Arial, Tahoma, Geneva, sans-serif; color: #000000; font-size: 17px; line-height: 21px;'>NGN ".$orderDetail->price."</span>
              </font>
              <div style='height: 10px; line-height: 10px; font-size: 10px;'>&nbsp;</div>
           </td>
        </tr>
     </table>";
       } else {
         $order .= "<table cellpadding='0' cellspacing='0' border='0' width='88%' style='width: 88% !important; min-width: 88%; max-width: 88%; border-width: 1px; border-style: solid; border-color: #e8e8e8; border-top: none; border-left: none; border-right: none;'>
        <tr>
           <td align='left' valign='middle' width='17%' style='width: 17%; max-width: 17%; min-width: 20px'>
              <div style='height: 10px; line-height: 10px; font-size: 10px;'>&nbsp;</div>
              <a href='#' target='_blank' style='display: block; max-width: 99px;'>
                 <img src='".$orderDetail->product->product_image->path."' alt='img' width='99' border='0' style='display: block; width: 99px; max-width: 100%;' />
              </a>
              <div style='height: 10px; line-height: 10px; font-size: 10px;'>&nbsp;</div>
           </td>
           <td width='7' style='width: 7px; max-width: 7px; min-width: 7px;'>&nbsp;</td>
           <td align='left' valign='top' width='57%' style='width: 57%; max-width: 57%; min-width: 90px'>
              <div style='height: 22px; line-height: 22px; font-size: 20px;'>&nbsp;</div>
              <font face='Source Sans Pro, sans-serif' color='#000000' style='font-size: 17px; line-height: 21px; font-weight: 600;'>
                 <span style='font-family: Source Sans Pro, Arial, Tahoma, Geneva, sans-serif; color: #000000; font-size: 17px; line-height: 21px; font-weight: 600;'>".$orderDetail->product->product_name." </span>
              </font>
              <div style='height: 10px; line-height: 10px; font-size: 10px;'>&nbsp;</div>
           </td>
           <td width='7' style='width: 7px; max-width: 7px; min-width: 7px;'>&nbsp;</td>
           <td align='left' valign='top' width='10%' style='width: 10%; max-width: 10%; min-width: 40px'>
              <div style='height: 22px; line-height: 22px; font-size: 20px;'>&nbsp;</div>
              <font face='Source Sans Pro, sans-serif' color='#000000' style='font-size: 17px; line-height: 21px;'>
                 <span style='font-family: Source Sans Pro, Arial, Tahoma, Geneva, sans-serif; color: #000000; font-size: 17px; line-height: 21px;'>".$orderDetail->quantity."</span>
              </font>
              <div style='height: 10px; line-height: 10px; font-size: 10px;'>&nbsp;</div>
           </td>
           <td width='7' style='width: 7px; max-width: 7px; min-width: 7px;'>&nbsp;</td>
           <td align='right' valign='top' width='12%' style='width: 12%; max-width: 12%; min-width: 70px'>
              <div style='height: 22px; line-height: 22px; font-size: 20px;'>&nbsp;</div>
              <font face='Source Sans Pro, sans-serif' color='#000000' style='font-size: 17px; line-height: 21px;'>
                 <span style='font-family: Source Sans Pro, Arial, Tahoma, Geneva, sans-serif; color: #000000; font-size: 17px; line-height: 21px;'>NGN ".$orderDetail->price."</span>
              </font>
              <div style='height: 10px; line-height: 10px; font-size: 10px;'>&nbsp;</div>
           </td>
        </tr>
     </table>";
       }
       
    }

    $order .= "<table cellpadding='0' cellspacing='0' border='0' width='88%' style='width: 88% !important; min-width: 88%; max-width: 88%; border-width: 4px; border-style: solid; border-color: #000000; border-top: none; border-left: none; border-right: none;'>
        <tr>
           <td align='right' valign='top'>
              <div style='height: 18px; line-height: 18px; font-size: 16px;'>&nbsp;</div>
              <font face='Source Sans Pro, sans-serif' color='#000000' style='font-size: 20px; line-height: 26px;'>
                 <span style='font-family: Source Sans Pro, Arial, Tahoma, Geneva, sans-serif; color: #000000; font-size: 20px; line-height: 26px;'> Subtotal: NGN ".$total." </span>
              </font>
              <div style='height: 2px; line-height: 2px; font-size: 1px;'>&nbsp;</div>
              <font face='Source Sans Pro, sans-serif' color='#000000' style='font-size: 20px; line-height: 26px;'>
                 <span style='font-family: Source Sans Pro, Arial, Tahoma, Geneva, sans-serif; color: #000000; font-size: 20px; line-height: 26px;'>Shipping: NGN1000</span>
              </font>
              <div style='height: 2px; line-height: 2px; font-size: 1px;'>&nbsp;</div>
              <div style='height: 18px; line-height: 18px; font-size: 16px;'>&nbsp;</div>
           </td>
        </tr>
     </table>
     <table cellpadding='0' cellspacing='0' border='0' width='88%' style='width: 88% !important; min-width: 88%; max-width: 88%;'>
        <tr>
           <td align='right' valign='top'>
              <div style='height: 14px; line-height: 14px; font-size: 12px;'>&nbsp;</div>
              <font face='Source Sans Pro, sans-serif' color='#000000' style='font-size: 23px; line-height: 28px; font-weight: bold;'>
                 <span style='font-family: Source Sans Pro, Arial, Tahoma, Geneva, sans-serif; color: #000000; font-size: 23px; line-height: 28px; font-weight: bold;'>TOTAL: NGN ".$total." </span>
              </font>
              <div style='height: 40px; line-height: 40px; font-size: 38px;'>&nbsp;</div>
           </td>
        </tr>
     </table>"; 
     //dd($order);
    return $order;
    }

    public function onShare() {
        $checkout = $this->preparePDF();
        $this->user = Auth::getUser();
        $data2 = array_combine(Input::get('email'), Input::get('name'));
        foreach ($data2 as $this->key => $this->value) {
            $vars = ['firstname' => $this->user->name, 'lastname' => $this->user->surname, 'checkout' => $checkout, 'name' =>$this->value];
            Mail::send('ffande.procurement::mail.sharedcheckout', $vars, function($message) {
                $message->to($this->key, $this->value);
                $message->subject('FF&E Checkout by '.$this->user->name.' '.$this->user->surname);
            }); 
        }
        \Flash::success('You have successfully shared your checkout with your friends');
        return Redirect::refresh();
    }

    public function onInvite() {
        $this->user = Auth::getUser();
        $data2 = array_combine(Input::get('email'), Input::get('name'));
        foreach ($data2 as $this->key => $this->value) {
            $vars = ['firstname' => $this->user->name, 'lastname' => $this->user->surname, 'name' =>$this->value];
            Mail::send('ffande.procurement::mail.invitation', $vars, function($message) {
                $message->to($this->key, $this->value);
                $message->subject('FF&E Invitation by '.$this->user->name.' '.$this->user->surname);
            }); 
        }
    }

    public function customerOrder() {
         $order = $this->newOrder;
         dd($order->order_details);
    }

    public function sendOrderConfirmation(){
        $this->user = Auth::getUser();
        //$cart = Session::get('cart');
        $order = $this->prepareOrder();
        //dd($order);
        if ($this->newOrder->discounted_total) {
           $total = $this->newOrder->discounted_total;
        } else {
            $total = $this->newOrder->total;
        }
        $vars = ['order' => $order,'firstname' => $this->user->name, 'lastname' => $this->user->surname, 'email' => $this->user->email, 'order' => $order, 'total' => $total, 'order_id' => $this->newOrder->id];
        Mail::send('ffande.procurement::mail.thanksfororder', $vars, function($message) {
            $message->to($this->user->email, $this->user->name.' '.$this->user->surname);
            $message->subject('Kokore Order Confirmation | Order Nº'.$this->newOrder->id.' '.$this->user->name.' '.$this->user->surname);
        }); 

        Mail::send('ffande.procurement::mail.ordernotification', $vars, function($message) {
           $message->to('info@thisiskokore.co', 'Kokore Client Print Order');
           $message->subject('Kokore Print Order Nº'.$this->newOrder->id.' '.$this->user->name.' '.$this->user->surname);
        });
    }

    public function sendClientOrder(){
        $this->user = Auth::getUser();
        //$cart = $this->preparePDF();
        $vars = ['firstname' => Input::get('name'), 'lastname' => Input::get('surname'), 'email' => Input::get('email'), 'cart' => $cart, 'ordernumber' => $this->newOrder->id];
        Mail::send('ffande.procurement::mail.ordernotification', $vars, function($message) {
            $message->to('info@thisiskokore.co', 'Kokore Client Order');
            $message->subject('kokore Order Nº'.$this->newOrder->id.' '.Input::get('name').' '.Input::get('surname'));
        }); 
    }

   public function sendArtistNotification($artist){
      $this->user = Auth::getUser();
      $this->artistMail = $artist->email;
      $this->artistDisplayName = $artist->display_name;
      //$cart = $this->preparePDF();
      //$vars = ['firstname' => Input::get('name'), 'lastname' => Input::get('surname'), 'email' => Input::get('email'), 'cart' => $cart, 'ordernumber' => $this->newOrder->id];
      $vars = ['display_name' => $this->artistDisplayName];
      Mail::send('ffande.procurement::mail.ordernotification', $vars, function($message) {
          $message->to($this->artistMail, 'Kokore Client Order');
          $message->subject('kokore Order Nº'.$this->newOrder->id.' '.Input::get('name').' '.Input::get('surname'));
      }); 
  }

    
    public $value;
    public $key;
    public $user;
    public $newOrder;
    public $vars;
    public $artistMail;
    public $artistDisplayName;
    public $dbCartTotal;
    public $dbCart;
}