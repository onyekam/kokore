<?php namespace Ffande\Procurement\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Ffande\Procurement\Models\SolutionsCategory;
use Ffande\Procurement\Models\ProjectSolutions;
use Redirect; 
use DB;
use Session;
use Flash;
use BackendAuth;


class DisplayProjectSolutions extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Display Project Solutions in category',
			'description' => 'Display Project Solutions in Category'
		];
	}



	public function onRun(){
		$this->backendUser = BackendAuth::getUser();
		$this->projectSolutionCategories = $this->projectSolutionsCategories();
		$this->categoryProjectSolutions = $this->getCategoryProjectSolutions();
		if($this->param('category') && $this->param('slug')) {
			$this->projectSolution = $this->getProjectSolution();
			$this->projectSolutionCategories = $this->projectSolutionsCategories();
			if($this->getRelatedProjectSolutions()){
				$this->relatedPSs = $this->getRelatedProjectSolutions();
			}
			//dd($this->projectSolution);
		}
		//dd($this->categoryProjectSolutions);
    }

    public function getCategoryProjectSolutions(){		
		$projectSolutionsCategory = SolutionsCategory::where('slug', $this->param('slug'))->first();
		$projectSolutions = ProjectSolutions::where('category_id', $projectSolutionsCategory['id'])->get();
		$this->projectSolutionsCategory = $projectSolutionsCategory;
		return $projectSolutions;
	}

	public function getRelatedProjectSolutions(){		
		$projectSolution = ProjectSolutions::where('slug', $this->param('slug'))->first();
		//$projectSolutionsCategory = SolutionsCategory::where('slug', $this->param('slug'))->first();
		$projectSolutions = ProjectSolutions::where('category_id', $projectSolution->category->id)->take(4)->inRandomOrder()->get();
		//dd($projectSolutions);

		return $projectSolutions;
	}

	public function projectSolutionsCategories(){
		$projectSolutionCategories = SolutionsCategory::all()->keyBy('category');
		return $projectSolutionCategories;
	}


	public function getProjectSolution() {
		$projectSolutionsCategory = SolutionsCategory::where('slug', $this->param('category'))->first();
		$projectSolution = ProjectSolutions::where('category_id',$projectSolutionsCategory['id'])->where('slug', $this->param('slug'))->first();
		return $projectSolution;
		//$this->relatedArtisans = Artisan::where('category_id',$this->categoryId)->take(4)->orderBy(Db::raw('RAND()'))->get();
	}
	
	// public function displayManufacturerProducts(){

    // 	$this->products = Product::where('manufacturer_id', $this->manufacturer->id)->get();
    // }
    //public $manufacturers;
	public $projectSolutionCategories;
	public $categoryProjectSolutions;
	public $projectSolutionsCategory;
	public $projectSolution;
	public $backendUser;
	public $relatedPSs;
	//public $products;

}