<?php namespace Ffande\Procurement\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Ffande\Procurement\Models\Manufacturer;
use Ffande\Procurement\Models\Product;
use Ffande\Procurement\Models\Annotations;
use Ffande\Procurement\Models\Slide;
use Redirect; 
use Session;
use Response;
use Network\Http;


class Homeslider extends ComponentBase {
    public function componentDetails(){
		return [
			'name' => 'Home Sliders',
			'description' => 'Display and Control of sliders'
		];
    }
    
    public function onRun(){
        
        if ($this->page->id == 'decor-services') {
            $this->decorSlides = $this->getDecorSlides();
        } else {
            $this->slides = $this->getSlides();
        }
        //dd($code);

    }

    public function getSlides(){
        $slides = Slide::where('decor_services','0')->get();
        return $slides;
    }

    public function getDecorSlides(){
        $slides = Slide::where('decor_services', '1')->get();
        return $slides;
    }
    public $decorSlides;
    public $slides;

}