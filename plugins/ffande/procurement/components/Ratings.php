<?php namespace Ffande\Procurement\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Ffande\Procurement\Models\Manufacturer;
use Ffande\Procurement\Models\Product;
use Auth;
use Ffande\Procurement\Models\Rating;
use Redirect; 


class Ratings extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Ratings',
			'description' => 'Create and display product ratings'
		];
	}



	public function onRun(){
                

    }

    // public function displayCategoryProducts(){
    //     $productCategory = Category::where('slug', $this->param('category'))->first();
    //     $this->products = Product::where('category_id', $productCategory->id)->simplepaginate(30);
    // }
    
    public function onRate(){
        $user = Auth::getUser();
        $rating = new Rating;
        $rating->comment = post('comments');
        $rating->rating = post('rating');
        $rating->product_id = post('product_id');
        $rating->user_id = $user->id;
        $rating->save();
        return redirect($this->currentPageUrl());
    }

    public function displayHomeItemsMenu(){
        $this->homeItemsCategories = Category::where('home_item_category', '1')->get();
    }

	public function loadCategories() {
		$categories = Category::all();
		return $categories;
	}

	// public function displayManufacturerProducts(){

    // 	$this->products = Product::where('manufacturer_id', $this->manufacturer->id)->get();
    // }
    //public $manufacturers;
	public $productRatings;
}