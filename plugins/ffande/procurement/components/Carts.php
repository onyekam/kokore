<?php namespace Ffande\Procurement\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Ffande\Procurement\Models\Manufacturer;
use Ffande\Procurement\Models\Product;
use Ffande\Procurement\Models\Category;
use Ffande\Procurement\Models\Size;
use Ffande\Procurement\Models\Price;
use Ffande\Procurement\Models\Cart;
use Ffande\Procurement\Models\Color;
use Ffande\Procurement\Models\Frame;
use Redirect; 
use Session;
use Auth;
use Cookie;

class Carts extends ComponentBase {
	public function componentDetails(){
		return [
			'name' => 'Shopping Cart',
			'description' => 'Products added to cart'
		];
	}
	public function onRun(){
		$this->page['cart'] = $this->cart();
		$this->cart = $this->cart();
		$this->dbCart = $this->dbCart();
        $this->page['cartTotal'] = Session::get('cartTotal');
		$this->total = $this->cartTotal();
		$this->cartColors = $this->getColors();
		$this->cartFrames = $this->frames();
	}
	
	public function getColors() {
		$colors = Color::all();
		return $colors;
	}

	public function frames(){
		$frames = Frame::all();
		return $frames;
	}
    /**
     * Returns a cart, if available, 
     * 
     */

	
    public function cart()
    {
        if (!$cart = Session::has('cart') ){
            return null;
        }
		$cart = Session::get('cart');
        return $cart;
	}
	
	public function dbCart()
    {	
		$user = Auth::getUser();
		$cookie = Cookie::get('cartCookie');
		
		//dd($cookie);
		if($user){
			$cart = Cart::where('user_id', $user->id)->get();	
			$this->dbCartTotal = $this->getDbCartTotal();	
			return $cart;
		} elseif($cookie) {
			$cart = Cart::where('cookie', $cookie)->get();
			$this->dbCartTotal = $this->getDbCartTotal();	
			return $cart;
		}

		
	}
	
	public function getDbCartTotal() {
		$user = Auth::getUser();
		$cookie = Cookie::get('cartCookie');
		$cartTotal = 0;
		if($user){
			$cart = Cart::where('user_id', $user->id)->get();
			foreach($cart as $cartItem){
				$cartTotal += $cartItem->total;
			}
		} elseif($cookie) {
			$cart = Cart::where('cookie', $cookie)->get();
			foreach($cart as $cartItem){
				$cartTotal += $cartItem->total;
			}
		}
		return $cartTotal;
	}

    public function cartTotal()
    {
        if (!$cart = Session::has('cart') ){
            return null;
        }
        $cart = Session::get('cart');
        $total = 0;
        foreach ($cart as $cartItemKey ) {
			$total += $cartItemKey['cost'];
			//foreach ($cartItemKey as $cartItem) {
				//$total += $cartItem['cost'];
			//}
        }
        Session::put('cartTotal', $total);
        return $total;
	}

	public function findKey($array, $keySearch){
		// check if it's even an array
		if (!is_array($array)) return false;

		// key exists
		if (array_key_exists($keySearch, $array)) return true;

		// key isn't in this array, go deeper
		foreach($array as $key => $val)
		{
			// return true if it's found
			if ($this->findKey($val, $keySearch)) return true;
		}
		return false;
	}

    public function onAddToCartOld(){
		$product = Product::where('id', Input::get('product'))->first();
		$sanitizedPrice = preg_replace("/[^0-9.]/", "", $product->price);
		$quantity = Input::get('quantity');
		$cost = round($sanitizedPrice * $quantity,2);
		$cartItem = array('id'=>$product->id,'product_image'=>$product->product_image->path,'slug'=>$product->slug,'price'=>$sanitizedPrice,'product'=>str_replace("<br />","",$product->product_name),'quantity'=>$quantity,'cost'=>$cost);
		$productKey = (string) $product->slug;	
		if (!$cart = Session::has('cart') ){
			$cartArray = array();
			$cartArray[$productKey] = $cartItem;
			Session::put('cart', $cartArray);
			$cart = Session::get('cart');	
			\Flash::success('Item Successfully added to Cart');
		}
		else {
			$cartArray = Session::get('cart');
			//return $cart;
			//$cartItemUpdate = [];
			if ($this->findKey($cartArray, $product->slug)){
				$newQuantity = Input::get('quantity'); 
				$cost = round($sanitizedPrice * $newQuantity,2);
				// foreach( $cart as $cartArrayItem){
				// 	foreach($cartArrayItem as $key => $val) {
				// 		if ($this->findKey($key, $product->slug)){
				// 			unset($key[$product->slug]);
				// 		}
				// 	}
				// }
				//unset($cart[$product->slug]);
				$cartArray[$productKey] = array('id'=>$product->id,'product_image'=>$product->product_image->path,'slug'=>$product->slug,'price'=>$sanitizedPrice,'product'=>str_replace("<br />","",$product->product_name),'quantity'=>$newQuantity,'cost'=>$cost);
				//Session::forget('cart');
				Session::put('cart', $cartArray);
				//Session::push('cart', $cart);
				\Flash::success('Item quantity updated Successfully');
			}
			else {
				$cartArray = Session::get('cart');
				$cartArray[$product->slug] = $cartItem;
				//Session::put('cart',[]);
				//array_push($cart, $cartItem);
				Session::put('cart', $cartArray);
				\Flash::success('Another item Successfully added to Cart');
			}
		}
		$this->cartTotal();
		//return [
		// 	'#minicart' => $this->renderPartial('Cart::minicart')
		//];

		return \Redirect::refresh();
	}

	public function onAddToDBCart(){
		$product = Product::where('id', Input::get('product'))->first();
		$quantity = Input::get('quantity');
		if (Input::get('size')) {
			$size = Input::get('size');
		}
		// if($product->sale_off){
		// 	$sanitizedPrice = Input::get('price') - (Input::get('price') * $product->sale_amount / 100); 
		// } else {
			$sanitizedPrice = Input::get('price'); 
		//}
		$color = Input::get('color');
		$frame = Input::get('frame');
		$border = Input::get('border');
		$cost = round($sanitizedPrice * $quantity,2);
		$this->productSizes = [
			'5 x 7' => $product->base_price->five_by_seven,
			'8 x 12' => $product->base_price->eight_by_twelve,
			'12 x 16' => $product->base_price->twelve_by_sixteen,
			'16 x 20' => $product->base_price->sixteen_by_twenty,
			'20 x 20' => $product->base_price->twenty_by_twenty,
			'20 x 38' => $product->base_price->twenty_by_twenty_eight,
			'24 x 36' => $product->base_price->twenty_four_by_thirty_six,
			'28 x 39' => $product->base_price->twenty_eight_by_thirty_nine
		];
		$newCartItem = new Cart;
		$newCartItem->product_id = Input::get('product');
		$newCartItem->price = $sanitizedPrice;
		$newCartItem->total = $cost;
		$user = Auth::getUser();
		$cookie = Cookie::get('cartCookie');
		if($user){	
			$newCartItem->user_id = $user->id;
		} elseif($cookie) {
			Cookie::queue('cartCookie',$cookie, 360);
			//setcookie("cartCookie", $cookie, time() + 3600 * 24 * 7);
			$newCartItem->cookie = $cookie;
		} 
		else {
			$uniqueVal = uniqid();
			Cookie::queue('cartCookie', $uniqueVal, 360);
			//$cookie = Cookie::forever('cartCookie', $uniqueVal);
			//Cookie::make('cartCookie',$uniqueVal,60*24*7);
			//setcookie("cartCookie", $uniqueVal, time() + 3600 * 24 * 7);
			$newCartItem->cookie = $uniqueVal;
		}
		$newCartItem->quantity = Input::get('quantity');
		if(!$product->homeitem){
			$newCartItem->size = Input::get('size');
			$newCartItem->frame_id = Input::get('frame');
		} else {
			$newCartItem->color = Input::get('color');
		}
		$newCartItem->border = $border;
		$newCartItem->save();
		\Flash::success('Item Successfully added to Cart');
		return \Redirect::refresh();
	}

	public function onAddToCart(){
		$product = Product::where('id', Input::get('product'))->first();
		//$sanitizedPrice = preg_replace("/[^0-9.]/", "", $product->price);
		$quantity = Input::get('quantity');
		if (Input::get('size')) {
			$size = Input::get('size');
		}
		$sanitizedPrice = Input::get('price');
		//$sanitizedPrice = $sanitizedPrice->price;
		$color = Input::get('color');
		$frame = Input::get('frame');
		$cost = round($sanitizedPrice * $quantity,2);
		//return $frame.$color.$size;
		//dd();
		$this->productSizes = [
			'5 x 7' => $product->base_price->five_by_seven,
			'8 x 12' => $product->base_price->eight_by_twelve,
			'12 x 16' => $product->base_price->twelve_by_sixteen,
			'16 x 20' => $product->base_price->sixteen_by_twenty,
			'20 x 20' => $product->base_price->twenty_by_twenty,
			'20 x 38' => $product->base_price->twenty_by_twenty_eight,
			'24 x 36' => $product->base_price->twenty_four_by_thirty_six,
			'28 x 39' => $product->base_price->twenty_eight_by_thirty_nine
		];

		$cost = round($sanitizedPrice * $quantity,2);
		if ($product->homeitem) {
			$cartItem = array('id'=>$product->id,'product_image'=>$product->product_image->path,'slug'=>$product->slug,'price'=>$sanitizedPrice,'product'=>str_replace("<br />","",$product->product_name),'quantity'=>$quantity,'cost'=>$cost);
			
		} else {
			$cartItem = array('id'=>$product->id,'product_image'=>$product->product_image->path,'slug'=>$product->slug,'price'=>$sanitizedPrice,'product'=>str_replace("<br />","",$product->product_name),'quantity'=>$quantity,'cost'=>$cost, 'size'=>$size, 'frame'=>$frame);
			//return "did you get here?";
		}
		if ($product->homeitem) {
			$productKey = (string) $product->slug;	
		} else {
			$productKey = (string) $product->slug.'/'.$size.'/'.$frame;	
		}
		if (!$cart = Session::has('cart') ){
			$cartArray = array();
			$cartArray[$productKey] = $cartItem;
			Session::put('cart', $cartArray);
			$cart = Session::get('cart');	
			\Flash::success('Item Successfully added to Cart');
		}
		else {
			$cartArray = Session::get('cart');
			if ($this->findKey($cartArray, $product->slug)){
				$newQuantity = Input::get('quantity'); 
				$cost = round($sanitizedPrice * $newQuantity,2);
				if ($product->homeitem) {
					$cartArray[$productKey] = array('id'=>$product->id,'product_image'=>$product->product_image->path,'slug'=>$product->slug,'price'=>$sanitizedPrice,'product'=>str_replace("<br />","",$product->product_name),'quantity'=>$newQuantity,'cost'=>$cost);
				} else {
					$cartArray[$productKey] = array('id'=>$product->id,'product_image'=>$product->product_image->path,'slug'=>$product->slug,'price'=>$sanitizedPrice,'product'=>str_replace("<br />","",$product->product_name),'quantity'=>$newQuantity,'cost'=>$cost, 'size'=>$size, 'frame'=>$frame);
				}
				//Session::forget('cart');
				Session::put('cart', $cartArray);
				//Session::push('cart', $cart);
				\Flash::success('Item quantity updated Successfully');
			}
			else {
				$cartArray = Session::get('cart');
				$cartArray[$product->slug] = $cartItem;
				//Session::put('cart',[]);
				//array_push($cart, $cartItem);
				Session::put('cart', $cartArray);
				\Flash::success('Another item Successfully added to Cart');
			}
		}
		$this->cartTotal();
		//return [
		// 	'#minicart' => $this->renderPartial('Cart::minicart')
		//];
		
		return \Redirect::to('/prints');
	}

	public function onAddHomeItemToDbCart(){
		$product = Product::where('id', Input::get('product'))->first();
		$quantity = Input::get('quantity');
		if (Input::get('color')) {
			$color = Input::get('color');
			
		}
		$frame = 'N/A';
		$size = 'N/A';
		if($product->sale_off){
			$sanitizedPrice = $product->price - ($product->price * $product->sale_amount / 100); 
			$cost = $sanitizedPrice * $quantity;
		} else {
			$cost = $product->price * $quantity;
		}
		
		$newCartItem = new Cart;
		//$newCartItem->color_id = $color;
		$newCartItem->product_id = Input::get('product');
		$newCartItem->price = $product->price;
		$newCartItem->total = $cost;
		$user = Auth::getUser();
		$cookie = Cookie::get('cartCookie');
		if($user){	
			$newCartItem->user_id = $user->id;
		} elseif($cookie) {
			Cookie::queue('cartCookie',$cookie, 360);
			//setcookie("cartCookie", $cookie, time() + 3600 * 24 * 7);
			$newCartItem->cookie = $cookie;
		} 
		else {
			$uniqueVal = uniqid();
			Cookie::queue('cartCookie', $uniqueVal, 360);
			//$cookie = Cookie::forever('cartCookie', $uniqueVal);
			//Cookie::make('cartCookie',$uniqueVal,60*24*7);
			//setcookie("cartCookie", $uniqueVal, time() + 3600 * 24 * 7);
			$newCartItem->cookie = $uniqueVal;
		}
		$newCartItem->quantity = $quantity;
		
		$newCartItem->save();
		\Flash::success('Item Successfully added to Cart');
		return \Redirect::to('/home-items');

	}

	public function onAddHomeItemToCart(){
		$product = Product::where('id', Input::get('product'))->first();
		//$sanitizedPrice = preg_replace("/[^0-9.]/", "", $product->price);
		$quantity = Input::get('quantity');
		if (Input::get('size')) {
			$size = Input::get('size');
		}
		$sanitizedPrice = Input::get('price');
		$color = Input::get('color');
		$frame = Input::get('frame');
		$cost = round($sanitizedPrice * $quantity,2);
		if ($product->homeitem) {
			$cartItem = array('id'=>$product->id,'product_image'=>$product->product_image->path,'slug'=>$product->slug,'price'=>$sanitizedPrice,'product'=>str_replace("<br />","",$product->product_name),'quantity'=>$quantity,'cost'=>$cost);
			
		} else {
			$cartItem = array('id'=>$product->id,'product_image'=>$product->product_image->path,'slug'=>$product->slug,'price'=>$sanitizedPrice,'product'=>str_replace("<br />","",$product->product_name),'quantity'=>$quantity,'cost'=>$cost, 'size'=>$size, 'frame'=>$frame);
		}
		if ($product->homeitem) {
			$productKey = (string) $product->slug;	
		} else {
			$productKey = (string) $product->slug.'/'.$size.'/'.$frame;	
		}
		if (!$cart = Session::has('cart') ){
			$cartArray = array();
			$cartArray[$productKey] = $cartItem;
			Session::put('cart', $cartArray);
			$cart = Session::get('cart');	
			\Flash::success('Item Successfully added to Cart');
		}
		else {
			$cartArray = Session::get('cart');
			if ($this->findKey($cartArray, $product->slug)){
				$newQuantity = Input::get('quantity'); 
				$cost = round($sanitizedPrice * $newQuantity,2);
				if ($product->homeitem) {
					$cartArray[$productKey] = array('id'=>$product->id,'product_image'=>$product->product_image->path,'slug'=>$product->slug,'price'=>$sanitizedPrice,'product'=>str_replace("<br />","",$product->product_name),'quantity'=>$newQuantity,'cost'=>$cost);
				} else {
					$cartArray[$productKey] = array('id'=>$product->id,'product_image'=>$product->product_image->path,'slug'=>$product->slug,'price'=>$sanitizedPrice,'product'=>str_replace("<br />","",$product->product_name),'quantity'=>$newQuantity,'cost'=>$cost, 'size'=>$size, 'frame'=>$frame);
				}
				//Session::forget('cart');
				Session::put('cart', $cartArray);
				//Session::push('cart', $cart);
				\Flash::success('Item quantity updated Successfully');
			}
			else {
				$cartArray = Session::get('cart');
				$cartArray[$product->slug] = $cartItem;
				//Session::put('cart',[]);
				//array_push($cart, $cartItem);
				Session::put('cart', $cartArray);
				\Flash::success('Another item Successfully added to Cart');
			}
		}
		$this->cartTotal();
		//return [
		// 	'#minicart' => $this->renderPartial('Cart::minicart')
		//];
		
		return \Redirect::to('/home-items');
	}

	public function onRemoveFromCart(){
		$product = Product::where('id', Input::get('product'))->first();
		$size = Input::get('size');
		$frame = Input::get('frame');
		return $size.'/'.$frame;
		dd();
		$cartArray = Session::get('cart');
		if ($product->homeitem) {
			$productKey = (string) $product->slug;
		} else {
			$productKey = (string) $product->slug.'/'.$size.'/'.$frame;
		}
		unset($cartArray[$productKey]);
		Session::put('cart', $cartArray);
		\Flash::success('Item was successfully removed from the Cart');
		return \Redirect::refresh();

	}

	public function onRemoveFromDbCart(){
		$cartItemId = post('cartitemid');
		$cartItem = Cart::where('id', $cartItemId)->first();
		$cartItem->delete();
		$this->dbCartTotal = $this->getDbCartTotal();
		\Flash::success('Item was successfully removed from the Cart');
		return \Redirect::refresh();

	}

	public function onEmptyDbCart(){
		$user = Auth::getUser();
		if($user){
			$cart = Cart::where('user_id', $user->id)->get();
		} else {
			$cart = Cart::where('cookie', Cookie::get('cartCookie'))->get();
		}
		foreach ($cart as $cartItem) {
			$cartItem->delete();
		}
		return \Redirect::refresh();
	}

	// public function onDbUpdateItem(){
	// 	$cartItemId = post('cartitemid');
	// 	$cartItem = Cart::where('id', $cartItemId)->first();
	// 	$cartItem->quantity = $quantity[0] = $quantity = post('quantity');
	// 	$cartItem->size = $size[0] = $size = post('size');
	// 	$cartItem->frame = $frame[0] = $frame = post('frame');
	// 	$cartItem->total = $total[0] = $total = post('cartitemtotalcost');
	// 	$cartItem->save();

	// 	$this->dbCartTotal = $this->getDbCartTotal();
	// 	\Flash::success('Cart updated successfully');
	// 	return Redirect::to('/cart');
	// }

	public function onDBCartUpdate(){
		$cartUpdateInput = Input::all();
		//dd($cartUpdateInput);
		$sizes = post('size');
		$frames = post('frame');
		$colors = post('color');
		$products = post('product');
		$quantities = post('quantity');
		$costs = post('cartitemtotalcost');
		$cartItemIds = post('cartitemid');
		$cartCount = post('cartcount');
		
		////// find current user cart
		// find logged in user
		$user = Auth::getUser();
		
		for ($i=0; $i <= $cartCount - 1; $i++) { 
			$cart = Cart::where('id', $cartItemIds[$i] )->first();
			$size = $sizes[$i];
			if (!$size=="NA") {
				$cart->size = $sizes[$i];
				$sizeInText = $this->sizeMap[$size];
				$product = Product::where('id', $products[$i])->first();
				$cart->price = $product->base_price[$sizeInText];
				$cart->frame = $frames[$i];
			} else {
				$cart->size = $sizes[$i];
				$product = Product::where('id', $products[$i])->first();
				$cart->price = $product->price;
				$cart->color_id = $colors[$i];
			}
			$cart->quantity = $quantities[$i];
			$cart->total = $costs[$i];
			$cart->save();
		}
		$this->dbCartTotal = $this->getDbCartTotal();
		\Flash::success('Cart updated successfully');
		return Redirect::to('/cart');
	}


	public function onCartUpdate(){
		$cartUpdateInput = Input::all();
		//dd($cartUpdateInput);
		$newCost = 0;
		$existingCartArray = Session::get('cart');
		for ($i=0; $i <= sizeof($cartUpdateInput); $i++) {
			$newQuantity = $cartUpdateInput['quantity'][$i];
			$productToUpdate = (string) $cartUpdateInput['products'][$i];
			$price = $existingCartArray[$productToUpdate]['price'];
			$price = $this->tofloat($price);
			$newCost =  $newQuantity * $price;
			$newSize = $cartUpdateInput['size'][$i];
			$newFrame = $cartUpdateInput['frame'][$i];
			$existingCartArray[$cartUpdateInput['products'][$i]]['quantity'] = $newQuantity;
			$existingCartArray[$cartUpdateInput['products'][$i]]['cost'] = $newCost;
			$existingCartArray[$cartUpdateInput['products'][$i]]['size'] = $newSize;
			$existingCartArray[$cartUpdateInput['products'][$i]]['frame'] = $newframe;
		}
		 Session::put('cart',$existingCartArray);
		 $this->cartTotal();
		 \Flash::success('Cart updated successfully');
		 return Redirect::refresh();
	}

	public function tofloat($num) {
		$dotPos = strrpos($num, '.');
		$commaPos = strrpos($num, ',');
		$sep = (($dotPos > $commaPos) && $dotPos) ? $dotPos : 
			((($commaPos > $dotPos) && $commaPos) ? $commaPos : false);

		if (!$sep) {
			return floatval(preg_replace("/[^0-9]/", "", $num));
		} 

		return floatval(
			preg_replace("/[^0-9]/", "", substr($num, 0, $sep)) . '.' .
			preg_replace("/[^0-9]/", "", substr($num, $sep+1, strlen($num)))
		);
	}

	public function onFlush(){
		Session::forget('cart');
		//Session::forget('cart');
		Session::forget('carttotal');
		\Flash::success('Cart cleared successfully');
		 return Redirect::to('/prints');
	}

	public function onSizeSelectCart(){
		$sizeInText = post('sizeInText');
		$product = Product::where('id',post('product'))->first();
		if($product->sale_off){
			$price = $product->base_price[$sizeInText] - ($product->base_price[$sizeInText] * $product->sale_amount / 100);
		} else {
			$price = $product->base_price[$sizeInText];
		}
		$index = post('index');
		$quantity = post('quantity');
		$cost = $price * $quantity;
		return [
			".finalprice$index" => $this->renderPartial('@finalprice', ['price' => $price]),
			".itemtotalcost$index" => $this->renderPartial('@itemtotalcost', ['cost' => $cost]),
			".quantity$index" => $this->renderPartial('@quantityreset')
		];
	}

	public function onChangeQuantityCart(){
		$index = post('index');
		$cost = post('cost');
		return [
			".itemtotalcost$index" => $this->renderPartial('@itemtotalcost', ['cost' => $cost])
		];
	}

	public $cart;
	public $total;
	public $dbCartTotal;
	public $productSizes;
	public $cartColors;
	public $cookie;
	public $cartFrames;
	public $sizeMap = [
		'5 x 7' => 'five_by_seven',
		'8 x 12' => 'eight_by_twelve',
		'12 x 16' => 'twelve_by_sixteen',
		'16 x 20' => 'sixteen_by_twenty',
		'20 x 20' => 'twenty_by_twenty',
		'20 x 38' => 'twenty_by_twenty_eight',
		'24 x 36' => 'twenty_four_by_thirty_six',
		'28 x 39' => 'twenty_eight_by_thirty_nine'
	];
	public $sizeMap2 = [
		'5+x+7' => 'five_by_seven',
		'8+x+12' => 'eight_by_twelve',
		'12+x+16' => 'twelve_by_sixteen',
		'16+x+20' => 'sixteen_by_twenty',
		'20+x+20' => 'twenty_by_twenty',
		'20+x+38' => 'twenty_by_twenty_eight',
		'24+x+36' => 'twenty_four_by_thirty_six',
		'28+x+39' => 'twenty_eight_by_thirty_nine'
	];
	public $productSizesMap = [
        '12cmx18cm-5x7' => "five_by_seven",
        '21cmx30cm-8x12' => "eight_by_twelve",
        '30cmx40cm-12x16' => "twelve_by_sixteen",
        '40cmx50cm-16x20' => "sixteen_by_twenty",
        '50cmx50cm-20x20' => "twenty_by_twenty",
        '50cmx70cm-20x28' => "twenty_by_twenty_eight",
        '61cmx91cm-24x36' => "twenty_four_by_thirty_six",
        '70cmx100cm-28x39' => "twenty_eight_by_thirty_nine"
    ];
}