<?php namespace Ffande\Procurement\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Ffande\Forms\Models\CallForTender;
use Ffande\Forms\Models\Product;
use Ffande\Procurement\Models\DecorOrders;
use Redirect; 
use Auth;


class DecorOrdersComponent extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Display Design Orders',
			'description' => 'List all Orders'
		];
	}



	public function onRun(){
		$this->displayOrders();
		//return $this->dOrders;
		//dd($this->dOrders);
		//return $this->callForTenders;
        // if ($this->param('slug')) {
		// 	$this->displayManufacturer();
		// 	$this->displayManufacturerProducts();	
        // } else {
        // 	$this->manufacturers = Manufacturer::all();
        // }
        

    }

    // public function displayManufacturer(){
    // 	$this->manufacturer = Manufacturer::where('slug', $this->param('slug'))->first();
	// }
	
	public function displayOrders(){
		$user = Auth::getUser();
		$this->dOrders = DecorOrders::where('user_id', $user->id)->orderBy('id')->get();

		//dd($this->dOrders);
		
    }
    public $dOrders;
	
	//public $products;

}