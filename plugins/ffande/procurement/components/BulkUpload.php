<?php namespace Ffande\Procurement\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Ffande\Procurement\Models\Manufacturer;
use Ffande\Procurement\Models\Product;
use Ffande\Procurement\Models\Category;
use Redirect; 
use Session;

use Vdomah\Excel\Classes\Excel;


class BulkUpload extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Test Bulk Upload',
			'description' => 'Test Bulk Upload'
		];
	}



	public function onRun(){
		$data = Excel::excel()->selectSheets('PORT')->load(base_path() . '/storage/app/media/test.xlsx', function($reader) {})->get();
       if (!empty($data) && $data->count()) {
           foreach ($data as $key => $value ) {
               echo $value->vessel . '<br/><br/>';
           }

        //    foreach ($data as $sheet) {
        //        echo $sheet->getTitle();
        //    }
       }
    }
}