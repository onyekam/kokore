<?php namespace Ffande\Procurement\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Ffande\Procurement\Models\Manufacturer;
use Ffande\Procurement\Models\Product;
use Ffande\Procurement\Models\Annotations;
use Redirect; 
use Session;
use Response;
use Network\Http;


class Instagram extends ComponentBase {
    public function componentDetails(){
		return [
			'name' => 'Annotations',
			'description' => 'Display & Create Annotations'
		];
    }
    
    public function onRun(){
        $code = \Http::get('https://api.instagram.com/oauth/authorize/?client_id=5da62ac727a54c4386f7abafbfa1c8a9&redirect_uri=https://kokore.tk&response_type=code');
        //dd($code);

    }

    public function getFeed(){
        $r = new HttpRequest('https://api.instagram.com/v1/users/self/media/recent/?access_token=349295974.5da62ac.9120d93cba6945c1b5931aff134ba5a8', HttpRequest::METH_GET);
    }

    public function onSaveAnnotation(){
     // dd(post());
      //$data = post();
    
      $annotation = new Annotations;
      $annotation->text = post('text');
      $annotation->context = post('context');
      $annotation->width = post('width');
      $annotation->height = post('height');
      $annotation->x = post('x');
      $annotation->y = post('y');
      $annotation->project_solution_url = post('project_solution_url');
      $annotation->save();
      return Response::json(['response' => 'Annotation saved successfully']);

    
    
    }

    public function onGetAnnotations(){
      $annotations = Annotations::where('project_solution_url',post('url'))->get();
      return Response::json($annotations);
    }
}