<?php namespace Ffande\Procurement\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Ffande\Procurement\Models\Manufacturer;
use Ffande\Procurement\Models\Product;
use Ffande\Procurement\Models\Category;
use Ffande\Procurement\Models\Color;
use Redirect; 
use Illuminate\Support\Collection;
use Auth;
use Flash;
use System\Models\File as File;
use DB;
use Validator;
use Mail;


class DashboardProducts extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Display Shop',
			'description' => 'List all products'
		];
	}



	public function onRun(){
        $user = Auth::getUser();
        if ($user) {
            $this->products = $this->displaySupplierProducts();
            $this->categories = $this->getCategories();
            $this->manufacturers = $this->getManufacturers();
            $this->getColors();
            if ($this->param('slug')) {
                $this->product = $this->displayProduct();
            }
        } else {
            Flash::fail('Please login to proceed to your dashboard');
            Redirect::to('/login');
        }
        
    }
    public function getCategories(){
        $categories = Category::all();
        return $categories;
    }

    public function getManufacturers(){
        $manufacturers = Manufacturer::all();
        return $manufacturers;
    }
	public function displaySupplierProducts(){
        $user = Auth::getUser();
		//$productCategory = Category::where('slug', $this->param('slug'))->first();
        $products = Product::where('user_id', $user->id)->get();
		//$products = new Collection;
		return $products;
    }
    public function onEditProduct(){
        $user = Auth::getUser();
        $product = Product::where('slug', $this->param('slug'))->where('user_id', $user->id)->first();
        $product->product_name = Input::get('product_name');
        $product->price = Input::get('price');
        $product->slug = $this->to_permalink(Input::get('product_name'));
        $product->product_excerpt = Input::get('product_excerpt');
        $product->product_description = Input::get('product_description');
        $product->category_id = Input::get('category_id');
        $product->manufacturer_id = Input::get('manufacturer_id');
        $product->product_reference = Input::get('product_reference');
        $product->product_sku = Input::get('product_sku');
        $product->user_id = Input::get('user_id');
        $product->product_image = Input::file('product_image');
        $product->hover_image = Input::file('hover_image');
        if (Input::file('image1')) {
            $product->gallery = nput::file('image1');
        }
        if (Input::file('image2')) {
            $product->gallery = nput::file('image2');
        }
        if (Input::file('image3')) {
            $product->gallery = nput::file('image3');
        }
        if (Input::file('image4')) {
            $product->gallery = nput::file('image4');
        }
        $product->save();
		Flash::success('Product Successfully Edited!');
        return Redirect::to('/my-account/products');
    }

	public function onCreateProduct(){
        $rules = [
            'product_name'=>'required',
            //'product_reference'=>'required',
            'category_id'=>'required',
            'color_id'=>'required',
            'product_image'=>'required'
            // 'hover_image'=>'required'
        ];
        $data = Input::all();
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return Redirect::to('/my-account/create-print')->withErrors($validator);
        }
        $product = new Product;
        $product->product_name = Input::get('product_name');
        //$product->price = Input::get('price');
        $product->slug = $this->to_permalink(Input::get('product_name'));
        $product->product_excerpt = Input::get('product_excerpt');
        $product->product_description = Input::get('product_description');
        //$product->category_id = Input::get('category_id');
        $product->manufacturer_id = Input::get('manufacturer_id');
       // $product->product_reference = Input::get('product_reference');
        $product->product_sku = Input::get('product_sku');
        $product->published = 0;
        $product->user_id = Input::get('user_id');
        $product->product_image = Input::file('product_image');
        // $product->hover_image = Input::file('hover_image');
        if (Input::file('image1')) {
            $product->gallery = Input::file('image1');
        }
        if (Input::file('image2')) {
            $product->gallery = Input::file('image2');
        }
        if (Input::file('image3')) {
            $product->gallery = Input::file('image3');
        }
        if (Input::file('image4')) {
            $product->gallery = Input::file('image4');
        }
        $product->save();
        if(post('color_id')){
            $colors = post('color_id');
            $colorProductsArray = [];
            foreach ($colors as $color) {
                array_push($colorProductsArray, ['product_id' => $product->id, 'color_id' => $color]);
            }
        }
        //dd($colorProductsArray);
        if(post('category_id')){
            $categories = post('category_id');
            $categoryProductsArray = [];
            foreach ($categories as $category) {
                array_push($categoryProductsArray, ['product_id' => $product->id, 'category_id' => $category]);
            }
        }
        
        $cats = DB::table('ffande_procurement_categories_products')->insert($categoryProductsArray);
        $cols = DB::table('ffande_procurement_colors_products')->insert($colorProductsArray);
        

        // $colors = post('color_id');
        // for ($i=0; $i < count($colors); $i++) { 
        //     DB::statement('INSERT INTO ffande_procurement_colors_products (product_id, color_id) VALUES ('.$product->id, $colors[$i].')');
        // }
        // $categories = post('category_id');
        // for ($i=0; $i < count($categories); $i++) { 
        //     DB::statement('INSERT INTO ffande_procurement_categories_products (product_id, category_id) VALUES ('.$product->id, $colors[$i].')');
        // }
        $this->newProduct = $product;
        $this->sendProductUploadNotification();
		Flash::success('Product Successfully Created!');
        return Redirect::refresh();
    }


    public function sendProductUploadNotification(){
        $this->user = Auth::getUser();
        //$cart = Session::get('cart');
       // $notification = $this->prepareNotification();
        //dd($order);
        
        $vars = [
            'firstname' => $this->user->name, 
            'lastname' => $this->user->surname, 
            'display_name' => $this->user->display_name, 
            'product_id' => $this->newProduct->id, 
            'product_name' => $this->newProduct->product_name
        ];
        Mail::send('ffande.procurement::mail.productupload', $vars, function($message) {
            $message->to('info@thisiskokore.co', 'Kokore Admin');
            $message->subject('New Print Uploaded by '.$this->newProduct->user->display_name.' called '.$this->newProduct->product_name);
        }); 
    }


    // public function onGalleryUpload(){
    //     $image = Input::all();
    //     $images = [];
    //     $print_images = "";
    //     foreach ($image['gallery'] as $printImageFromInput) {
	// 		array_push($images, (new File())->fromPost($printImageFromInput));
    //     }
    //     foreach ($images as $print_image){
    //         $print_images .= '<img src="'. $print_image->getThumb(200, 200, ['mode' => 'crop']).'" style="padding:2px;"/>';
    //     }
    //     return [
    //         '#gallery' => $print_images
    //     ];
    // }

    public function array_push_assoc($array, $key, $value){
        $array[$key] = $value;
        return $array;
    }

    public function onImageOne(){
        $image = Input::all();
        //$images = [];
        // foreach ($image['project_images'] as $projectImage) {
		// 	array_push($images, (new File())->fromPost($projectImage));
        // }
        $product_image = (new File())->fromPost($image['image1']);
        // foreach ($images as $product_image){
        //     $product_images .= '<img src="'. $product_image->getThumb(200, 200, ['mode' => 'crop']).'" style="padding:2px;"/>';
        // }
        $product_image = '<img src="'.$product_image->getThumb(200, 200, ['mode' => 'crop']).'" style="padding:2px;"/>';
  
        return [
            '#product_image1' => $product_image
        ];
    }
    public function onImage2(){
        $image = Input::all();
        //$images = [];
        $project_images = "";
        // foreach ($image['project_images'] as $projectImage) {
		// 	array_push($images, (new File())->fromPost($projectImage));
        // }
        $product_image = (new File())->fromPost($image['image2']);
        // foreach ($images as $product_image){
        //     $product_images .= '<img src="'. $product_image->getThumb(200, 200, ['mode' => 'crop']).'" style="padding:2px;"/>';
        // }
        $product_image = '<img src="'.$product_image->getThumb(200, 200, ['mode' => 'crop']).'" style="padding:2px;"/>';
  
        return [
            '#product_image2' => $product_image
        ];
    }
    public function onImage3(){
        $image = Input::all();
        //$images = [];
        $project_images = "";
        // foreach ($image['project_images'] as $projectImage) {
		// 	array_push($images, (new File())->fromPost($projectImage));
        // }
        $product_image = (new File())->fromPost($image['image3']);
        // foreach ($images as $product_image){
        //     $product_images .= '<img src="'. $product_image->getThumb(200, 200, ['mode' => 'crop']).'" style="padding:2px;"/>';
        // }
        $product_image = '<img src="'.$product_image->getThumb(200, 200, ['mode' => 'crop']).'" style="padding:2px;"/>';
  
        return [
            '#product_image3' => $product_image
        ];
    }
    public function onImage4(){
        $image = Input::all();
        //$images = [];
        $project_images = "";
        // foreach ($image['project_images'] as $projectImage) {
		// 	array_push($images, (new File())->fromPost($projectImage));
        // }
        $product_image = (new File())->fromPost($image['image4']);
        // foreach ($images as $product_image){
        //     $product_images .= '<img src="'. $product_image->getThumb(200, 200, ['mode' => 'crop']).'" style="padding:2px;"/>';
        // }
        $product_image = '<img src="'.$product_image->getThumb(200, 200, ['mode' => 'crop']).'" style="padding:2px;"/>';
  
        return [
            '#product_image4' => $product_image
        ];
    }

    public function onPrintImageUpload(){
        $image = Input::all();
        //$images = [];
        $project_images = "";
        // foreach ($image['project_images'] as $projectImage) {
		// 	array_push($images, (new File())->fromPost($projectImage));
        // }
        $product_image = (new File())->fromPost($image['product_image']);
        // foreach ($images as $product_image){
        //     $product_images .= '<img src="'. $product_image->getThumb(200, 200, ['mode' => 'crop']).'" style="padding:2px;"/>';
        // }
        $product_image = '<img src="'.$product_image->getThumb(200, 200, ['mode' => 'crop']).'" style="padding:2px;"/>';
  
        return [
            '#product_image' => $product_image
        ];
    }

    public function onHoverImageUpload(){
        $image = Input::all();
        //$images = [];
        $project_images = "";
        // foreach ($image['project_images'] as $projectImage) {
		// 	array_push($images, (new File())->fromPost($projectImage));
        // }
        $hover_image = (new File())->fromPost($image['hover_image']);
        // foreach ($images as $product_image){
        //     $product_images .= '<img src="'. $product_image->getThumb(200, 200, ['mode' => 'crop']).'" style="padding:2px;"/>';
        // }
        $hover_image = '<img src="'.$hover_image->getThumb(200, 200, ['mode' => 'crop']).'" style="padding:2px;"/>';
  
        return [
            '#hover_image' => $hover_image
        ];
    }
    

    public function onDeleteProduct(){

    }

    public function displayProduct(){
        $user = Auth::getUser();
        $product = Product::where('slug', $this->param('slug'))->where('user_id', $user->id)->first();
        return $product;
	}

	public function loadCategories() {
		$categories = Category::all();
		return $categories;
	}

	public function loadChildCategories(){}
	public function onAddToCart(){
		
	}

	public function flattenArray($array) {
 $arrayValues = array();
 foreach (new RecursiveIteratorIterator( new RecursiveArrayIterator($array)) as $val) {
  $arrayValues[] = $val;
 }
 return $arrayValues;
}
    public function to_permalink($str){
		if($str !== mb_convert_encoding( mb_convert_encoding($str, 'UTF-32', 'UTF-8'), 'UTF-8', 'UTF-32') )
			$str = mb_convert_encoding($str, 'UTF-8', mb_detect_encoding($str));
		$str = htmlentities($str, ENT_NOQUOTES, 'UTF-8');
		$str = preg_replace('`&([a-z]{1,2})(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig);`i', '\\1', $str);
		$str = html_entity_decode($str, ENT_NOQUOTES, 'UTF-8');
		$str = preg_replace(array('`[^a-z0-9]`i','`[-]+`'), '-', $str);
		$str = strtolower( trim($str, '-') );
		return $str;
    }

    public function getColors(){
        $this->colors = Color::all();
    }

// function flattenArrayIndexed()
	
	// public function displayManufacturerProducts(){

    // 	$this->products = Product::where('manufacturer_id', $this->manufacturer->id)->get();
    // }
    //public $manufacturers;
    public $newProduct;
	public $product;
	public $products;
    public $categories;
    public $manufacturers;
    public $colors;
    public $user;


}