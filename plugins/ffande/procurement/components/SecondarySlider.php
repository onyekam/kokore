<?php namespace Ffande\Procurement\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Ffande\Procurement\Models\Manufacturer;
use Ffande\Procurement\Models\Product;
use Ffande\Procurement\Models\Annotations;
use Ffande\Procurement\Models\SlideTwo as Slide;
use Redirect; 
use Session;
use Response;
use Network\Http;


class SecondarySlider extends ComponentBase {
    public function componentDetails(){
		return [
			'name' => 'Secondary Home Slides',
			'description' => 'Display and Control of sliders'
		];
    }
    
    public function onRun(){
        
       
            $this->slides = $this->getSlides();
    
        //dd($code);

    }

    public function getSlides(){
        $slides = Slide::all();
        return $slides;
    }

    public $slides;

}