<?php namespace Ffande\Procurement\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Ffande\Procurement\Models\Manufacturer;
use Ffande\Procurement\Models\Product;
use Ffande\Procurement\Models\Category;
use Ffande\Procurement\Models\Frame;
use Ffande\Procurement\Models\Size;
use Ffande\Procurement\Models\Coupon;
use Redirect; 
use Session;
use ValidationException;
use ApplicationException;
use Validator;
use Ffande\Procurement\Models\Order;
use Ffande\Procurement\Models\OrderDetails;
use Ffande\Procurement\Models\OrderShipping;
use Onyekam\Payment\Components\PaystackPayment;
use Auth;
use Renatio\DynamicPDF\Classes\PDF;
use Mail;
use Request;
use Ffande\Procurement\Models\DecorOrders;
use Ffande\Procurement\Models\DecorPackages;
use Rainlab\User\Models\User;


class Bookdecor extends ComponentBase {
	public function componentDetails(){
		return [
			'name' => 'Book Decor',
			'description' => 'Decor Package Booking Component'
		];
	}
	public function onRun() {
         $this->packages = $this->getPackages();
         $this->free = DecorPackages::where('package_name','Free')->first();
         $this->full = DecorPackages::where('package_name','Full')->first();
         $this->mini = DecorPackages::where('package_name','Mini')->first();
    }

    public function getPackages(){
        $packages = DecorPackages::all();
        return $packages;
    }

    public function onFullPackage(){
        return Redirect::to('/decor-services/choose-designer')->with('package','full');
    }
    public function onMiniPackage(){
        return Redirect::to('/decor-services/choose-designer')->with('package','mini');
    }
    public function onFreePackage(){
        return Redirect::to('/decor-services/choose-designer')->with('package','free');
    }

    public function onBookDecor(){
        $package = DecorPackages::where('package_name', post('package'))->first();
        $decorOrder = new DecorOrders;
        $decorOrder->user_id = post('decorator');
        //dd(post('decorator'));
        $this->decorator = User::find(post('decorator'));
        $decorOrder->package_id = $package->id;
        $decorOrder->f_name = post('f_name');
        $decorOrder->l_name = post('l_name');
        $decorOrder->message = post('message');
        $decorOrder->email = post('email');
        $decorOrder->phone = post('phone');
        $decorOrder->amount = $package->cost;
        $decorOrder->status_id = 1;
        $decorOrder->save();

        
        return Redirect::to('/decor-confirm-pay'.'/'.$decorOrder->id);
    }

    public function sendDecoOrderConfirmation(){
        //$this->user = Auth::getUser();
        //$cart = Session::get('cart');
        //$order = $this->prepareOrder();
        //dd($order);
        $vars = ['firstname' => $this->newOrder->f_name, 'lastname' => $this->newOrder->l_name, 'order' => $this->newOrder, 'package' => $this->newOrder->package->package_name, 'order_id' => $this->newOrder->id];
        Mail::send('ffande.procurement::mail.deco-order-confirmation', $vars, function($message) {
            $message->to($this->newOrder->email, $this->newOrder->f_name.' '.$this->newOrder->l_name);
            $message->subject('Kokore Decor Order Confirmation | Order Nº'.$this->newOrder->id.' '.$this->newOrder->f_name.' '.$this->newOrder->l_name);
        }); 
    }

    public function sendDecoOrderToDecorator(){
        //$this->user = Auth::getUser();
        
        //$cart = Session::get('cart');
        //$order = $this->prepareOrder();
        //dd($order);
       //dd($this->decorator->email.' '.$this->decorator->name.' '.$this->decorator->surname);
        $vars = ['messagesent' => $this->newOrder->message, 'email' => $this->newOrder->email ,'phone' => $this->newOrder->phone,  'firstname' => $this->newOrder->f_name, 'lastname' => $this->newOrder->l_name, 'order' => $this->newOrder, 'package' => $this->newOrder->package->package_name, 'order_id' => $this->newOrder->id, 'name' => $this->decorator->name];

        Mail::send('ffande.procurement::mail.designer-deco-notification', $vars, function($message) {
            $message->to($this->decorator->email, $this->decorator->name.' '.$this->decorator->surname);
            $message->subject('Client Decor Order Notification | Order Nº'.$this->newOrder->id.' '.$this->newOrder->f_name.' '.$this->newOrder->l_name);
        }); 
    }

    // public function initiatePayment($amount, $secretKey, $transRef, $orderID, $email){
    //     // Create map with request parameters
    //     $params = array ('amount' => $amount, 'secretkey' => $secretKey, 'transRef' => $transRef, 'orderId' => $orderID, 'email' => $email);
    //     PaystackPayment::onRedirectToGateway();
    // }


    public $user;
    public $newOrder;
    public $packages;
    public $decorator;
    public $free;
    public $full;
    public $mini;
}