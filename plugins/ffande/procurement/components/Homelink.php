<?php namespace Ffande\Procurement\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Ffande\Procurement\Models\Manufacturer;
use Ffande\Procurement\Models\Product;
use Ffande\Procurement\Models\Annotations;
use Ffande\Procurement\Models\HomeLinks;
use Redirect; 
use Session;
use Response;
use Network\Http;


class Homelink extends ComponentBase {
    public function componentDetails(){
		return [
			'name' => 'Home Links',
			'description' => 'Display and Control of Home Links'
		];
    }
    
    public function onRun(){
        
        $this->homelinks = $this->getHomeLinks();

    }

    public function getHomeLinks(){
        $homelinks = HomeLinks::all();
        return $homelinks;
    }
    public $homelinks;


}