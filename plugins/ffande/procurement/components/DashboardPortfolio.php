<?php namespace Ffande\Procurement\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Ffande\Procurement\Models\ArtistPortfolio;
use Redirect; 
use Illuminate\Support\Collection;
use Auth;
use Flash;
use System\Models\File as File;
use DB;
use Validator;
use Mail;

class DashboardPortfolio extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Display Shop',
			'description' => 'List all products'
		];
	}



	public function onRun(){
        $user = Auth::getUser();
        if ($user) {
            $this->products = $this->displayProducts();
            // $this->categories = $this->getCategories();
            // $this->manufacturers = $this->getManufacturers();
            // $this->getColors();
            if ($this->param('slug')) {
                $this->product = $this->displayProduct();
            }
            
        } else {
            Flash::fail('Please login to proceed to your dashboard');
            Redirect::to('/login');
        } 
    }

    public function onEditPortfolio(){
        $user = Auth::getUser();
        $product = Product::where('slug', $this->param('slug'))->where('user_id', $user->id)->first();
        $product->product_name = Input::get('product_name');
        $product->price = Input::get('price');
        $product->slug = $this->to_permalink(Input::get('product_name'));
        $product->product_excerpt = Input::get('product_excerpt');
        $product->product_description = Input::get('product_description');
        $product->category_id = Input::get('category_id');
        $product->manufacturer_id = Input::get('manufacturer_id');
        $product->product_reference = Input::get('product_reference');
        $product->product_sku = Input::get('product_sku');
        $product->user_id = Input::get('user_id');
        $product->product_image = Input::file('product_image');
        $product->hover_image = Input::file('hover_image');
        if (Input::file('image1')) {
            $product->gallery = nput::file('image1');
        }
        if (Input::file('image2')) {
            $product->gallery = nput::file('image2');
        }
        if (Input::file('image3')) {
            $product->gallery = nput::file('image3');
        }
        if (Input::file('image4')) {
            $product->gallery = nput::file('image4');
        }
        $product->save();
		Flash::success('Portfolio Successfully Edited!');
        return Redirect::to('/my-account/portfolio');
    }

	public function onCreatePortfolio(){
        $rules = [
            'product_name'=>'required',
            //'product_reference'=>'required',
            
            'product_image'=>'required'
            // 'hover_image'=>'required'
        ];
        $data = Input::all();
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return Redirect::to('/my-account/create-portfolio')->withErrors($validator);
        }
        $product = new ArtistPortfolio;
        $product->name = Input::get('product_name');
        //$product->price = Input::get('price');
        $product->slug = $this->to_permalink(Input::get('product_name'));
        $product->description = Input::get('product_description');
        $product->user_id = Input::get('user_id');
        $product->portfolio_featured_image = Input::file('product_image');
        // $product->hover_image = Input::file('hover_image');
        if (Input::file('image1')) {
            $product->gallery = Input::file('image1');
        }
        if (Input::file('image2')) {
            $product->gallery = Input::file('image2');
        }
        if (Input::file('image3')) {
            $product->gallery = Input::file('image3');
        }
        if (Input::file('image4')) {
            $product->gallery = Input::file('image4');
        }
        $product->save();
        
        $this->newProduct = $product;
        //$this->sendProductUploadNotification();
		Flash::success('Portfolio Successfully Created!');
        return Redirect::refresh();
    }


    public function sendProductUploadNotification(){
        $this->user = Auth::getUser();
        //$cart = Session::get('cart');
        // $notification = $this->prepareNotification();
        //dd($order);
        $vars = [
            'firstname' => $this->user->name, 
            'lastname' => $this->user->surname, 
            'display_name' => $this->user->display_name, 
            'product_id' => $this->newProduct->id, 
            'product_name' => $this->newProduct->product_name
        ];
        Mail::send('ffande.procurement::mail.productupload', $vars, function($message) {
            $message->to('info@thisiskokore.co', 'Kokore Admin');
            $message->subject('New Print Uploaded by '.$this->newProduct->user->display_name.' called '.$this->newProduct->product_name);
        }); 
    }


    // public function onGalleryUpload(){
    //     $image = Input::all();
    //     $images = [];
    //     $print_images = "";
    //     foreach ($image['gallery'] as $printImageFromInput) {
	// 		array_push($images, (new File())->fromPost($printImageFromInput));
    //     }
    //     foreach ($images as $print_image){
    //         $print_images .= '<img src="'. $print_image->getThumb(200, 200, ['mode' => 'crop']).'" style="padding:2px;"/>';
    //     }
    //     return [
    //         '#gallery' => $print_images
    //     ];
    // }

    public function array_push_assoc($array, $key, $value){
        $array[$key] = $value;
        return $array;
    }

    public function onImageOne(){
        $image = Input::all();
        //$images = [];
        // foreach ($image['project_images'] as $projectImage) {
		// 	array_push($images, (new File())->fromPost($projectImage));
        // }
        $product_image = (new File())->fromPost($image['image1']);
        // foreach ($images as $product_image){
        //     $product_images .= '<img src="'. $product_image->getThumb(200, 200, ['mode' => 'crop']).'" style="padding:2px;"/>';
        // }
        $product_image = '<img src="'.$product_image->getThumb(200, 200, ['mode' => 'crop']).'" style="padding:2px;"/>';
  
        return [
            '#product_image1' => $product_image
        ];
    }

    public function onPortfolioImageUpload(){
        $image = Input::all();
        //$images = [];
        $project_images = "";
        // foreach ($image['project_images'] as $projectImage) {
		// 	array_push($images, (new File())->fromPost($projectImage));
        // }
        $product_image = (new File())->fromPost($image['product_image']);
        // foreach ($images as $product_image){
        //     $product_images .= '<img src="'. $product_image->getThumb(200, 200, ['mode' => 'crop']).'" style="padding:2px;"/>';
        // }
        $product_image = '<img src="'.$product_image->getThumb(200, 200, ['mode' => 'crop']).'" style="padding:2px;"/>';
  
        return [
            '#product_image' => $product_image
        ];
    }
    
    public function onDeletePortfolio(){
        $user = Auth::getUser();
        ArtistPortfolio::where('user_id', $user->id)->where('id', post('portfolio_id'))->delete();
        return Redirect::refresh();
    }

    public function displayProduct(){
        $user = Auth::getUser();
        $product = ArtistPortfolio::where('slug', $this->param('slug'))->where('user_id', $user->id)->first();
        return $product;
    }
    
    public function displayProducts(){
        $user = Auth::getUser();
        $products = ArtistPortfolio::where('user_id', $user->id)->get();
        return $products;
	}

	// public function loadCategories() {
	// 	$categories = Category::all();
	// 	return $categories;
	// }

	public function loadChildCategories(){}

	public function flattenArray($array) {
 $arrayValues = array();
 foreach (new RecursiveIteratorIterator( new RecursiveArrayIterator($array)) as $val) {
  $arrayValues[] = $val;
 }
 return $arrayValues;
}
    public function to_permalink($str){
		if($str !== mb_convert_encoding( mb_convert_encoding($str, 'UTF-32', 'UTF-8'), 'UTF-8', 'UTF-32') )
			$str = mb_convert_encoding($str, 'UTF-8', mb_detect_encoding($str));
		$str = htmlentities($str, ENT_NOQUOTES, 'UTF-8');
		$str = preg_replace('`&([a-z]{1,2})(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig);`i', '\\1', $str);
		$str = html_entity_decode($str, ENT_NOQUOTES, 'UTF-8');
		$str = preg_replace(array('`[^a-z0-9]`i','`[-]+`'), '-', $str);
		$str = strtolower( trim($str, '-') );
		return $str;
    }

    public function getColors(){
        $this->colors = Color::all();
    }

// function flattenArrayIndexed()
	
	// public function displayManufacturerProducts(){

    // 	$this->products = Product::where('manufacturer_id', $this->manufacturer->id)->get();
    // }
    //public $manufacturers;
    public $newProduct;
	public $product;
	public $products;
    public $categories;
    public $manufacturers;
    public $colors;
    public $user;


}