<?php namespace Ffande\Procurement\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Ffande\Procurement\Models\Manufacturer;
use Ffande\Procurement\Models\Product;
use Ffande\Procurement\Models\Category;
use Redirect; 


class ShopCategory extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Display Shop',
			'description' => 'List products by category'
		];
	}



	public function onRun(){
        
        if ($this->param('slug')) {
			//$this->displayProduct();
			//$this->displayManufacturerProducts();	
        } else {
			$this->products = Product::paginate(30);
			$this->productCategories = $this->loadCategories();
        }
        

    }

    public function displayCategoryProducts(){
        $productCategory = Category::where('slug', $this->param('category'))->first();
        $this->products = Product::where('category_id', $productCategory->id)->simplepaginate(30);
	}

	public function loadCategories() {
		$categories = Category::all();
		return $categories;
	}

	public function loadChildCategories(){}
	public function onAddToCart(){
		
	}
	
	// public function displayManufacturerProducts(){

    // 	$this->products = Product::where('manufacturer_id', $this->manufacturer->id)->get();
    // }
    //public $manufacturers;
	public $product;
	public $products;
	public $productCategories;

}