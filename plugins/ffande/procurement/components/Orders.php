<?php namespace Ffande\Procurement\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Ffande\Forms\Models\CallForTender;
use Ffande\Forms\Models\Product;
use Ffande\Procurement\Models\Order;
use Redirect; 
use Auth;


class Orders extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Display Orders',
			'description' => 'List all Orders'
		];
	}



	public function onRun(){
		$this->displayOrders();
		//return $this->callForTenders;
        // if ($this->param('slug')) {
		// 	$this->displayManufacturer();
		// 	$this->displayManufacturerProducts();	
        // } else {
        // 	$this->manufacturers = Manufacturer::all();
        // }
        

    }

    // public function displayManufacturer(){
    // 	$this->manufacturer = Manufacturer::where('slug', $this->param('slug'))->first();
	// }
	
	public function displayOrders(){
		$user = Auth::getUser();
		$this->orders = Order::where('user_id', $user->id)->orderBy('id')->get();
		
    }
    public $orders;
	
	//public $products;

}