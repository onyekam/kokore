<?php namespace Ffande\Procurement\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Ffande\Procurement\Models\Manufacturer;
use Ffande\Procurement\Models\Product;
use Ffande\Procurement\Models\Category;
use Ffande\Procurement\Models\Size;
use Redirect; 


class Navigation extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Menu & Mega Menu Items',
			'description' => 'Gets Product based Mega Menu Items'
		];
	}



	public function onRun(){
        
        $this->displayPrintsMegaMenu();
        $this->displayHomeItemsMenu();
        

    }

    // public function displayCategoryProducts(){
    //     $productCategory = Category::where('slug', $this->param('category'))->first();
    //     $this->products = Product::where('category_id', $productCategory->id)->simplepaginate(30);
    // }
    
    public function displayPrintsMegaMenu(){
        $this->printsMegaMenu = Category::where('home_item_category','0')->where('parent_category_id', '!=', null)->get();
        $this->printSizes = Size::all();
    }

    public function displayHomeItemsMenu(){
        $this->homeItemsCategories = Category::where('home_item_category', '1')->get();
    }

	public function loadCategories() {
		$categories = Category::all();
		return $categories;
	}

	// public function displayManufacturerProducts(){

    // 	$this->products = Product::where('manufacturer_id', $this->manufacturer->id)->get();
    // }
    //public $manufacturers;
	public $product;
	public $products;
    public $productCategories;
    public $printsMegaMenu;
    public $homeItemsCategories;
    public $printSizes;
}