<?php namespace Ffande\Procurement\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Ffande\Procurement\Models\Manufacturer;
use Ffande\Procurement\Models\Product;
use Redirect; 


class Community extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Display Manufacturers',
			'description' => 'List all manufacturers on manufacturers page'
		];
	}



	public function onRun(){
        
        if ($this->param('slug')) {
			$this->displayManufacturer();
			$this->displayManufacturerProducts();	
        } else {
        	$this->manufacturers = Manufacturer::all();
        }
    }

    public function displayManufacturer(){
    	$this->manufacturer = Manufacturer::where('slug', $this->param('slug'))->first();
	}
	
	public function displayProfessions(){

    	$this->products = Product::where('manufacturer_id', $this->manufacturer->id)->get();
    }
    public $manufacturers;
	public $manufacturer;
	public $products;

}