<?php namespace Ffande\Procurement\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Ffande\Forms\Models\CallForTender;
use Ffande\Forms\Models\Product;
use Ffande\Procurement\Models\DecorOrders;
use Redirect; 
use Auth;


class DecorOrderDetails extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Display Order details',
			'description' => 'List all Order details'
		];
	}



	public function onRun(){
		$this->displayOrderDetails();
		//return $this->callForTenders;
        // if ($this->param('slug')) {
		// 	$this->displayManufacturer();
		// 	$this->displayManufacturerProducts();	
        // } else {
        // 	$this->manufacturers = Manufacturer::all();
        // }
        

    }

    // public function displayManufacturer(){
    // 	$this->manufacturer = Manufacturer::where('slug', $this->param('slug'))->first();
	// }
	
	public function displayOrderDetails(){
		$user = Auth::getUser();
		$this->orderDetails = DecorOrders::where('id', $this->param('slug'))->first();
		// $this->order = Order::where('id',$this->param('slug'))->first();
		
    }
	public $orderDetails;
	
	//public $products;

}