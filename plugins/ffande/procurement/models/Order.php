<?php namespace Ffande\Procurement\Models;

use Model;

/**
 * Model
 */
class Order extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ffande_procurement_order_summary';

    public $hasOne = [
        'delivery_term' => ['Ffande\Procurement\Models\DeliveryTerms', 'key' => 'id', 'otherKey' => 'delivery_terms_id'],
        
        'ordershipping'=> 'Ffande\Procurement\Models\OrderShipping'
    ];

    public $belongsTo = [
        'user' => 'Rainlab\User\Models\User',
        'status' => 'Ffande\Procurement\Models\Status',
        'payment_method' => 'Ffande\Procurement\Models\PaymentMethod', 
        
    ];

    public $belongsToMany = [
        
        
    ];

    public $hasMany = [
        'order_details' => 'Ffande\Procurement\Models\OrderDetails',
    ];


    // public $belongsTo = [
        
    // ];
}