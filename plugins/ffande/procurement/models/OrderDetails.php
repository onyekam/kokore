<?php namespace Ffande\Procurement\Models;

use Model;

/**
 * Model
 */
class OrderDetails extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ffande_procurement_order_details';

    public $hasOne = [
        'order' => [ 'Ffande\Procurement\Models\Order', 'key'=>'id', 'otherKey'=>'order_id' ],
        'product' => ['Ffande\Procurement\Models\Product','key'=>'id','otherKey'=>'product_id'],
        'size' => ['Ffande\Procurement\Models\Size','key'=>'id','otherKey'=>'size_id'],
        'color' => ['Ffande\Procurement\Models\Color','key'=>'id','otherKey'=>'color_id'],
        'frame' => ['Ffande\Procurement\Models\Frame','key'=>'id','otherKey'=>'frame_id']
    ];

    public $belongsTo = [
        'order' => [ 'Ffande\Procurement\Models\Order', 'key'=>'id', 'otherKey'=>'order_id' ]
    ];


}