<?php namespace Ffande\Procurement\Models;

use Model;

/**
 * Model
 */
class Color extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ffande_procurement_colors';

    public $attachOne = [
        'color_image' => 'System\Models\File',
    ];

    public $belongsToMany = [
        'products' => 'Ffande\Procurement\Models\Product'
    ];
}