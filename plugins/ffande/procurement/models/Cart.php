<?php namespace Ffande\Procurement\Models;

use Model;

/**
 * Model
 */
class Cart extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ffande_procurement_cart';

    public $hasOne = [
        'user' => ['Rainlab\User\Models\User', 'key'=>'id', 'otherKey'=>'user_id'],
        'product' => ['Ffande\Procurement\Models\Product', 'key'=>'id', 'otherKey'=>'product_id'],
        'color' => ['Ffande\Procurement\Models\Color', 'key'=>'id', 'otherKey'=>'color_id'],
        'frame' => ['Ffande\Procurement\Models\Frame', 'key'=>'id', 'otherKey'=>'frame_id']
    ];

}