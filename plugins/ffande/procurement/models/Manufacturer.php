<?php namespace Ffande\Procurement\Models;

use Model;
use Category;
use Product;

/**
 * Model
 */
class Manufacturer extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ffande_procurement_manufacturers';

    public $hasMany = [
        'products' => 'Ffande\Procurement\Models\Product'
       // 'productcatalogs' => 'System\Models\File'
    ];

     public $attachOne = [
        'manufacturer_image' => 'System\Models\File'
    ];

    
}