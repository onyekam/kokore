<?php namespace Ffande\Procurement\Models;

use Model;

/**
 * Model
 */
class Category extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ffande_procurement_categories';

    public $hasMany = [
        //'products' => 'Ffande\Procurement\Models\Product',
        'categories' => ['Ffande\Procurement\Models\Category', 'key' =>'parent_category_id', 'other_key' => 'id']
    ];

    public $belongsTo = [
        'parent_category' => ['Ffande\Procurement\Models\Category', 'key' => 'parent_category_id', 'other_key' => 'id']
    ];

    public $belongsToMany = [
        'products' => [
            'Ffande\Procurement\Models\Product',
            'table' => 'ffande_procurement_categories_products',
            'order' => 'product_name'
        ]
    ];
}