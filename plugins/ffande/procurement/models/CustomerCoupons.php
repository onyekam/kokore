<?php namespace Ffande\Procurement\Models;

use Model;

/**
 * Model
 */
class CustomerCoupons extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /*
     * Validation
     */
    public $rules = [
    ];

    public $hasOne = [
        'coupons' => ['Ffande\Procurement\Models\Coupon', 'key' => 'coupon_id'],
        'customers' => ['Rainlab\User\Models\User', 'key' => 'customer_id']
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ffande_procurement_coupon_user';
}