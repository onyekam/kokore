<?php namespace Ffande\Procurement\Models;

use Model;

/**
 * Model
 */
class Product extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;
    public $implement = ['BABA.Favoritebutton.Behaviors.Favoriteable'];
    
    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ffande_procurement_products';

    public $belongsTo = [
        'category' => 'Ffande\Procurement\Models\Category',
        'manufacturer' => 'Ffande\Procurement\Models\Manufacturer',
        'user' => 'Rainlab\User\Models\User',
        'offer' => 'Ffande\Procurement\Models\Offer',
        'base_price' => ['Ffande\Procurement\Models\Pricingtwo', 'key' => 'pricingtwo_id', 'other_key' => 'id']
    ];

    public $belongsToMany = [
        'sizes' => [
            'Ffande\Procurement\Models\Size',
            'table' => 'ffande_procurement_products_sizes',
            'order' => 'size'
        ],
        'colors' => [
            'Ffande\Procurement\Models\Color',
            'table' => 'ffande_procurement_colors_products',
            'order' => 'color'
        ],
        'formats' => [
            'Ffande\Procurement\Models\Format',
            'table' => 'ffande_procurement_formats_products',
            'order' => 'format'
        ],
        'categories' => [
            'Ffande\Procurement\Models\Category',
            'table' => 'ffande_procurement_categories_products',
            'order' => 'category_name'
        ]
    ];

    public $hasMany = [
        'likes' => 'BABA\Favoritebutton\Models\Favorite',
        'prices' => 'Ffande\Procurement\Models\Price',
        'ratings' => 'Ffande\Procurement\Models\Rating'
    ];

    public $attachOne = [
        'product_image' => 'System\Models\File',
        'hover_image' => 'System\Models\File',
        'perfect_pair_1_image' => 'System\Models\File',
        'perfect_pair_2_image' => 'System\Models\File'
    ];

    public $attachMany = [
        'gallery' => 'System\Models\File'
    ];

    public static $allowedSortingOptions = array(
        'pricingtwo_id asc' => 'Lowest to Highest Price',
        // 'pricingtwo_id desc' => 'Highest to Lowest Price',
        'new asc' => 'New Prints',
        //'likes desc' => 'Most Popular',
    );

    public function scopeListFrontEnd($query, $options = []) {
        if (isset($_COOKIE['pagination_posjs'])) {
            $page = $_COOKIE['pagination_posjs'];
        } else {
            $page = 1;
        }
        extract(array_merge([
            'page' => $page,
            'perPage' => 36,
            'sort' => 'product_name',
            'sizes' => null,
            'formats' => null,
            'colors' => null,
            'order' => 'product_name',
            'productCategories' => null
        ], $options));

        if (!is_array($sort)) {
            $sort = [$sort];

        }

        foreach ($sort as $_sort) {
            if (in_array($_sort, array_keys(self::$allowedSortingOptions))) {
                $parts = explode(' ',$_sort);
                if (count($parts) < 2) {
                    array_push($parts, 'desc');
                }

                list($sortField, $sortDirection) = $parts;
                if ($sortField == 'likes' ) {
                    $likeViews = DB::select("CREATE VIEW `product` AS SELECT `membership_number`,`full_names`,`gender` FROM `members`;");
                    //$query->orderBy($self->likes, $sortDirection);
                } else {
                    $query->orderBy($sortField, $sortDirection);
                }
            }
        }

        if ($productCategories !== null) {
            if(!is_array($productCategories)){
                $productCategories = [$productCategories];
            }

            foreach ($productCategories as $category) {
                $query->wherehas('categories', function($q) use ($category){
                    $q->where('slug',$category);
                });
            }

        }
        
        if ($sizes !== null) {
            if(!is_array($sizes)){
                $sizes = [$sizes];
            }

            foreach ($sizes as $size) {
                $query->wherehas('sizes', function($q) use ($size){
                    $q->where('slug',$size);
                });
            }

        }

        if ($colors !== null) {
            if(!is_array($colors)){
                $colors = [$colors];
            }

            foreach ($colors as $color) {
                $query->wherehas('colors', function($q) use ($color){
                    $q->where('slug',$color);
                });
            }

        }

        $lastPage = $query->paginate($perPage, $page)->lastPage();
        if ($lastPage < $page ) {
            $page = 1;
        }

        if ($formats !== null) {
            if(!is_array($colors)){
                $colors = [$colors];
            }

            foreach ($formats as $format) {
                $query->wherehas('formats', function($q) use ($format){
                    $q->where('slug',$format);
                });
            }

        }


        return $query->where('perfect_pair',0)->paginate($perPage, $page);
    }
}