<?php namespace Ffande\Procurement\Models;

use Model;

/**
 * Model
 */
class Solution extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ffande_procurement_project_solutions_solution';

    public $attachOne = [
        'solution_image' => 'System\Models\File'
    ];

    public $belongsTo = [
        'project_solutions' => 'Ffande\Procurement\Models\ProjectSolutions'
    ];
}