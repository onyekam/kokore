<?php namespace Ffande\Procurement\Models;

use Model;

/**
 * Model
 */
class DecorOrders extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ffande_procurement_dorders';


    public $belongsTo = [
        'user' => 'Rainlab\User\Models\User',    
        'package' => ['Ffande\Procurement\Models\DecorPackages','key' => 'package_id', 'other_key' => 'id'],    
        'status' => 'Ffande\Procurement\Models\Status',
    ];

}