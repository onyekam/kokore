<?php namespace Ffande\Procurement\Models;

use Model;

/**
 * Model
 */
class Annotations extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ffande_procurement_project_solutions_annotations';

    protected $fillable = ['text','context','width','height','x','y'];
}