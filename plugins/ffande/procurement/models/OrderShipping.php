<?php namespace Ffande\Procurement\Models;

use Model;

/**
 * Model
 */
class OrderShipping extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ffande_procurement_order_shipping';

    public $belongsTo = [
        'order' => 'Ffande\Procurement\Models\Order'
    ];

    public $hasMany = [
        'orders' => 'Ffande\Procurement\Models\Order',
    ];
}