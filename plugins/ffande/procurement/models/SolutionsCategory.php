<?php namespace Ffande\Procurement\Models;

use Model;

/**
 * Model
 */
class SolutionsCategory extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ffande_procurement_project_solutions_category';

    public $hasMany = [
        'project_solutions' => 'Ffande\Procurement\Models\ProjectSolutions'
    ];

    public $attachOne = [
        'category_image' => 'System\Models\File'
    ];
}