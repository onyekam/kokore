<?php namespace Ffande\Procurement\Models;

use Model;

/**
 * Model
 */
class Coupon extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ffande_procurement_coupon';

    public $belongsToMany = [
        'customers' => ['Rainlab\User\Models\User', 'key' => 'coupon_id', 'otherKey' => 'customer_id','table' => 'ffande_procurement_coupon_user','pivot' => ['customer_id', 'coupon_id']]
    ];

    public $belongsTo = [
        'user' => 'Rainlab\User\Models\User'
    ];
}