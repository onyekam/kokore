<?php namespace Ffande\Procurement\Models;

use Model;

/**
 * Model
 */
class Offer extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ffande_procurement_offers';
    public $attachOne = [
        'offer_image' => 'System\Models\File'
    ];
}