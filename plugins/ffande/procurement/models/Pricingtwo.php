<?php namespace Ffande\Procurement\Models;

use Model;

/**
 * Model
 */
class Pricingtwo extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ffande_procurement_pricing_two';

    // public $belongsTo = [
    //     'product' => ['Ffande\Procurement\Models\Product']
    // ];

    public $hasMany = [
        'likes' => 'BABA\Favoritebutton\Models\Favorite',
        'products' => 'Ffande\Procurement\Models\Product'
    ];
}