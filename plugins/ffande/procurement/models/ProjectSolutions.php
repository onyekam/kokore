<?php namespace Ffande\Procurement\Models;

use Model;

/**
 * Model
 */
class ProjectSolutions extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ffande_procurement_project_solutions';

    public $belongsTo = [
        'category' => 'Ffande\Procurement\Models\SolutionsCategory'
    ];

    public $hasMany = [
        'solutions' => 'Ffande\Procurement\Models\Solution'
    ];


}