<?php namespace Ffande\Procurement;

use System\Classes\PluginBase;
use Ffande\Procurement\Models\Product as ProductModel;
use BABA\Favoritebutton\Models\Favorite;
use Ffande\Procurement\Models\OrderDetails;


class Plugin extends PluginBase
{
    public function registerComponents()
    {
    	return [
        'Ffande\Procurement\Components\DisplayManufacturers' => 'DisplayManufacturers',
        'Ffande\Procurement\Components\Shop' => 'Shop',
        'Ffande\Procurement\Components\Carts' => 'Cart',
        'Ffande\Procurement\Components\Checkout' => 'Checkout',
        'Ffande\Procurement\Components\DisplaySingleProduct' => 'DisplaySingleProduct',
        'Ffande\Procurement\Components\DashboardProducts' => 'DashboardProducts',
        'Ffande\Procurement\Components\DashboardPortfolio' => 'DashboardPortfolio',
        'Ffande\Procurement\Components\Orders' => 'Orders',
        'Ffande\Procurement\Components\DecorOrdersComponent' => 'Decororders',
        'Ffande\Procurement\Components\DecorOrderDetails' => 'Decororderdetails',
        'Ffande\Procurement\Components\DisplayOrderDetails' => 'DisplayOrderDetails',
        'Ffande\Procurement\Components\DisplayProjectSolutions' => 'DisplayProjectSolutions',
        'Ffande\Procurement\Components\BulkUpload' => 'BulkUpload',
        'Ffande\Procurement\Components\Annotation' => 'Annotation',
        'Ffande\Procurement\Components\Instagram' => 'Instagram',
        'Ffande\Procurement\Components\Navigation' => 'Navigation',
        'Ffande\Procurement\Components\Homeslider' => 'Slider',
        'Ffande\Procurement\Components\SecondarySlider' => 'SecondarySlider',
        'Ffande\Procurement\Components\Newfav' => 'Newfav',
        'Ffande\Procurement\Components\Ratings' => 'Ratings',
        'Ffande\Procurement\Components\Bookdecor' => 'Bookdecor',
        'Ffande\Procurement\Components\Offers' => 'Offers',
        'Ffande\Procurement\Components\Homelink' => 'homelink'
    	];
    }

    public function registerSettings()
    {
    }

    public function registerMailTemplates()
    {
        return [
            'ffande.procurement::mail.thanksfororder' => 'Thanks the customer for order and displays the order',
            'ffande.procurement::mail.blogpost'  => 'Sends latest blog post to the subscribed users',
            'ffande.procurement::mail.artistordernotification'  => 'Lets artists know they received orders',
            'ffande.procurement::mail.ordernotification'  => 'Let\'s Kokore know they\'ve received an order'
        ];
    }

    // public function registerNotificationRules()
    // {
    //     return [
    //         'groups' => [
    //             'blog' => [
    //                 'label' => 'Blog',
    //                 'icon' => 'icon-book'
    //             ],
    //         ],
    //         'events' => [
    //             \Ffande\Procurement\NotifyRules\BlogPostEvent::class,
    //         ],
    //         'actions' => [],
    //         'conditions' => [
    //             \RainLab\User\NotifyRules\UserAttributeCondition::class
    //         ],
    //     ];
    // }

    public function boot()
    {
        

        ProductModel::extend(function($model){
            $model->addDynamicMethod('orderCount', function() use ($model){
                $productOrders = OrderDetails::where('product_id', $model->id)->get();
                $orderCount = count($productOrders);
                return $productOrders;
            });

            $model->addDynamicMethod('lowestprintprice', function() use ($model){
                
                //$product = ProductModel::where('id', $model->id)->first();
                $sizes = $model->sizes;
                //dd($sizes);
                $lowestSize = $sizes['0'];
                //dd($lowestSize);
                $size = $lowestSize->sizeintext;
                //dd($size);
                $price = $model->base_price[$size];
                //dd($price);
                return $price;
            });

            // Find the number of Sales for One Product
            // $model->addDynamicMethod('productCount', function() use ($model){
            //     $products = OrderDetails::where('product_id', $model->id)->get();

            // });
        });
        
    }
}
