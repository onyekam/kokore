<?php namespace Onyekam\Payment\Components;

use Illuminate\Http\Request;
use Cms\Classes\ComponentBase;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Paystack;
use Ffande\Procurement\Models\Order;
use Ffande\Procurement\Models\OrderDetails;
use Ffande\Procurement\Models\Product;
use Ffande\Procurement\Models\Size;
use Ffande\Procurement\Models\Price;
use Ffande\Procurement\Models\Frame;
use Redirect;
use Auth;

class PaystackPayment extends ComponentBase {
    public function componentDetails(){
		return [
			'name' => 'Implement Paystack',
			'description' => 'Implementing Paystack Payments'
		];
	}

    public function onRun(){
        if ($this->getUser()) {
            $user = $this->getUser();
            $url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
            if ($this->param('id')) {
                $order = Order::where('id', $this->param('id'))->first();
                if ($user->id === $order->user_id) {
                    if ($order->status_id > 1) {
                    \Flash::error('This order has already been paid for');
                    return Redirect::to('/prints');
                    } else {
                        $this->transRef = $this->getTransRef();
                        $this->order = $this->getOrder();
                        $this->orderDetails = $this->getOrderDetails();
                        $this->secretKey = $this->getSecretKey();
                    }
                } else {
                    \Flash::error('Page Restricted');
                    return Redirect::to('/prints');
                }
                
            } elseif (strpos($url,'trxref')) {
                $this->handleGatewayCallback();
            } elseif (post('')) {
                \Flash::error('Please provide an order number in order to proceed with payment');
                return Redirect::to('/prints');
            }
        } else {
            \Flash::error('You must be signed in to make payment');
            return Redirect::back();
        }
    }

    public function onCOD(){
        $orderId = post('id');
        $order = Order::where('id',$orderId)->first();
        $order->status_id = 1;
        $order->payment_method_id = 1 ;
        return Redirect::to('/thank-you');
     }
    /**
     * Redirect the User to Paystack Payment Page
     * @return Url
     */
    public function onRedirectToGateway(){
        $order = Order::where('id', post('orderID'))->first();
        $order->transref = post('reference');
        $order->save();
        return Paystack::getAuthorizationUrl()->redirectNow();
    }
    /**
     * Obtain Paystack payment information
     * @return void
     */
    public function handleGatewayCallback(){
        $paymentDetails = Paystack::getPaymentData();
       // $paymentDetails = json_decode($paymentDetails);
        $reference = $paymentDetails['data']['reference'];
        $order = Order::where('transref', $reference)->first();
        if ($paymentDetails['status'] && $paymentDetails['message'] === 'Verification successful')  {
            $order->status_id = 2;
            $order->payment_method_id = 2;
            $order->save();
            $this->order = $order;
            $this->transRef = $order->transref;
            \Flash::success('Payment was successful and your order is being processed');
            return Redirect::to('/prints');
        } else {
            \Flash::error('Something went wrong please try again!');
            return Redirect::to('/confirm-pay'.'/'.$order->id);
        }
        //dd($paymentDetails);
        // Now you have the payment details,
        // you can store the authorization_code in your db to allow for recurrent subscriptions
        // you can then redirect or do whatever you want
    }

    public function getOrder(){
        $order = Order::where('id', $this->param('id'))->first();
        return $order;
    }

    public function getOrderDetails(){
        $orderDetails = OrderDetails::where('order_id', $this->param('id'))->first();
        return $orderDetails;
    }

    public function getTransRef(){
        $transRef = Paystack::genTranxRef();
        return $transRef;
    }

    public function getSecretKey(){
        $secretKey = config('paystack.secretKey');
        return $secretKey;
    }

    public function getUser(){
        $user = Auth::getUser();
        return $user;
    }

    public $secretKey;
    public $order;
    public $orderDetails;
    public $transRef;
}