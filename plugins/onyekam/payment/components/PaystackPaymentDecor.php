<?php namespace Onyekam\Payment\Components;

use Illuminate\Http\Request;
use Cms\Classes\ComponentBase;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Paystack;
use Ffande\Procurement\Models\DecorOrders as Order;
use Redirect;
use Auth;
use Mail;

class PaystackPaymentDecor extends ComponentBase {
    public function componentDetails(){
		return [
			'name' => 'Implement Paystack',
			'description' => 'Implementing Paystack Payments'
		];
	}

    public function onRun(){
        //if ($this->getUser()) {
            //$user = $this->getUser();
            $url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
            if ($this->param('id')) {
                $order = Order::where('id', $this->param('id'))->first();
                //if ($user->id === $order->user_id) {
                    if ($order->status_id > 1) {
                        \Flash::error('This order has already been paid for');
                        return Redirect::to('/prints');
                    } else {
                        $this->transRef = $this->getTransRef();
                        $this->order = $this->getOrder();
                        $this->secretKey = $this->getSecretKey();
                    }
                //} else {
                //    \Flash::error('Page Restricted');
                //    return Redirect::to('/decor-services');
                //}
                
            } elseif (strpos($url,'trxref')) {
                $this->handleGatewayCallback();
            } else {
                \Flash::error('Please provide a valid order number in order to proceed with payment');
                return Redirect::to('/decor-services');
            }
        //} else {
        //   \Flash::error('You must be signed in to make payment');
        //    return Redirect::back();
        //}
    }
    /**
     * Redirect the User to Paystack Payment Page
     * @return Url
     */
    public function onRedirectToGateway(){
        $order = Order::where('id', post('orderID'))->first();
        $order->transref = post('reference');
        $order->save();
        return Paystack::getAuthorizationUrl()->redirectNow();
    }
    /**
     * Obtain Paystack payment information
     * @return void
     */
    public function handleGatewayCallback(){
        $paymentDetails = Paystack::getPaymentData();
       // $paymentDetails = json_decode($paymentDetails);
        $reference = $paymentDetails['data']['reference'];
        $order = Order::where('transref', $reference)->first();
        if ($paymentDetails['status'] && $paymentDetails['message'] === 'Verification successful')  {
            $order->status_id = 2;
            //$order->payment_method_id = 2;
            $order->save();
            $this->order = $order;
            $this->transRef = $order->transref;
            //\Flash::success('Payment was successful and your order is being processed');
            \Flash::success('Payment was successful! The Interior Decorator will be contacting you shortly');
            $this->newOrder = $order;
            $this->decorator = $order->user;
            $this->sendDecoOrderConfirmation();
            $this->sendDecoOrderToDecorator();
            return Redirect::to('/decor-services');
        } else {
            \Flash::error('Something went wrong please try again!');
            return Redirect::to('/decor-confirm-pay'.'/'.$order->id);
        }
        //dd($paymentDetails);
        // Now you have the payment details,
        // you can store the authorization_code in your db to allow for recurrent subscriptions
        // you can then redirect or do whatever you want
    }

    public function sendDecoOrderConfirmation(){
        //$this->user = Auth::getUser();
        //$cart = Session::get('cart');
        //$order = $this->prepareOrder();
        //dd($order);
        $vars = ['firstname' => $this->newOrder->f_name, 'lastname' => $this->newOrder->l_name, 'order' => $this->newOrder, 'package' => $this->newOrder->package->package_name, 'order_id' => $this->newOrder->id];
        Mail::send('ffande.procurement::mail.deco-order-confirmation', $vars, function($message) {
            $message->to($this->newOrder->email, $this->newOrder->f_name.' '.$this->newOrder->l_name);
            $message->subject('Kokore Decor Order Confirmation | Order Nº'.$this->newOrder->id.' '.$this->newOrder->f_name.' '.$this->newOrder->l_name);
        }); 
    }

    public function sendDecoOrderToDecorator(){
        //$this->user = Auth::getUser();
        
        //$cart = Session::get('cart');
        //$order = $this->prepareOrder();
        //dd($order);
       //dd($this->decorator->email.' '.$this->decorator->name.' '.$this->decorator->surname);
        $vars = ['messagesent' => $this->newOrder->message, 'email' => $this->newOrder->email ,'phone' => $this->newOrder->phone,  'firstname' => $this->newOrder->f_name, 'lastname' => $this->newOrder->l_name, 'order' => $this->newOrder, 'package' => $this->newOrder->package->package_name, 'order_id' => $this->newOrder->id, 'name' => $this->decorator->name];

        Mail::send('ffande.procurement::mail.designer-deco-notification', $vars, function($message) {
            $message->to($this->decorator->email, $this->decorator->name.' '.$this->decorator->surname);
            $message->subject('Client Decor Order Notification | Order Nº'.$this->newOrder->id.' '.$this->newOrder->f_name.' '.$this->newOrder->l_name);
        }); 
    }

    public function getOrder(){
        $order = Order::where('id', $this->param('id'))->first();
        return $order;
    }

    public function getTransRef(){
        $transRef = Paystack::genTranxRef();
        return $transRef;
    }

    public function getSecretKey(){
        $secretKey = config('paystack.secretKey2');
        return $secretKey;
    }

    public function getUser(){
        $user = Auth::getUser();
        return $user;
    }

    public $secretKey;
    public $order;
    public $transRef;
    public $newOrder;
    public $decorator;
}